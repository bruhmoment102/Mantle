# Mantle
A Minecraft clone written in D, using Vulkan and SDL2. Features:
- Infinite world generation in all directions, including up and down
- Multi-colored smooth lighting
- MSAA anti-aliasing
- Basic creative-mode type gameplay
- Linux and Windows support

At some point I lost interest in working on this but it seems like a shame to keep it on my hard drive instead of sharing it with the world, because I put a lot of effort into it. These days I mostly program Rust instead of D because I got tired of fighting the garbage collector, although Rust is a lot uglier ;)

![Screenshot](https://files.catbox.moe/8rxfrq.png)
