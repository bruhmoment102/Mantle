#version 450

layout(location = 0) in vec3 inTex;
layout(location = 1) in vec3 inCol;

layout(location = 0) out vec4 outCol;

layout(binding = 0) uniform sampler2DArray toSample;

void main()
{
    outCol = texture(toSample, inTex) * vec4(inCol, 1.0);
    if (outCol.a < 0.5)
    {
        discard;
    }
    outCol.a = 1;
}

