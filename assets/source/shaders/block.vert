#version 450

layout(location = 0) in vec3 inPos;
layout(location = 1) in uint inNorm;
layout(location = 2) in vec3 inTex;
layout(location = 3) in uint inCol;

layout(location = 0) out vec3 outTex;
layout(location = 1) out vec3 outCol;
layout(location = 2) out vec3 outPos;

layout(push_constant) uniform push
{
    mat4 transform;
    
    vec3 sunLightDir;
    float sunLight;
    vec3 moonLightDir;
    float moonLight;
};

void main()
{
    //Transform vertices into clip space
    gl_Position = transform * vec4(inPos, 1.0);
    
    //Pass along fragment attributes
    outTex = inTex;
    outPos = inPos;
    outCol = vec3(pow(float((inCol >> 12) & 0xF)/15, 2), pow(float((inCol >> 8) & 0xF)/15, 2), pow(float((inCol >> 4) & 0xF)/15, 2));

    //Consider the direction for artifical lighting
    vec3 normal = (vec3(bitfieldExtract(inNorm, 20, 10), bitfieldExtract(inNorm, 10, 10), bitfieldExtract(inNorm, 0, 10))-512) / 512;
    outCol *= 0.8 + clamp(normal.x, 0, 1)*0.2;
    
    //Apply sky lighting to the vertex color
    float sunDirIntensity  = 0.8 + clamp(dot(normal, sunLightDir ), 0, 1)*0.2;
    float moonDirIntensity = 0.8 + clamp(dot(normal, moonLightDir), 0, 1)*0.2;
        
    float sky = pow(float(inCol & 0xF)/16, 2) * (sunLight*sunDirIntensity + moonLight*moonDirIntensity);
    outCol.x = max(sky, outCol.x);
    outCol.y = max(sky, outCol.y);
    outCol.z = max(sky, outCol.z);
    
    //Apply ambient occlusion
    outCol *= float(inCol >> 16) / 4;
}
