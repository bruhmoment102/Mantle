#version 450

#define PI 3.1415926538

layout(location = 0) in vec3 inPos;

layout(location = 0) out vec4 outCol;

layout(push_constant) uniform push
{
    layout(offset = 108) float time;
    layout(offset = 124) float dayLight;
};

float white(vec4 point, float index)
{
    float sum1 = point.x + point.y*2 - point.z*3 + point.w*4 + index*5;
    float sum2 = point.x*5 - point.y*4 + point.z*3 + point.w*2 + index;
    return mod(tan(sum1) + tan(sum1*0.5) + sin(sum2), 1);
}

float qlerp(float val1, float val2, float dist)
{
    float ease = 6*pow(dist,5) - 15*pow(dist,4) + 10*pow(dist,3);
    return (1-ease)*val1 + ease*val2;
}

float perlin(vec4 point)
{
    //Find the hypercube that the input point falls into, and relative position
    vec4 rel  = mod(point, 1);
    ivec4 box = ivec4(point - rel);

    //Calculate the noise value
    float[2*2*2] noise;
    for (int w = 0; w < 2; ++w)
    {
        for (int z = 0; z < 2; ++z)
        {
            for (int y = 0; y < 2; ++y)
            {
                ivec4 gradient0pos = ivec4(box.x+0, box.y+y, box.z+z, box.w+w);
                vec4 gradient0 = normalize(vec4( cos(white(gradient0pos, 0)*PI*2), cos(white(gradient0pos, 1)*PI*2), cos(white(gradient0pos, 2)*PI*2), cos(white(gradient0pos, 3)*PI*2) ));
                
                ivec4 gradient1pos = gradient0pos + ivec4(1,0,0,0);                               
                vec4 gradient1 = normalize(vec4( cos(white(gradient1pos, 0)*PI*2), cos(white(gradient1pos, 1)*PI*2), cos(white(gradient1pos, 2)*PI*2), cos(white(gradient1pos, 3)*PI*2) ));
                
                noise[w*4 + z*2 + y] = qlerp(dot(gradient0, rel - vec4(0,y,z,w)), dot(gradient1, rel - vec4(1,y,z,w)), rel.x);
            }
        }
    }
    return (qlerp(qlerp(qlerp(noise[0], noise[1], rel.y), qlerp(noise[2], noise[3], rel.y), rel.z), 
                  qlerp(qlerp(noise[4], noise[5], rel.y), qlerp(noise[6], noise[7], rel.y), rel.z), rel.w) + 1) / 2;
}

void main()
{
    vec3 unit = normalize(inPos);
    float h = (unit.y + 1)/2;
    outCol.a = clamp((-0.5/( clamp(h-0.25, 0.00001, 0.75)*(4.0/3.0) ))+1.5, 0, 1);
    if (outCol.a == 0)
    {
        //Try to return if possible
        return;
    }
    
    vec3 cloudPos = floor(unit*96)/96;
    float noise = perlin(vec4(cloudPos.xy, cloudPos.z + time*0.02, time*0.01))*0.9 + perlin(vec4(cloudPos.xy*10, cloudPos.z*10 + time*0.2, time*0.01))*0.1;
    outCol.a *= clamp(-1/noise + 2, 0, 1);
    outCol.rgb = dayLight*vec3(1,1,1);
}
