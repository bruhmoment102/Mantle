#version 450

layout(push_constant) uniform push
{
    mat4 transform;
};

void main()
{
    gl_Position = transform * vec4(-1 + (gl_VertexIndex % 2)*4, -0.1,
                                   -1 + (gl_VertexIndex / 2)*4, 1);
}
