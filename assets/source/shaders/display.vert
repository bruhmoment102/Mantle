#version 450

layout(location = 0) in vec3 inPos;
layout(location = 1) in uint inNorm;
layout(location = 2) in vec3 inTex;

layout(location = 0) out vec3 outTex;
layout(location = 1) out vec3 outCol;

layout(push_constant) uniform push
{
    mat4 transform;
    uint[3] textureID;
};

void main()
{
    //Transform vertices into clip space
    gl_Position = transform * vec4(inPos, 1.0);
    
    //Pass along fragment attributes
    outTex = vec3(inTex.xy, textureID[int(inTex.z)]);
    
    vec3 normal = (vec3(bitfieldExtract(inNorm, 20, 10), bitfieldExtract(inNorm, 10, 10), bitfieldExtract(inNorm, 0, 10))-512) / 512;
    float sunDirIntensity = 0.8 + clamp(dot(normal, normalize(vec3(+0,+1,-0.7))), 0, 1)*0.2;
    outCol = vec3(1,1,1) * sunDirIntensity;
}
