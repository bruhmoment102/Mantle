#version 450

layout(location = 0) in vec3 inTex;
//layout(location = 1) in vec3 inCol;
layout(location = 0) out vec4 outCol;

layout(binding = 0) uniform sampler2DArray toSample;

layout(push_constant) uniform push
{
    layout(offset = 96) uint col;
    layout(offset = 100) float skyLight;
};

void main()
{
    vec3 light = vec3(float((col >> 24) & 0xFF), float((col >> 16) & 0xFF), float((col >> 8) & 0xFF))/16;
    float sky = skyLight * (float(col & 0xFF)/16);
    light.x = max(sky, light.x);
    light.y = max(sky, light.y);
    light.z = max(sky, light.z);
    
    outCol = texture(toSample, vec3(inTex.xy, 1)) * vec4(light, 1);
}
