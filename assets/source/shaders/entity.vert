#version 450

layout(location = 0) in vec3 inPos;
layout(location = 2) in vec3 inTex;

layout(location = 0) out vec3 outTex;

layout(push_constant) uniform push
{
    mat4 transform;
};

void main()
{
    //Transform vertices into clip space
    gl_Position = transform * vec4(inPos, 1.0);
    outTex = inTex;
}
