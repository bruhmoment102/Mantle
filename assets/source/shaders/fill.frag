#version 450

layout(location = 0) in vec3 inTex;

layout(location = 0) out vec4 outCol;

layout(binding = 0) uniform sampler2DArray toSample;

layout(push_constant) uniform push
{
    layout(offset = 96) vec3 colMod;
};

void main()
{
    outCol = vec4(colMod, 1) * texture(toSample, inTex);
}

