#version 450

layout(location = 0) out vec3 outTex;

layout(push_constant) uniform push
{
    layout(offset = 64) vec3 start;
    layout(offset = 80) vec3 end;
};

void main()
{
    gl_Position = vec4(-1 + (gl_VertexIndex % 2)*4,
                       +1 - (gl_VertexIndex / 2)*4, 0, 1);
    outTex = vec3(bool(gl_VertexIndex % 2) ? start.x : end.x*2,
                  bool(gl_VertexIndex / 2) ? start.y : end.y*2, start.z);
}
