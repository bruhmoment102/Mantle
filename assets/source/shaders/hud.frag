#version 450

layout(location = 0) in vec2 inTex;
layout(location = 1) in vec3 inCol;

layout(location = 0) out vec4 outCol;

layout(binding = 0) uniform sampler2D gui;

void main()
{
    outCol = texture(gui, inTex) * vec4(inCol, 1);
}
