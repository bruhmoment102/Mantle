#version 450

layout(location = 0) in vec2 inPos;
layout(location = 2) in vec2 inTex;
layout(location = 3) in uint inCol;

layout(location = 0) out vec2 outTex;
layout(location = 1) out vec3 outCol;

void main()
{
    gl_Position = vec4(inPos, 0, 1);
    outTex = inTex;
    outCol = vec3(float((inCol >> 24) & 0xFF), float((inCol >> 16) & 0xFF), float((inCol >> 8) & 0xFF))/255;
}
