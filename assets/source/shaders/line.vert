#version 450

layout(location = 0) in vec3 inPos;

layout(push_constant) uniform push
{
    mat4 transform;
};

void main()
{
    gl_Position = transform * vec4(inPos, 1.0);
}
