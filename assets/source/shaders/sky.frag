#version 450

layout(location = 0) in vec3 inPos;

layout(location = 0) out vec4 outCol;

layout(push_constant) uniform push
{
    layout(offset = 96) vec3 top;
    layout(offset = 112) vec3 bottom;
};

void main()
{
    float h = (normalize(inPos).y + 1)/2;
    outCol = vec4(top*h + bottom*(1-h), 1.0);
}
