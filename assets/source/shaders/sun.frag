#version 450

layout(location = 0) in vec2 inTex;

layout(location = 0) out vec4 outCol;

layout(binding = 0) uniform sampler2D sun;

void main()
{
    outCol = texture(sun, inTex);
}
