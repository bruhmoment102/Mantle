#version 450

layout(location = 0) in vec3 inPos;
layout(location = 2) in vec2 inTex;

layout(location = 0) out vec2 outTex;

layout(push_constant) uniform push
{
    mat4 transform;
};

void main()
{
    gl_Position = transform * vec4(inPos, 1);
    outTex = inTex;
}
