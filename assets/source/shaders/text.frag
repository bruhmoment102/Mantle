#version 450

layout(location = 0) in vec3 inTex;

layout(location = 0) out vec4 outCol;

layout(binding = 0) uniform sampler2DArray ascii;

layout(push_constant) uniform push
{
    layout(offset = 96) vec4 color;
};

void main()
{
    outCol = texture(ascii, inTex) * color;
}
