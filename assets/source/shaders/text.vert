#version 450

layout(location = 0) in vec2 inPos;
layout(location = 2) in vec3 inTex;

layout(location = 0) out vec3 outTex;

layout(push_constant) uniform push
{
    mat4 transform;
    uint[8] chars;
};

void main()
{
    gl_Position = transform * vec4(inPos.xy, 0, 1);
    gl_Position.z = 0;
    gl_Position.w = 1;
    
    int index = int(inTex.z);
    for (int i = 0; i < 8; ++i)
    {
        if (i == index/4)
        {
            outTex = vec3(inTex.xy, (chars[i] >> (8*(index%4))) & 0xFF);
            return;
        }
    }
}
