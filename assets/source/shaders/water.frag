#version 450

#define PI 3.1415926538

layout(location = 0) in vec3 inTex;
layout(location = 1) in vec3 inCol;
layout(location = 2) in vec3 inPos;

layout(location = 0) out vec4 outCol;

layout(binding = 0) uniform sampler2DArray toSample;

layout(push_constant) uniform push
{
    mat4 transform;
    layout(offset = 96) ivec3 chunkPos;
    layout(offset = 108) float time;
};

float white(vec4 point, float index)
{
    float sum1 = point.x + point.y*2 - point.z*3 + point.w*4 + index*5;
    float sum2 = point.x*5 - point.y*4 + point.z*3 + point.w*2 + index;
    return mod(cos(sum1)*100 + sin(sum2)*50 + cos(sum2*0.5)*20, 1);
}

float qlerp(float val1, float val2, float dist)
{
    float ease = 6*pow(dist,5) - 15*pow(dist,4) + 10*pow(dist,3);
    return (1-ease)*val1 + ease*val2;
}

vec4 unit(vec4 v)
{
    if (dot(v,v) != 0)
    {
        return normalize(v);
    }
    return v;
}

float perlin(vec4 point)
{
    //Find the hypercube that the input point falls into, and relative position
    vec4 rel  = mod(point, 1);
    ivec4 box = ivec4(point - rel);

    //Calculate the noise value
    float[2*2*2] noise;
    for (int w = 0; w < 2; ++w)
    {
        for (int z = 0; z < 2; ++z)
        {
            for (int y = 0; y < 2; ++y)
            {
                ivec4 gradient0pos = ivec4(box.x+0, box.y+y, box.z+z, box.w+w);
                vec4 gradient0 = unit(vec4( cos(white(gradient0pos, 0)*PI*2), cos(white(gradient0pos, 1)*PI*2), cos(white(gradient0pos, 2)*PI*2), cos(white(gradient0pos, 3)*PI*2) ));

                ivec4 gradient1pos = gradient0pos + ivec4(1,0,0,0);                               
                vec4 gradient1 = unit(vec4( cos(white(gradient1pos, 0)*PI*2), cos(white(gradient1pos, 1)*PI*2), cos(white(gradient1pos, 2)*PI*2), cos(white(gradient1pos, 3)*PI*2) ));
                
                noise[w*4 + z*2 + y] = qlerp(dot(gradient0, rel - vec4(0,y,z,w)), dot(gradient1, rel - vec4(1,y,z,w)), rel.x);
            }
        }
    }
    return (qlerp(qlerp(qlerp(noise[0], noise[1], rel.y), qlerp(noise[2], noise[3], rel.y), rel.z), 
                  qlerp(qlerp(noise[4], noise[5], rel.y), qlerp(noise[6], noise[7], rel.y), rel.z), rel.w) + 1) / 2;
}

void main()
{
    vec4 pos = vec4(floor((inPos+chunkPos)*16)/16, float(time));
    float noise = 0.3 + perlin(pos)*0.5 + perlin(pos*3)*0.2;
    outCol = vec4(texture(toSample, inTex).rgb * inCol * noise, 0.8);
}

