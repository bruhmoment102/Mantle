dcOpt = -Isource --d-version=BindBC_Static --d-version=SDL_2014 --preview=in

ifeq ($(config),release)
	dcOpt += -release -O3
else
    ifeq ($(config),profile)
        dcOpt += -release -O3 -g
    else
        ifeq ($(config),dev)
            dcOpt += --d-debug -g -O3
        else
	        dcOpt += --d-debug -g
        endif
    endif
endif

ifeq ($(target),windows)
    # Configuration options for building Windows executables
    cliBin = build/mantle.exe
    serBin = build/mantle-server.exe
	dcOpt += -mtriple=x86_64-windows-mingw-w64
	objExt = obj
else
    # Assume the same options as for Linux executables by default
    cliBin = build/mantle
    serBin = build/mantle-server
	objExt = o
endif

# Actions to perform
.PHONY: client server clean

client : $(cliBin) build/base.rarc

server : $(serBin)

clean :
	$(MAKE) -C assets clean
	$(MAKE) -C tools clean
	rm -rf build

# Files to build
$(cliBin) : $(subst source/,build/source/,$(subst .d,.$(objExt),$(wildcard source/client/*.d source/client/*/*.d source/common/*.d source/common/*/*.d)))
	ldc2 --of=$@ $^ $(dcOpt) -L=-ldvec -L=-lSDL2 -L=-lerupted -link-defaultlib-shared=false
ifeq ($(config),release)
	strip --strip-unneeded $@
endif
ifeq ($(target),windows)
	cp /usr/lib/win64/SDL2.dll /usr/lib/win64/druntime-ldc-shared.dll /usr/lib/win64/phobos2-ldc-shared.dll build
endif

$(serBin) : $(subst source/,build/source/,$(subst .d,.$(objExt),$(wildcard source/server/*.d source/server/*/*.d source/common/*.d source/common/*/*.d)))
	ldc2 --of=$@ $^ $(dcOpt) -L=-ldvec -link-defaultlib-shared=false
ifeq ($(config),release)
	strip --strip-unneeded $@
endif

build/base.rarc :
	$(MAKE) -C assets
	mkdir -p build
	cp assets/base.marc build/base.marc

build/source/%.$(objExt) : source/%.d
	ldc2 --of=$@ -c $? $(dcOpt)
