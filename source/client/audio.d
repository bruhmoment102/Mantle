module client.audio;

import client.resource;
import core.memory;
import bindbc.sdl;
import core.sync.mutex;
import std.algorithm,
       std.conv,
       std.exception,
       std.format,
       std.typecons;
       
enum AudioFormat : ubyte
{
    ushortlittle,
    ushortbig
}
       
struct Audio
{
    private:
        void[] _data;
        uint _sampleRate;
        ubyte _channels;
        AudioFormat _format;
        
    public:
        this(const(void)[] audio)
        {
            //Validate the header
            enforce(audio[0..4] == "RIFF", "Invalid magic number for WAVE format data");
            enforce(*(cast(uint*)&audio[4]) == audio.length-8, "Invalid data size in WAVE header");
            enforce(audio[8..12] == "WAVE", "Invalid magic number for WAVE format data");
            enforce(audio[12..16] == "fmt ", "Invalid magic number for WAVE format chunk data");
            
            //Validate and store sample format data
            enforce(*(cast(uint*)&audio[16])   == 16 &&
                    *(cast(ushort*)&audio[20]) == 1, "Unsupported audio format");
            enforce(*(cast(ushort*)&audio[22]) <= ubyte.max, "Number of audio channels must not exceed 255");
            _channels   = cast(ubyte)*(cast(ushort*)&audio[22]);
            _sampleRate = *(cast(uint*)&audio[24]);
            enforce(*(cast(uint*)&audio[28])   == _channels * _sampleRate * 2 &&
                    *(cast(ushort*)&audio[32]) == _channels * 2, "Invalid audio format header in WAVE data");
            enforce(*(cast(ushort*)&audio[34]) == 16, format!"Unsupported sample depth of %s bits"(*(cast(ushort*)&audio[34])));
            
            //Validate the audio data header
            enforce(audio[36..40] == "data", "Invalid magic number for audio chunk of WAVE data");
            enforce(*(cast(uint*)&audio[40]) == audio[44..$].length, "Invalid data size in audio chunk header of WAVE data");
            _data = new void[*(cast(uint*)&audio[40])];
            _data[] = audio[44..$];
        }
        
        ~this() { GC.free(_data.ptr); }
}

class AudioResource : Resource
{
    Audio audio;
    alias audio this;
    
    this(const(void)[] data) { audio = Audio(data); }
}

private struct AudioStream
{
    Audio* target;
    size_t read;
    float volume;
    float targetVolume;
}

struct AudioPlayer
{
    private:
        SDL_AudioDeviceID _outputDevice;
        AudioStream[] _streams;
        
        nothrow auto addWave(short[] dst, short[] src, float volume, float targetVolume)
            in (dst.length == src.length && volume >= 0)
        {
            foreach (i; 0..dst.length)
            {
                dst[i] = cast(ushort)clamp((cast(int)src[i] * volume) + dst[i], short.min, short.max);
                volume = volume * 0.99 + targetVolume * 0.01;
            }
            return volume;
        }
        
        pragma(inline, true) nothrow void fill(ubyte* stream, int len)
        {
            stream[0..len] = 0;
            
            foreach (ref i; _streams)
            {
                //If this audio stream is being used
                if (i.target && i.target._channels == 2)
                {
                    //Copy as much sample data into the provided buffer as possible
                    auto remaining = i.target._data.length-i.read;
                    
                    if (remaining <= len)
                    {
                        //If the amount remaining is less than or equals the size of the buffer to fill, then stop this audio stream
                        i.volume = addWave(cast(short[])stream[0..remaining], cast(short[])i.target._data[i.read..i.read+remaining], i.volume, i.targetVolume);
                        i.target = null;
                    }
                    else
                    {
                        //Otherwise just copy the full length of data as needed
                        i.volume = addWave(cast(short[])stream[0..len], cast(short[])i.target._data[i.read..i.read+len], i.volume, i.targetVolume);
                        i.read += len;
                    }
                }
            }
        }
        
    public:
        void setVolumeTarget(uint stream, float newTarget)
        {
            SDL_LockAudioDevice(_outputDevice);
            _streams[stream].targetVolume = newTarget;
            SDL_UnlockAudioDevice(_outputDevice);
        }
    
        void playStream(uint streamMin, uint streamMax, ref Audio audio, float volume = 1)
        {
            if (volume > 0)
            {
                SDL_LockAudioDevice(_outputDevice);
                foreach (i; streamMin..streamMax)
                {
                    if (!_streams[i].target)
                    {
                        _streams[i] = AudioStream(&audio, 0, volume, volume);
                        break;
                    }       
                }
                SDL_UnlockAudioDevice(_outputDevice);
            }
        }
    
        void playStream(uint stream, ref Audio audio, float volume = 1) { playStream(stream, stream+1, audio, volume); }
    
        this(int sampleRate, uint playbackStreams)
            in (sampleRate > 0)
        {
            enforce(SDL_Init(SDL_INIT_AUDIO) >= 0, format!"Failed to initialise audio: %s"(SDL_GetError.to!string));
            
            //Create an audio output device and audio streams
            SDL_AudioSpec spec = void;
            with (spec)
            {
                freq     = sampleRate;
                format   = AUDIO_S16LSB;
                channels = 2;
                samples  = 2048;
                callback = function (void* userData, ubyte* stream, int len) nothrow { (cast(AudioPlayer*)userData).fill(stream, len); };
                userdata = &this;
            }
        
            _streams.length = playbackStreams;
            _outputDevice   = SDL_OpenAudioDevice(null, 0, &spec, null, 0);
            enforce(_outputDevice, format!"Failed to create audio device: %s"(SDL_GetError.to!string));
            SDL_PauseAudioDevice(_outputDevice, 0);
        }
        
        ~this()
        {
            SDL_UnlockAudioDevice(_outputDevice);
            SDL_CloseAudioDevice(_outputDevice);
            SDL_QuitSubSystem(SDL_INIT_AUDIO);
            GC.free(_streams.ptr);    
        }
}
