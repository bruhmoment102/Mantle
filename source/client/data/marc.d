module common.data.marc;

public import common.data.common;
  
/* Data structures used in Mantle ARChive files */
struct MARCHeader
{
    enum validMagic = "MARCVER0";

    char[8] magic;
    ulong numResource;

    bool valid() { return magic == validMagic && numResource > 0; }
}

struct MARCHeaderRef
{
    UUID uuid;
    ulong offset;
}

enum MARCResourceLife
{
    Temporary,
    Streamed,
    Full
}

struct MARCResourceHeader
{
    enum maxNameChars = 256;

    UUID uuid;
    UUID typeUUID;
    MARCResourceLife lifetime;
    ulong uncompressedLen;
    ulong compressedLen;
}
