module common.data.mobj;

public import common.data.common;
import dvec.vector;

struct Vertex
{
    align(16) vec3 pos;
    align(4) float extra;
    align(16) vec3 tex;
    align(4) uint color;
}

struct MOBJHeader
{
    enum validMagic = "MOBJVER0";
    
    char[8] magic;
    
    bool valid() { return magic == validMagic; }
}
