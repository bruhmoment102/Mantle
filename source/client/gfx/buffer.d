module client.gfx.buffer;

import client.gfx.device,
       client.gfx.renderer,
       client.gfx.system;
import erupted;
import std.exception,
       std.format,
       std.math;
       
/* Generic buffer type that wraps Vulkan buffer objects */
struct Buffer 
{
    private:
        VkBuffer _buf;
        VkDeviceMemory _mem;
        Device* _device;
        void* _map;
        size_t _len;
        
    package:
        ref auto device() { return *_device; }
        auto buf() { return _buf; }
        auto mem() { return _mem; }
        auto valid() { return _buf !is null; }
        auto map() out (ret; ret.ptr != null) { return _map[0.._len]; }
        
        alias buf this;
    
        this(ref Device device, VkBufferUsageFlags usageFlags, size_t len, bool local = false, string caller = __PRETTY_FUNCTION__) 
        {
            _device = &device;
            _len    = len;
            enforce(len > 0, "Length of Vulkan buffer to allocate must be greater than 0");
        
            //Create a buffer object
            VkBufferCreateInfo info;
            with (info) 
            {
                sType       = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
                size        = len;
                usage       = usageFlags;
                sharingMode = VK_SHARING_MODE_EXCLUSIVE;
            }
            vkCreateBuffer(device, &info, null, &_buf);
            
            //Create and bind a suitable memory allocation to the buffer object
            VkMemoryRequirements requirements;
            vkGetBufferMemoryRequirements(device, _buf, &requirements);
            _mem = device.malloc(requirements.size, local);
            vkBindBufferMemory(device, _buf, _mem, 0);
            
            //Map the memory allocation if a suitable memory type was selected
            if (local)
            {
                vkMapMemory(device, _mem, 0, len, 0, &_map);
            }
        }
        
    public:
        ~this() 
        {
            if (_device)
            {
                if (_map)
                {
                    vkUnmapMemory(*_device, mem);
                }
                if (_buf)
                {
                    vkDestroyBuffer(*_device, _buf, null);
                }
                _device.free(mem);   
            }    
        }
}

/* Image object type used to wrap Vulkan image objects */
struct Image
{
    private:
        VkImage _img;
        VkDeviceMemory _mem;
        VkImageView _view;
        Device* _device;
        
        /* Format metadata member variables */
        VkExtent3D _resolution;
        VkFormat _format;
        uint _layers;
        uint _mipLevels;
        bool _definedLayout;
        
        void transitionLayoutCmd(VkCommandBuffer cmd, VkImageLayout src, VkImageLayout dst, uint mipLevel, uint mipLevelCount = 1)
            in (cast(size_t)mipLevel + mipLevelCount <= _layers)
        {
            if (mipLevelCount > 0)
            {
                //Create commands for transitioning the image layout
                VkImageMemoryBarrier barrierInfo;
                VkPipelineStageFlags before;
                VkPipelineStageFlags after;
                
                with (barrierInfo)
                {
                    sType     = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
                    image     = _img;
                    oldLayout = src;
                    newLayout = dst;
                    with (subresourceRange)
                    {
                        aspectMask     = _format.aspect;
                        baseMipLevel   = mipLevel;
                        levelCount     = mipLevelCount;
                        baseArrayLayer = 0;
                        layerCount     = _layers;
                    }
                    
                    //Determine the access scopes and synchronisation scopes to use for the pipeline barrier based on the type of transition to perform
                    if (src == VK_IMAGE_LAYOUT_UNDEFINED && dst == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
                    {
                        srcAccessMask = 0;
                        dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

                        before = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
                        after  = VK_PIPELINE_STAGE_TRANSFER_BIT;
                    }
                    else if (src == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && dst == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
                    {
                        srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                        dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
                        
                        before = VK_PIPELINE_STAGE_TRANSFER_BIT;
                        after  = VK_PIPELINE_STAGE_TRANSFER_BIT;
                    }
                    else if ((src == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL || src == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) && dst == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
                    {
                        srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                        dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

                        before = VK_PIPELINE_STAGE_TRANSFER_BIT;
                        after  = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
                    }
                    else
                    {
                        throw new Exception(std.format.format!"Unrecognised image layout transition %s to %s"(src, dst));
                    }
                }
                
                vkCmdPipelineBarrier(cmd, before, after, 0, 0, null, 0, null, 1, &barrierInfo);
            }
        }
        
        void defineLayoutCmd(VkCommandBuffer cmd)
        {
            if (!_definedLayout)
            {
                transitionLayoutCmd(cmd, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 0, _mipLevels);
                _definedLayout = true;
            }
        }
        
        void pushCmd(VkCommandBuffer cmd, ref Buffer src, uint baseLayer, uint numLayers)
            in (cast(size_t)baseLayer + numLayers <= _layers)
        {
            //Add commands to the specified command buffer for pushing the staging buffer data to an image objects memory allocation
            VkBufferImageCopy copy;
            with (copy)
            {
                bufferOffset      = 0;
                bufferRowLength   = 0;
                bufferImageHeight = 0;
                with (imageSubresource)
                {
                    aspectMask     = _format.aspect;
                    mipLevel       = 0;
                    baseArrayLayer = baseLayer;
                    layerCount     = numLayers;
                }
                imageOffset = VkOffset3D(0,0,0);
                imageExtent = _resolution;
            }
            vkCmdCopyBufferToImage(cmd, src, _img, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy);
        }
        
        void genMipLevelsCmd(VkCommandBuffer cmd)
        {
            //Describe the area to copy
            VkImageBlit blitInfo;
            with (blitInfo)
            {
                with (srcSubresource)
                {
                    aspectMask     = _format.aspect;
                    baseArrayLayer = 0;
                    layerCount     = _layers;
                }
                srcOffsets[0]  = VkOffset3D(0,0,0);
                srcOffsets[1]  = VkOffset3D(_resolution.width, _resolution.height, 1);
                dstSubresource = srcSubresource;
                dstOffsets     = srcOffsets;
            }
                
            //Add commands to generate the mipmap levels
            foreach (i; 0.._mipLevels-1)
            {
                with (blitInfo)
                {
                    srcSubresource.mipLevel = i;
                    dstSubresource.mipLevel = i+1;
                    dstOffsets[1] = VkOffset3D(srcOffsets[1].x/2, srcOffsets[1].y/2, 1);
                    
                    transitionLayoutCmd(cmd, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, i);
                    vkCmdBlitImage(cmd, _img, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, _img, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &blitInfo, VK_FILTER_LINEAR);
                    
                    srcOffsets[1] = dstOffsets[1];
                }
            }
            
            //Transition all miplevel image object layouts to shader read-only optimal
            transitionLayoutCmd(cmd, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 0, _mipLevels-1);
            transitionLayoutCmd(cmd, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, _mipLevels-1);
        }

    package:
        ref auto device() { return *_device; }
        auto img() { return _img; }
        auto view() { return _view; }
        auto format() { return _format; }
        
        alias img this;

        this(ref Device device, VkImageViewType type, VkImageUsageFlags usageFlags, VkFormat valueFormat, uint w, uint h, uint layers, uint mip = 1, uint sampleCount = 1)
            in (w > 0 && h > 0 && mip > 0 && sampleCount > 0 && !((sampleCount >>> 1) & sampleCount))
        {
            _device     = &device;
            _resolution = VkExtent3D(w,h,1);
            _format     = valueFormat;
            _layers     = layers;
            _mipLevels  = mip;

            //Create a target image object
            VkImageCreateInfo imageInfo;
            with (imageInfo)
            {
                sType         = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
                flags         = (type == VK_IMAGE_VIEW_TYPE_CUBE) * VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
                imageType     = VK_IMAGE_TYPE_2D;
                format        = valueFormat;
                extent        = VkExtent3D(w,h,1);
                mipLevels     = mip;
                arrayLayers   = layers;
                samples       = cast(VkSampleCountFlagBits)sampleCount;
                tiling        = VK_IMAGE_TILING_OPTIMAL;
                usage         = VK_IMAGE_USAGE_TRANSFER_DST_BIT | (mip > 1)*VK_IMAGE_USAGE_TRANSFER_SRC_BIT | usageFlags;
                sharingMode   = VK_SHARING_MODE_EXCLUSIVE;
                initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            }
            vkCreateImage(device, &imageInfo, null, &_img);
            
            //Create and bind a suitable memory allocation to the image object
            VkMemoryRequirements requirements;
            vkGetImageMemoryRequirements(device, _img, &requirements);
            _mem = device.malloc(requirements.size);
            vkBindImageMemory(device, _img, _mem, 0);
            
            //Create an image view object referencing the image object
            VkImageViewCreateInfo viewInfo;
            with (viewInfo)
            {
                sType      = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                image      = _img;
                viewType   = type;
                format     = valueFormat;
                components = VkComponentMapping(VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY);
                with (subresourceRange)
                {
                    aspectMask     = valueFormat.aspect;
                    baseMipLevel   = 0;
                    levelCount     = mip;
                    baseArrayLayer = 0;
                    layerCount     = layers;
                }
            }
            vkCreateImageView(device, &viewInfo, null, &_view);
        }
        
    public:
        ~this()
        {
            if (_device)
            {
                _device.waitIdle;
                if (_view)
                {
                    vkDestroyImageView(*_device, _view, null);
                }
                if (_img)
                {
                    vkDestroyImage(*_device, _img, null);
                }
                _device.free(_mem);
            }
        }
}

/* Staging buffer type used to represent a host-local memory allocation for asynchronously pushing data to other buffers */
struct StagingBuffer
{
    private:
        VkCommandBuffer _pushCmd;
        VkFence _pushed;
        
        void pushCmd(VkCommandBuffer cmd, ref Buffer dst, VkBufferCopy[] copy)
            in (copy.length <= uint.max)
        {
            //Add commands to the specified command buffer for pushing the staging buffer data to another buffer objects memory allocation
            foreach (ref i; copy)
            {
                assert(i.srcOffset + i.size <= _len    , "Invalid buffer copy range for source"     );
                assert(i.dstOffset + i.size <= dst._len, "Invalid buffer copy range for destination");
            }
            vkCmdCopyBuffer(cmd, _buf, dst, cast(uint)copy.length, copy.ptr);
        }

        void allocCmd()
        {
            //Create a command buffer to use in copying data if one already does not exist. If one exists then a fence to signal when the previous has finished execution must be available.
            if (_pushed)
            {
                waitFinished;
                vkResetFences(*_device, 1, &_pushed);
            }
            
            if (!_pushCmd)
            {
                _pushCmd = _device.allocCmd;
            }
            else
            {
                _device.resetCmd(_pushCmd);
            }    
        }
        
    package:
        Buffer buffer;
        alias buffer this;
        
        void asyncPush(ref Image dst, uint baseLayer, uint numLayers, bool toReadable)
        {
            allocCmd;
            dst.defineLayoutCmd(_pushCmd);
            dst.pushCmd(_pushCmd, buffer, baseLayer, numLayers);
            
            //If this is the last image data being copied to the image object bound memory allocation, generate mipmaps and transition the layout
            if (toReadable)
            {
                dst.genMipLevelsCmd(_pushCmd);
            }
            device.asyncExecCmd(_pushCmd, _pushed);
        }
    
        void asyncPush(ref Buffer dst, VkBufferCopy[] copy ...)
        {
            allocCmd;
            pushCmd(_pushCmd, dst, copy);
            device.asyncExecCmd(_pushCmd, _pushed);
        }
        void asyncPush(ref Buffer dst)
        {
            VkBufferCopy copy;
            with (copy)
            {
                srcOffset = 0;
                dstOffset = 0;
                size      = _len;
            }
            asyncPush(dst, copy);
        }
        
        this(ref Device device, size_t len, bool signalled)
        {
            //Create a staging buffer and a fence if the staging buffer is to be signalled
            buffer = Buffer(device, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, len, true);
            if (signalled)
            {
                VkFenceCreateInfo fenceInfo;
                with (fenceInfo)
                {
                    sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
                    flags = VK_FENCE_CREATE_SIGNALED_BIT;
                }
                vkCreateFence(device, &fenceInfo, null, &_pushed);
            }        
        }
        
    public:    
        auto finished()
        {
            //Function for checking if the command buffer for copying has finished execution
            if (!_pushCmd)
            {
                return true;
            }
            assert(_pushed);
            return vkGetFenceStatus(*_device, _pushed) == VK_SUCCESS;
        }
        
        void waitFinished()
        {
            assert(_pushed);
            vkWaitForFences(buffer.device, 1, &_pushed, VK_TRUE, ulong.max);
        }
    
        ~this()
        {
            if (_device)
            {
                if (_pushed)
                {
                    waitFinished;
                    vkDestroyFence(*_device, _pushed, null);
                } 
                else
                {
                    _device.waitIdle;
                }
                _device.freeCmd(_pushCmd);
            }
        }
}
