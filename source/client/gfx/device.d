module client.gfx.device;

import client.gfx.system,
       client.gfx.window;
import erupted;
import std.conv,
       std.datetime.stopwatch,
       std.exception,
       std.parallelism;
import std.experimental.logger;

/* Logical device type, used to represent rendering/compute processing devices */
struct Device
{
    private:
        VkPhysicalDevice _physical;
        VkDevice _device;
        VkQueue[2] _queues;
        uint[2] _queueFamilies;
        VkCommandPool _cmdPool;
        uint[2] _memTypes;
        VkPhysicalDeviceFeatures _features;
        VkSampleCountFlags _msaaSupport;
        uint _maxAnisotropy;
        
        void selectDevice(VkPhysicalDevice device, ref VkPhysicalDeviceProperties prop)
        {
            _physical      = device;
            _msaaSupport   = prop.limits.framebufferColorSampleCounts & prop.limits.framebufferDepthSampleCounts;
            _maxAnisotropy = cast(uint)prop.limits.maxSamplerAnisotropy;
            
            size_t nameLen;
            foreach (e; prop.deviceName)
            {
                if (e != '\0')
                {
                    nameLen += 1;
                }
                else
                {
                    break;
                }
            }
            logf(LogLevel.info, "Selected %s for creation of logical device", prop.deviceName[0..nameLen]);
        }

        void createDevice(ref Window window) 
        {
            //Choose the physical device to use
            auto physical = getVkSlice!(vkEnumeratePhysicalDevices, VkPhysicalDevice)(instance);
            enforce(physical.length, "No Vulkan physical devices available");
            
            foreach (i; physical) 
            {
                //Select the first dedicated physical device supporting anisotropic filtering presented
                VkPhysicalDeviceProperties prop;
                vkGetPhysicalDeviceProperties(i, &prop);
                VkPhysicalDeviceFeatures features;
                vkGetPhysicalDeviceFeatures(i, &features);
                
                if (prop.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU && features.samplerAnisotropy) 
                {
                    selectDevice(i, prop);
                    break;
                }
            }
            
            if (_physical == null) 
            {
                //Select the first physical device if no suitable one was found
                VkPhysicalDeviceProperties prop;
                vkGetPhysicalDeviceProperties(physical[0], &prop);
                selectDevice(physical[0], prop);
            }

            //Find the queue families to use for rendering commands and presentation
            auto families = getVkSlice!(vkGetPhysicalDeviceQueueFamilyProperties, VkQueueFamilyProperties)(_physical);
            bool found;
            foreach (i, ref e; families)
            {
                if (e.queueFlags & VK_QUEUE_GRAPHICS_BIT)
                {
                    found = true;
                    _queueFamilies[0] = cast(uint)i;
                    break;
                }
            }
            enforce(found, "Selected physical device does not support rendering commands");
            
            VkBool32 supported;
            vkGetPhysicalDeviceSurfaceSupportKHR(_physical, _queueFamilies[0], window.surface, &supported);
            if (supported) 
            {
                //Use the same queue family for both rendering and presentation if possible
                _queueFamilies[1] = _queueFamilies[0];
            } 
            else 
            {
                foreach (i; 0..families.length) 
                {
                    vkGetPhysicalDeviceSurfaceSupportKHR(_physical, cast(uint)i, window.surface, &supported);
                    if (supported) 
                    {
                        _queueFamilies[1] = cast(uint)i;
                        break;
                    }
                }
            }
            
            //Create the logical device
            VkPhysicalDeviceFeatures features;
            with (features)
            {
                samplerAnisotropy = VK_TRUE;
            }
            
            VkDeviceQueueCreateInfo[2] queues;
            auto priority = 1.0f;
            foreach (i,e; _queueFamilies) with (queues[i]) 
            {
                queueFamilyIndex = e;
                queueCount       = 1;
                pQueuePriorities = &priority;
            }
            
            auto extensions = "VK_KHR_swapchain\0".ptr;
            VkDeviceCreateInfo deviceInfo;
            with (deviceInfo) 
            {
                sType                   = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
                queueCreateInfoCount    = _queueFamilies[0] == _queueFamilies[1] ? 1 : 2;
                pQueueCreateInfos       = queues.ptr;
                enabledExtensionCount   = 1;
                ppEnabledExtensionNames = &extensions;
                pEnabledFeatures        = &features;
            }
            vkCreateDevice(_physical, &deviceInfo, null, &_device);
            
            //Obtain handles to the created command queues
            vkGetDeviceQueue(_device, _queueFamilies[0], 0, &_queues[0]);
            vkGetDeviceQueue(_device, _queueFamilies[1], 0, &_queues[1]);
            
            //Choose host-usable and device-local memory types to use for memory allocations
            VkPhysicalDeviceMemoryProperties memProp;
            vkGetPhysicalDeviceMemoryProperties(_physical, &memProp);
            foreach (local, flags; [VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT])
            {
                foreach (uint i, ref e; memProp.memoryTypes) 
                {
                    if ((e.propertyFlags & flags) == flags) 
                    {
                        _memTypes[local] = i;
                        break;
                    }
                }
            }
        }
        
        void createCommandPool() 
        {
            //Create a command pool
            VkCommandPoolCreateInfo poolInfo;
            with (poolInfo) 
            {
                sType            = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
                flags            = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
                queueFamilyIndex = _queueFamilies[1];
            }
            
            vkCreateCommandPool(_device, &poolInfo, null, &_cmdPool);
        }
        
    package:
        auto physical() { return _physical; }
        auto device() { return _device; }
        auto renderQueue() { return _queues[0]; }
        auto presentQueue() { return _queues[1]; }
        auto renderFamily() { return _queueFamilies[0]; }
        auto presentFamily() { return _queueFamilies[1]; }
        
        alias device this;
        
        auto malloc(size_t len, bool local = false)
        {
            VkMemoryAllocateInfo alloc;
            with (alloc) 
            {
                sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
                allocationSize  = len;
                memoryTypeIndex = _memTypes[local];
            }
            
            VkDeviceMemory toReturn;
            vkAllocateMemory(device, &alloc, null, &toReturn);
            return toReturn;
        }
        
        auto free(VkDeviceMemory mem)
        {
            if (mem)
            {
                vkFreeMemory(_device, mem, null);
            }
        }
        
        void allocCmds(VkCommandBuffer[] cmd, VkCommandBufferUsageFlags usage = 0, VkCommandBufferLevel type = VK_COMMAND_BUFFER_LEVEL_PRIMARY)
            in (cmd.length <= uint.max)
        {
            //Allocate a command buffer to use
            VkCommandBufferAllocateInfo allocInfo;
            with (allocInfo)
            {
                sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
                commandPool        = _cmdPool;
                level              = type;
                commandBufferCount = cast(uint)cmd.length;
            }
            
            vkAllocateCommandBuffers(_device, &allocInfo, cmd.ptr);
            
            //Begin the recording of the command buffer and return its handle
            foreach (i; cmd)
            {
                VkCommandBufferBeginInfo beginInfo;
                with (beginInfo)
                {
                    sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
                    flags = usage;
                }
                
                vkBeginCommandBuffer(i, &beginInfo);
            }
        }
        
        auto allocCmd(VkCommandBufferUsageFlags usage = 0, VkCommandBufferLevel type = VK_COMMAND_BUFFER_LEVEL_PRIMARY)
        {
            VkCommandBuffer cmd;
            allocCmds((&cmd)[0..1], usage, type);
            return cmd;
        }
        auto allocTempCmd(VkCommandBufferUsageFlags usage = 0) { return allocCmd(usage | VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT, VK_COMMAND_BUFFER_LEVEL_PRIMARY); }
        
        void freeCmd(VkCommandBuffer cmd) { vkFreeCommandBuffers(_device, _cmdPool, 1, &cmd); }
        
        void resetCmd(VkCommandBuffer cmd, VkCommandBufferUsageFlags usage = 0)
        {
            //Reset a command buffer and continue its recording
            vkResetCommandBuffer(cmd, 0);
            
            VkCommandBufferBeginInfo beginInfo;
            with (beginInfo)
            {
                sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
                flags = usage;
            }
            vkBeginCommandBuffer(cmd, &beginInfo);
        }
        
        void endCmd(VkCommandBuffer cmd)
        {
            vkEndCommandBuffer(cmd);
        }
        
        void asyncExecCmd(VkCommandBuffer cmd, VkFence toSignal = null)
        {
            endCmd(cmd);

            //Submit the command buffer for execution
            VkSubmitInfo submitInfo;
            with (submitInfo) 
            {
                sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
                waitSemaphoreCount   = 0;
                pWaitSemaphores      = null;
                signalSemaphoreCount = 0;
                pSignalSemaphores    = null;
                pWaitDstStageMask    = [0U].ptr;
                commandBufferCount   = 1;
                pCommandBuffers      = &cmd;
            }
            vkQueueSubmit(_queues[0], 1, &submitInfo, toSignal);
        }
        
    public:
        uint msaaSupport(uint samples)
            in (!((samples >>> 1) & samples))
        {
            while (samples > 0)
            {
                if (_msaaSupport & samples)
                {
                    return samples;                    
                }
                samples >>>= 1;
            }
            assert(false, "Invalid sample count support query");
        }
    
        void waitIdle()
        {
            debug
            {
                StopWatch timer;
                timer.start;
                vkDeviceWaitIdle(_device);
                logf(LogLevel.trace, "Waited %s nanoseconds for device to finish work", timer.peek.total!"nsecs");
            }
            else
            {
                vkDeviceWaitIdle(_device);
            }
        }
    
        this(ref Window window)
        {
            createDevice(window);
            createCommandPool;
        }
        
        @disable this();
    
        ~this()
        {
            if (_device)
            {
                waitIdle;
                vkDestroyCommandPool(_device, _cmdPool, null);
                vkDestroyDevice(_device, null);
            }
        }
}
