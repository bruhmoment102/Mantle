module client.gfx.model;

import client.gfx.buffer,
       client.gfx.pipeline,
       client.gfx.renderer,
       client.gfx.texture;
import client.resource;
import dvec.matrix,
       dvec.vector;
import erupted;
import std.exception,
       std.format;

/* Model staging buffer type that allows for vertex data to be easily constructed directly in a staging buffer */
struct StagingModel
{
    private:
        StagingBuffer _buf;
        uint _curIndexLength;
        uint _maxIndexLength;
        uint _curVertexLength;
        uint _maxVertexLength;
        
        alias _buf this;

    public:
        auto finished() { return _buf.finished; }
        auto waitFinished() { _buf.waitFinished; }
        auto curIndexLength() { return _curIndexLength; }
        auto curVertexLength() { return _curVertexLength; }
    
        auto getIndex(uint len)
        {
            assert(_curIndexLength+len <= _maxIndexLength);
            _curIndexLength += len;
            return (cast(uint[])_buf.map)[_curIndexLength-len .. _curIndexLength];
        }
        
        auto getVertex(uint len)
        {
            assert((cast(uint)_curVertexLength+len) <= cast(uint)_maxVertexLength);
            _curVertexLength += len;
            return (cast(Vertex[])(_buf.map[uint.sizeof*_maxIndexLength..$]))[_curVertexLength-len .. _curVertexLength];
        }
        
        auto reset()
        {
            _curIndexLength = 0;
            _curVertexLength = 0;
        }
    
        this(ref Renderer renderer, uint maxIndexLength, uint maxVertexLength, bool signalled)
        {
            _buf = StagingBuffer(renderer, uint.sizeof*maxIndexLength + Vertex.sizeof*maxVertexLength, signalled);
            _maxIndexLength = maxIndexLength;
            _maxVertexLength = maxVertexLength;
        }
}

auto insertCubeVertex(ref StagingModel staging, in vec3 pos, in vec3 end)
{
    //Insert the vertex data for an axis-aligned cube into a staging model buffer
    auto vertex = staging.getVertex(8);
    vertex[0] = Vertex(vec3(end.x, pos.y, end.z));
    vertex[1] = Vertex(vec3(end.x, pos.y, pos.z));
    vertex[2] = Vertex(vec3(end.x, end.y, end.z));
    vertex[3] = Vertex(vec3(end.x, end.y, pos.z));
    vertex[4] = Vertex(vec3(pos.x, pos.y, end.z));
    vertex[5] = Vertex(vec3(pos.x, pos.y, pos.z));
    vertex[6] = Vertex(vec3(pos.x, end.y, end.z));
    vertex[7] = Vertex(vec3(pos.x, end.y, pos.z));
    return vertex;
}

auto normaliseExtent(ref Renderer renderer, in ivec2 extent)
{
    //Normalise the passed extent coordinates
    return vec2((cast(float)2*extent.x)/renderer.w, (cast(float)2*extent.y)/renderer.h);
}

auto normalisePos(ref Renderer renderer, in ivec2 pos)
{
    //Normalise the passed coordinates to create coordinates that are suitable for clip space
    return vec2((cast(float)2*pos.x)/renderer.w - 1, -(cast(float)2*pos.y)/renderer.h + 1);
}

auto insertBox(ref Renderer renderer, ref StagingModel staging, in ivec2 pos, in ivec2 end, in vec2 texPos, in vec2 texEnd)
{
    //Insert the vertex and index data for a square into a staging model buffer
    auto normPos = normalisePos(renderer, pos);
    auto normEnd = normalisePos(renderer, end);
    
    auto vertOffset = staging.curVertexLength;
    auto vertex = staging.getVertex(4);
    vertex[0] = Vertex(vec3(normPos.x, normPos.y, 0), vec3(texPos.x, texPos.y, 0), 0, uint.max);
    vertex[1] = Vertex(vec3(normEnd.x, normPos.y, 0), vec3(texEnd.x, texPos.y, 0), 0, uint.max);
    vertex[2] = Vertex(vec3(normPos.x, normEnd.y, 0), vec3(texPos.x, texEnd.y, 0), 0, uint.max);
    vertex[3] = Vertex(vec3(normEnd.x, normEnd.y, 0), vec3(texEnd.x, texEnd.y, 0), 0, uint.max);
    
    auto index = staging.getIndex(6);
    index[] = cast(uint[6])[0,1,2,1,3,2];
    index[] += vertOffset;
}

/* Model type representing a model index and vertex buffer with associated metadata to use in rendering */
struct Model(uint materialCount = 1)
{
    private:
        Buffer _model;
        Renderer* _renderer;
        uint[materialCount] _indexOffset;
        
        void setup(ref Renderer renderer, size_t dataLength, uint indexOffset, uint[] split)
        {
            _renderer = &renderer;
            if (dataLength)
            {
                _model = Buffer(renderer, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, dataLength);
            }
            
            //Save per-material offset data
            assert(indexOffset <= dataLength/uint.sizeof);
            _indexOffset[0..$-1] = split;
            _indexOffset[$-1] = indexOffset;
        }
        
    public:
        void renderRange(uint indexCount, uint indexOffset = 0)
        {
            if (indexCount)
            {
                assert(indexOffset+indexCount <= _indexOffset[$-1], format!"Invalid index range for rendering, %s-%s is not within %s indices"(indexOffset, indexOffset+indexCount, _indexOffset[$-1]));
                auto cmd = _renderer.frameCmd;
                vkCmdBindIndexBuffer(cmd, _model.buf, 0, VK_INDEX_TYPE_UINT32);
                
                auto buf = _model.buf;
                auto offset = uint.sizeof*_indexOffset[$-1];
                vkCmdBindVertexBuffers(cmd, 0, 1, &buf, &offset);
                
                vkCmdDrawIndexed(cmd, indexCount, 1, indexOffset, 0, 0);
            }
        }
        
        void renderRange(in mat4 transform, uint indexCount, uint indexOffset = 0)
        {
            if (indexCount)
            {
                vkCmdPushConstants(_renderer.frameCmd, _renderer.pipeLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, mat4.sizeof, &transform);
                renderRange(indexCount, indexOffset);
            }
        }
        
        void render(uint material = 0)
        {
            auto offset = material > 0 ? _indexOffset[material-1] : 0;
            renderRange(_indexOffset[material] - offset, offset);
        }
        
        void render(in mat4 transform, uint material = 0) 
        {
            auto offset = material > 0 ? _indexOffset[material-1] : 0;
            renderRange(transform, _indexOffset[material] - offset, offset);
        }
        
        this(string caller = __PRETTY_FUNCTION__)(ref Renderer renderer, ref StagingModel src, uint[] split ...)
            in (split.length == materialCount-1)
        {
            setup(renderer, src.curIndexLength*uint.sizeof + src.curVertexLength*Vertex.sizeof, src._curIndexLength, split);
            VkBufferCopy[2] copy;
            with (copy[0])
            {
                srcOffset = 0;
                dstOffset = 0;
                size      = uint.sizeof*_indexOffset[$-1];
            }
            with (copy[1])
            {
                srcOffset = uint.sizeof*src._maxIndexLength;
                dstOffset = copy[0].size;
                size      = Vertex.sizeof*src._curVertexLength;
            }

            if (copy[0].size)
            {
                src.asyncPush(_model, copy);
            }
        }
}

class ModelResource : Resource
{
    Model!() model;
    alias model this;
    
    this(ref Renderer renderer, ref StagingModel toPush, ref ResourceStore resources, void[] data)
    {
        import client.data.mobj;
    
        //Validate the header of the ROBJ data
        auto header = cast(MOBJHeader*)data.ptr;
        enforce(header.valid, "Invalid header for MOBJ model data");
        
        //Copy the index and vertex buffers to a physical device local memory allocation
        auto indexCount = *cast(uint*)(&data[MOBJHeader.sizeof]);
        auto indexBegin = MOBJHeader.sizeof+uint.sizeof;
        auto indexEnd = indexBegin + (uint.sizeof*indexCount);
        
        auto vertCount = data.length - indexEnd;
        enforce(!(vertCount % client.gfx.pipeline.Vertex.sizeof), "Invalid length of MOBJ model data");
        vertCount /= client.gfx.pipeline.Vertex.sizeof; 
        toPush = StagingModel(renderer, indexCount, cast(uint)vertCount, false);

        toPush.getIndex(indexCount)[] = cast(uint[])data[indexBegin .. indexEnd];
        toPush.getVertex(cast(uint)vertCount)[] = cast(client.gfx.pipeline.Vertex[])data[indexEnd .. $];
        model = Model!()(renderer, toPush);
    }
}

/* Model type used to represent models whose vertex data will be updated every frame */
struct PerFrameModel
{
    private:
        Buffer _model;
        Renderer* _renderer;
        uint _indexOffset;
        uint _vertOffset;
        
        auto map()
        {
            auto size = _model.map.length/_renderer.swapImages;
            auto offset = size * _renderer.swapImage;
            return _model.map[offset .. offset+size];
        }
        
    public:
        auto index()  { return cast(uint[])  map[0..uint.sizeof*_indexOffset]; }
        auto vertex() { return cast(Vertex[])map[uint.sizeof*_indexOffset..$]; }
    
        void render()
        {
            if (_indexOffset)
            {
                auto cmd = _renderer.frameCmd;
                auto offset = (_model.map.length/_renderer.swapImages) * _renderer.swapImage;
                vkCmdBindIndexBuffer(cmd, _model.buf, offset, VK_INDEX_TYPE_UINT32);
                
                auto buf = _model.buf;
                auto vertOffset = offset + uint.sizeof*_indexOffset;
                vkCmdBindVertexBuffers(cmd, 0, 1, &buf, &vertOffset);
                
                vkCmdDrawIndexed(cmd, cast(uint)_indexOffset, 1, 0, 0, 0);
            }
        }
    
        void render(in mat4 transform)
        {
            if (_indexOffset)
            {
                vkCmdPushConstants(_renderer.frameCmd, _renderer.pipeLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, mat4.sizeof, &transform);
                render;
            }
        }
        
        void build() { _model = Buffer(*_renderer, VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, (uint.sizeof*_indexOffset + Vertex.sizeof*_vertOffset)*_renderer.swapImages, true); }
        
        this(ref Renderer renderer, uint index, uint vertex)
        {
            _renderer = &renderer;
            _indexOffset = index;
            _vertOffset = vertex;
            _model = Buffer(*_renderer, VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, (uint.sizeof*index + Vertex.sizeof*vertex)*_renderer.swapImages, true);
        }
}
