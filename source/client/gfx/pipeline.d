module client.gfx.pipeline;

import client.gfx.device,
       client.gfx.renderer;
import client.resource;
import dvec.matrix,
       dvec.vector;
import erupted;
import std.algorithm,
       std.exception,
       std.string;
       
/* Shader types representing programs to be executed as part of a compute or graphics pipeline */
enum ShaderType
{
    Vertex,
    Fragment
}

struct Shader
{
    private:
        VkShaderModule _module;
        ShaderType _type;
        Device* _device;
    
    public:
        package auto shaderModule() { return _module; }
        package auto type()         { return _type;   }

        this(ref Device device, void[] data, ShaderType type)
        {
            _device = &device;
            _type = type;
            
            //Create a shader module object using the given bytecode
            VkShaderModuleCreateInfo moduleInfo;
            with (moduleInfo)
            {
                assert(data.length <= uint.max, "Shader bytecode exceeds maximum length allowed by Vulkan");
            
                sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
                codeSize = cast(uint)data.length;
                pCode    = cast(uint*)data.ptr;
            }
            vkCreateShaderModule(device, &moduleInfo, null, &_module);
        }
        
        @disable this();
        
        ~this()
        {
            if (_device)
            {
                vkDestroyShaderModule(*_device, _module, null); 
            }
        }
}

class ShaderResource : Resource
{
    Shader shader;
    
    alias shader this;
    
    this(ref Device device, ShaderType shaderType, void[] data)
    {
        shader = Shader(device, data, shaderType);
    }
}

/* Vertex structure used in vertex buffers processed by graphics pipelines */
struct Vertex
{
    align(16) vec3 pos;
    align(4) uint norm;
    align(16) vec3 tex;
    align(4) uint color;
    
    this(in vec3 pos, in vec3 tex = vec3.init, in uint norm = uint.init, in uint color = this.color.init)
    {
        this.pos   = pos;
        this.norm  = norm;
        this.tex   = tex;
        this.color = color;
    }
}

enum vertNorm(vec3 val) = ((((cast(uint)( (val.x+1) * 511.5 ))) & 0x3FF) << 20) | 
                          ((((cast(uint)( (val.y+1) * 511.5 ))) & 0x3FF) << 10) |
                            ((cast(uint)( (val.z+1) * 511.5 ))  & 0x3FF);

/* Types used to manage graphics pipelines */
enum GfxPipelineFlags
{
    None         = 0x00,
    NoDepthTest  = 0x01,
    NoDepthWrite = 0x02,
    Line         = 0x04,
    NoVertexData = 0x08,
    NoCulling    = 0x10,
}

struct GfxPipelineConfig
{
    private:
        VkPipelineVertexInputStateCreateInfo vertexConfig;
        VkPipelineDepthStencilStateCreateInfo depthConfig;
        VkPipelineInputAssemblyStateCreateInfo assemblyConfig;
        VkPipelineRasterizationStateCreateInfo rasterConfig;
        
        VkPipelineShaderStageCreateInfo vertShader;
        VkPipelineShaderStageCreateInfo fragShader;
        
    public:
        this(GfxPipelineFlags config, ref Shader vertex, ref Shader fragment)
        {
            //Configure vertex binding and attribute descriptions for this pipeline
            with (vertexConfig)
            {
                sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
                if (config & GfxPipelineFlags.NoVertexData)
                {
                    vertexBindingDescriptionCount   = 0;
                    vertexAttributeDescriptionCount = 0;
                }
                else
                {
                    vertexBindingDescriptionCount   = 1;
                    vertexAttributeDescriptionCount = (config & GfxPipelineFlags.Line) ? 2 : 4;
                }
            }
            
            with (depthConfig)
            {
                sType            = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
                depthTestEnable  = !cast(bool)(config & GfxPipelineFlags.NoDepthTest );
                depthWriteEnable = !cast(bool)(config & GfxPipelineFlags.NoDepthWrite);
                depthCompareOp   = VK_COMPARE_OP_LESS_OR_EQUAL;
            }
            
            with (assemblyConfig)
            {
                sType                  = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
                topology               = (config & GfxPipelineFlags.Line) ? VK_PRIMITIVE_TOPOLOGY_LINE_LIST : VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
                primitiveRestartEnable = false;
            }
            
            with (rasterConfig)
            {
                sType          = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
                polygonMode    = VK_POLYGON_MODE_FILL;
                depthBiasClamp = 0.0;
                lineWidth      = 1.0;
                frontFace      = VK_FRONT_FACE_COUNTER_CLOCKWISE;
                cullMode       = (config & GfxPipelineFlags.NoCulling) ? 0 : VK_CULL_MODE_BACK_BIT;
            }
            
            //Configure the shader stages of the graphics pipeline
            with (vertShader)
            {
                stage  = VK_SHADER_STAGE_VERTEX_BIT;
                Module = vertex.shaderModule;
                pName  = "main\0".ptr;
            }
            
            with (fragShader)
            {
                stage  = VK_SHADER_STAGE_FRAGMENT_BIT;
                Module = fragment.shaderModule;
                pName  = "main\0".ptr;
            }
            
            //Configure the overall graphics pipeline
            VkGraphicsPipelineCreateInfo pipelineInfo;
            with (pipelineInfo)
            {
                sType               = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
                stageCount          = 2;
                pStages             = &vertShader;
                pVertexInputState   = &vertexConfig;
                pInputAssemblyState = &assemblyConfig;
                pDepthStencilState  = &depthConfig;
                subpass             = 0;
            }
        }
}

struct GfxPipeline
{
    private:
        VkPipeline _pipeline;
        Renderer* _renderer;
        
    public:
        package auto pipeline() { return _pipeline; }
        ref auto renderer() { return *_renderer; }
        
        alias pipeline this;
        
        void bind() { vkCmdBindPipeline(_renderer.frameCmd, VK_PIPELINE_BIND_POINT_GRAPHICS, _pipeline); }
    
        void render(uint vertCount) { vkCmdDraw(_renderer.frameCmd, vertCount, 1, 0, 0); }
        
        void renderRange(in mat4 transform, uint vertCount)
        {
            vkCmdPushConstants(_renderer.frameCmd, _renderer.pipeLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, mat4.sizeof, &transform);
            render(vertCount);
        }

        ~this()
        {
            if (_renderer)
            {
                _renderer.waitIdle;
                vkDestroyPipeline(*_renderer, _pipeline, null);
            }
        }
}

private void createGfxPipelines(ref Renderer renderer, uint n, VkPipeline* toFill, VkGraphicsPipelineCreateInfo* pipeInfo, GfxPipelineConfig* config)
{
    //Configure how vertex buffers should be used
    VkVertexInputBindingDescription bindingConfig;
    with (bindingConfig)
    {
        binding   = 0;
        stride    = Vertex.sizeof;
        inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    }
    
    VkVertexInputAttributeDescription[4] attribConfig;
    with (attribConfig[0])
    {
        binding  = 0;
        location = 0;
        format   = VK_FORMAT_R32G32B32_SFLOAT;
        offset   = 0;
    }
    with (attribConfig[1])
    {
        binding  = 0;
        location = 2;
        format   = VK_FORMAT_R32G32B32_SFLOAT;
        offset   = 16;
    }
    with (attribConfig[2])
    {
        binding  = 0;
        location = 1;
        format   = VK_FORMAT_R32_UINT;
        offset   = 12;
    }
    with (attribConfig[3])
    {
        binding  = 0;
        location = 3;
        format   = VK_FORMAT_R32_UINT;
        offset   = 28;
    }
    
    //Correct the configuration structures of each of the pipelines to use the vertex binding and attribute descriptions
    foreach (i, ref e; config[0..n])
    {
        with (e.vertexConfig)
        {
            pVertexBindingDescriptions   = &bindingConfig;
            pVertexAttributeDescriptions = attribConfig.ptr;
        }
    }
    
    //Configure the rest of the fixed function stages of the pipeline
    VkPipelineViewportStateCreateInfo viewportConfig;
    with (viewportConfig)
    {
        sType         = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportCount = 1;
        scissorCount  = 1;
    }

    VkPipelineMultisampleStateCreateInfo multisampleConfig;
    with (multisampleConfig)
    {
        sType                = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        rasterizationSamples = renderer.msaaSamples;
    }
    
    VkPipelineColorBlendAttachmentState attachmentBlend;
    with (attachmentBlend)
    {
        colorWriteMask      = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        blendEnable         = VK_TRUE;
        srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
        dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        colorBlendOp        = VK_BLEND_OP_ADD;
    }
    
    VkPipelineColorBlendStateCreateInfo blendConfig;
    with (blendConfig)
    {
        sType           = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        attachmentCount = 1;
        pAttachments    = &attachmentBlend;
    }
    
    auto dynamicState =  cast(VkDynamicState[2])[VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR];
    VkPipelineDynamicStateCreateInfo dynamicConfig;
    with (dynamicConfig)
    {
        sType             = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        dynamicStateCount = 2;
        pDynamicStates    = dynamicState.ptr;
    }
        
    //Configure and create the graphics pipelines
    foreach (i, ref e; config[0..n])
    {
        with (pipeInfo[i])
        {
            sType               = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
            stageCount          = 2;
            pStages             = &e.vertShader;
            pVertexInputState   = &e.vertexConfig;
            pInputAssemblyState = &e.assemblyConfig;
            pDepthStencilState  = &e.depthConfig;
            pViewportState      = &viewportConfig;
            pRasterizationState = &e.rasterConfig;
            pMultisampleState   = &multisampleConfig;
            pColorBlendState    = &blendConfig;
            pDynamicState       = &dynamicConfig;
            layout              = renderer.pipeLayout;
            renderPass          = renderer.pass;
            subpass             = 0;
        }
    }
    vkCreateGraphicsPipelines(renderer, null, n, pipeInfo, null, toFill);
}

void createGfxPipelines(uint n)(ref Renderer renderer, ref GfxPipeline[n] toFill, GfxPipelineConfig[] config ...)
    in (config.length == n)
{
    //Create graphics pipelines
    VkGraphicsPipelineCreateInfo[n] info;
    VkPipeline[n] handles;
    createGfxPipelines(renderer, n, handles.ptr, info.ptr, config.ptr);
    
    //Create valid GfxPipeline structures from these newly created graphics pipelines
    foreach (i, ref e; toFill)
    {
        e._pipeline = handles[i];
        e._renderer = &renderer;
    }
}

void createGfxPipeline(ref Renderer renderer, ref GfxPipeline toFill, GfxPipelineConfig[] config ...) { renderer.createGfxPipelines(*(cast(GfxPipeline[1]*)&toFill), config); }
