module client.gfx.renderer;

import client.gfx.buffer,
       client.gfx.device,
       client.gfx.system,
       client.gfx.window;
import dvec.matrix;
import erupted;
import std.algorithm,
       std.exception,
       std.format;
import std.experimental.logger;

/* Renderer type used to represent a logical device with rendering state */
struct Renderer 
{
    private:
        Device _device;
        Window* _window;
        
        /* Swapchain related member variables */
        VkSwapchainKHR _swap;
        VkImageView[] _swapViews;
        VkFramebuffer[] _framebuffers;
        VkSurfaceFormatKHR _format;
        VkPresentModeKHR _present;
        VkRenderPass _pass;
        VkPipelineLayout _pipeLayout;
        VkExtent2D _resolution;
        
        /* Texture/image object related member variables */
        VkDescriptorPool _descriptors;
        VkDescriptorSetLayout[2] _descriptorLayouts;
        
        /* Rendering related member variables */
        uint _nextSwapImage;
        uint _curFrame;
        
        public enum numFramesInFlight = 3;
        VkCommandBuffer[numFramesInFlight] _renderCmd;
        VkSemaphore[numFramesInFlight] _swapImageObtained;
        VkSemaphore[numFramesInFlight] _swapImageRendered;
        VkFence[numFramesInFlight] _frameDone;
        
        Image _colorBuf;
        Image _depthBuf;
        VkFormat _depthFormat;
        VkSampler _sampler;
        VkSampleCountFlagBits _msaaSamples;
        bool _mipSupport;
        bool _resChanged;
        
        void updateResolution() 
        {
            VkSurfaceCapabilitiesKHR prop;
            vkGetPhysicalDeviceSurfaceCapabilitiesKHR(_device.physical, _window.surface, &prop);
            if (prop.currentExtent == VkExtent2D(uint.max, uint.max)) 
            {
                _resolution = VkExtent2D(_window.w, _window.h);
            } 
            else 
            {
                _resolution = prop.currentExtent;
            }
        }
        
        auto chooseSwapFormat() 
        {
            auto formats  = getVkSlice!(vkGetPhysicalDeviceSurfaceFormatsKHR, VkSurfaceFormatKHR)(_device.physical, _window.surface);
            auto toReturn = formats[0];
            
            //Choose the first format presented, or the highest in a list of preffered formats
            outer: foreach (i; [VK_FORMAT_R8G8B8_SRGB, VK_FORMAT_R8G8B8A8_SRGB, VK_FORMAT_B8G8R8A8_SRGB]) 
            {
                foreach (ref format; formats) 
                {
                    if (format.format == i) 
                    {
                        toReturn = format;
                        break outer;
                    }
                }
            }
            return toReturn;
        }
        
        auto chooseFormat(VkFormatFeatureFlags usage, VkFormat[] possible...)
        {
            foreach (i; possible)
            {
                VkFormatProperties formatProp;
                vkGetPhysicalDeviceFormatProperties(_device.physical, i, &formatProp);
                
                if (formatProp.optimalTilingFeatures & usage)
                {
                    return i;
                }
            }
            throw new Exception(format!"Failed to find Vulkan attachment format with %s that is compatible with physical device"(usage));
        }
        
        auto choosePresent(bool vsync)
        {
            outer: foreach (i; vsync ? [VK_PRESENT_MODE_FIFO_RELAXED_KHR, VK_PRESENT_MODE_FIFO_KHR] : [VK_PRESENT_MODE_IMMEDIATE_KHR])
            {
                foreach (e; getVkSlice!(vkGetPhysicalDeviceSurfacePresentModesKHR, VkPresentModeKHR)(_device.physical, _window.surface)) 
                {
                    if (e == i) 
                    {
                        return e;
                    }
                }
            }
            throw new Exception("No suitable swapchain presentation mode available");
        }
        
        void createSampler(uint anisotropy)
        {
            VkSamplerCreateInfo samplerInfo;
            with (samplerInfo)
            {
                sType                   = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
                magFilter               = VK_FILTER_NEAREST;
                minFilter               = magFilter;
                mipmapMode              = VK_SAMPLER_MIPMAP_MODE_LINEAR;
                addressModeU            = VK_SAMPLER_ADDRESS_MODE_REPEAT;
                addressModeV            = addressModeU;
                addressModeW            = addressModeV;
                mipmapMode              = VK_SAMPLER_MIPMAP_MODE_LINEAR;
                mipLodBias              = 0;
                minLod                  = 0;
                maxLod                  = 1000;
                anisotropyEnable        = anisotropy > 0;
                maxAnisotropy           = anisotropy;
            }
            vkCreateSampler(_device, &samplerInfo, null, &_sampler);
        }
        
        void createSwapchain() 
        {
            //Obtain window surface capabilities
            VkSurfaceCapabilitiesKHR surfaceCap;
            vkGetPhysicalDeviceSurfaceCapabilitiesKHR(_device.physical, *_window, &surfaceCap);
            
            auto numSurface = max(surfaceCap.minImageCount, 2);
            if (surfaceCap.maxImageCount != 0) 
            {
                numSurface = min(numSurface, surfaceCap.maxImageCount);
            }
            
            updateResolution;
        
            //Create a swapchain for the window the renderer should be attached to
            VkSwapchainCreateInfoKHR swapInfo;
            with (swapInfo) 
            {
                sType                 = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
                surface               = *_window;
                minImageCount         = numSurface;
                imageFormat           = _format.format;
                imageColorSpace       = _format.colorSpace;
                imageExtent           = _resolution;
                imageArrayLayers      = 1;
                imageUsage            = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
                imageSharingMode      = device.renderFamily != device.presentFamily ? VK_SHARING_MODE_CONCURRENT : VK_SHARING_MODE_EXCLUSIVE;
                queueFamilyIndexCount = 2;
                pQueueFamilyIndices   = [device.renderFamily, device.presentFamily].ptr;
                presentMode           = _present;
                clipped               = VK_TRUE;
            }
            
            VkSwapchainKHR toReturn;
            vkCreateSwapchainKHR(_device, &swapInfo, null, &_swap);
            
            //Create a depth attachment to combine with the swapchain images
            _colorBuf = Image(_device, VK_IMAGE_VIEW_TYPE_2D, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, _format.format, _resolution.width, _resolution.height, 1, 1, _msaaSamples);
            _depthBuf = Image(_device, VK_IMAGE_VIEW_TYPE_2D, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, _depthFormat, _resolution.width, _resolution.height, 1, 1, _msaaSamples);

            //Create image view and framebuffer objects for each swapchain image
            auto swapImages = getVkSlice!(vkGetSwapchainImagesKHR, VkImage)(_device.device, _swap);
            
            VkImageViewCreateInfo viewInfo;
            with (viewInfo)
            {
                sType            = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                viewType         = VK_IMAGE_VIEW_TYPE_2D;
                format           = _format.format;
                components       = VkComponentMapping(VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY);
                subresourceRange = VkImageSubresourceRange(VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1);
            }
            
            VkImageView[3] attachments;
            attachments[1] = _depthBuf.view;
            
            VkFramebufferCreateInfo fbInfo;
            with (fbInfo)
            {
                sType           = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
                renderPass      = _pass;
                width           = _resolution.width;
                height          = _resolution.height;
                layers          = 1;
                pAttachments    = attachments.ptr;
            }
            
            _swapViews.length    = swapImages.length;
            _framebuffers.length = swapImages.length;
            
            if (_msaaSamples > 1)
            {
                attachments[0] = _colorBuf.view;
                fbInfo.attachmentCount = 3;
                
                foreach (i,e; swapImages)
                {
                    //Create an image view object
                    viewInfo.image = e;
                    vkCreateImageView(_device, &viewInfo, null, &_swapViews[i]);
                    
                    //Create a framebuffer object
                    attachments[2] = _swapViews[i];
                    vkCreateFramebuffer(_device, &fbInfo, null, &_framebuffers[i]);
                }
            }
            else
            {
                fbInfo.attachmentCount = 2;
                
                foreach (i,e; swapImages)
                {
                    //Create an image view object
                    viewInfo.image = e;
                    vkCreateImageView(_device, &viewInfo, null, &_swapViews[i]);
                    
                    //Create a framebuffer object
                    attachments[0] = _swapViews[i];
                    vkCreateFramebuffer(_device, &fbInfo, null, &_framebuffers[i]);
                }
            }
        }
        
        void destroySwapchain()
        {
            foreach (i; _framebuffers)
            {
                vkDestroyFramebuffer(_device, i, null);
            }
            foreach (i; _swapViews)
            {
                vkDestroyImageView(_device, i, null);
            }
            vkDestroySwapchainKHR(_device, _swap, null);
        }
        
        void createRenderPass()
        {
            //Describe the attachments to use in the render pass
            VkAttachmentDescription[3] attachInfo;
            with (attachInfo[0])
            {
                //Main color attachment which is rendered with
                format        = _format.format;
                samples       = _msaaSamples;
                loadOp        = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
                storeOp       = VK_ATTACHMENT_STORE_OP_STORE;
                initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            }

            with (attachInfo[1])
            {
                //Depth attachment
                format        = _depthFormat;
                samples       = _msaaSamples;
                loadOp        = VK_ATTACHMENT_LOAD_OP_CLEAR;
                storeOp       = VK_ATTACHMENT_STORE_OP_STORE;
                initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                finalLayout   = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
            }
            
            if (_msaaSamples > 1)
            {
                attachInfo[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                with (attachInfo[2])
                {
                    //Resolve attachment for main color attachment
                    format        = _format.format;
                    samples       = VK_SAMPLE_COUNT_1_BIT;
                    loadOp        = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
                    storeOp       = VK_ATTACHMENT_STORE_OP_STORE;
                    initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                    finalLayout   = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
                }
            }
            else
            {
                attachInfo[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
            }
            
            //Create references to the attachment descriptions
            VkAttachmentReference[3] attachRef;
            with (attachRef[0])
            {
                attachment = 0;
                layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
            }
            
            with (attachRef[1])
            {
                attachment = 1;
                layout     = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
            }
            
            if (_msaaSamples > 1)
            {
                with (attachRef[2])
                {
                    attachment = 2;
                    layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                }
            }
              
            //Describe the subpass to be used in the render pass object
            VkSubpassDescription subpassInfo;
            with (subpassInfo)
            {
                pipelineBindPoint       = VK_PIPELINE_BIND_POINT_GRAPHICS;
                colorAttachmentCount    = 1;
                pColorAttachments       = &attachRef[0];
                pDepthStencilAttachment = &attachRef[1];
                pResolveAttachments     = (_msaaSamples > 1) ? &attachRef[2] : null;
            }
        
            //Create a render pass object containing this information
            VkRenderPassCreateInfo passInfo;
            with (passInfo)
            {
                attachmentCount = 2 + (_msaaSamples > 1);
                pAttachments    = attachInfo.ptr;
                subpassCount    = 1;
                pSubpasses      = &subpassInfo;
            }
            vkCreateRenderPass(_device, &passInfo, null, &_pass);
        }
        
        void createPipeLayout()
        {
            //Create a descriptor set layout for combined image sampler descriptors
            VkDescriptorSetLayoutBinding bindingInfo;
            with (bindingInfo)
            {
                binding         = 0;
                descriptorType  = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                descriptorCount = 1;
                stageFlags      = VK_SHADER_STAGE_FRAGMENT_BIT;
            }
            
            VkDescriptorSetLayoutCreateInfo setInfo;
            with (setInfo)
            {
                sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
                bindingCount = 1;
                pBindings    = &bindingInfo;
            }
            vkCreateDescriptorSetLayout(_device, &setInfo, null, &_descriptorLayouts[0]);
            
            //Create a descriptor set layout for the combined image sampler descriptors used in the post-processing pass
            bindingInfo.descriptorCount = 2;
            vkCreateDescriptorSetLayout(_device, &setInfo, null, &_descriptorLayouts[1]);
        
            //Create a pipeline layout describing the push constant range for transformation matrices and descriptor set layout to use for textures
            VkPushConstantRange[2] constantInfo;
            with (constantInfo[0])
            {
                //The first portion of the push constant data should be used to provide transformation matrices
                stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
                offset     = 0;
                size       = mat4.sizeof + 32;
            }
            with (constantInfo[1])
            {
                //The second portion of the push constant data should be used to provide arbitrary data to fragment shaders
                stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
                offset     = constantInfo[0].size;
                size       = 128 - offset;
            }
            
            VkPipelineLayoutCreateInfo layoutInfo;
            with (layoutInfo)
            {
                sType                  = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
                pushConstantRangeCount = constantInfo.length;
                pPushConstantRanges    = constantInfo.ptr;
                setLayoutCount         = 1;
                pSetLayouts            = &_descriptorLayouts[0];
            }
            vkCreatePipelineLayout(_device, &layoutInfo, null, &_pipeLayout);
        }
        
        void createDescriptorPool()
        {
            VkDescriptorPoolSize sizeInfo;
            with (sizeInfo)
            {
                type            = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                descriptorCount = maxTextures + 2;
            }
        
            //Create a descriptor pool
            VkDescriptorPoolCreateInfo poolInfo;
            with (poolInfo)
            {
                sType         = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
                flags         = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
                poolSizeCount = 1;
                pPoolSizes    = &sizeInfo;
                maxSets       = maxTextures + 2;
            }
            vkCreateDescriptorPool(_device, &poolInfo, null, &_descriptors);
        }
        
        void createRenderSync()
        {
            VkSemaphoreCreateInfo semaphoreInfo;
            with (semaphoreInfo)
            {
                sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
            }
            
            VkFenceCreateInfo fenceInfo;
            with (fenceInfo)
            {
                sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
                flags = VK_FENCE_CREATE_SIGNALED_BIT;
            }
            
            //Create semaphores and fences used to synchronise operations in rendering
            foreach (ref i; _swapImageObtained)
            {
                vkCreateSemaphore(_device, &semaphoreInfo, null, &i);
            }
            foreach (ref i; _swapImageRendered)
            {
                vkCreateSemaphore(_device, &semaphoreInfo, null, &i);
            }
            foreach (ref i; _frameDone)
            {
                vkCreateFence(_device, &fenceInfo, null, &i);
            }
        }
        
        void getSwapImage()
        {
            //After the last frame in flight has finished being rendered and presented, obtain an image for the new one
            vkWaitForFences(_device, 1, &_frameDone[_curFrame], VK_TRUE, ulong.max);
            vkResetFences(_device, 1, &_frameDone[_curFrame]);
            
            auto oldResolution = _resolution;
            updateResolution;
            if (_resolution != oldResolution || vkAcquireNextImageKHR(_device, _swap, ulong.max, _swapImageObtained[_curFrame], null, &_nextSwapImage) == VK_ERROR_OUT_OF_DATE_KHR)
            {
                //If the swapchain is now invalid then wait for the physical device to become idle, recreate the swapchain and try again
                _resChanged = true;
                _device.waitIdle;
                destroySwapchain;
                createSwapchain;
                vkAcquireNextImageKHR(_device, _swap, ulong.max, _swapImageObtained[_curFrame], null, &_nextSwapImage);
            }
            
            //Reset the command buffer for this frame in flight, begin the recording of a render pass instance
            _device.resetCmd(_renderCmd[_curFrame]);
            
            VkClearValue[2] clear;
            clear[0].color.float32[] = 0.0;
            clear[1].depthStencil.depth = 1.0;

            VkRenderPassBeginInfo passInfo;
            with (passInfo)
            {
                sType           = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
                renderPass      = _pass;
                framebuffer     = _framebuffers[_nextSwapImage];
                renderArea      = VkRect2D(VkOffset2D(0,0), VkExtent2D(cast(uint)_resolution.width, cast(uint)_resolution.height));
                clearValueCount = clear.length;
                pClearValues    = clear.ptr;
            }
            vkCmdBeginRenderPass(_renderCmd[_curFrame], &passInfo, VK_SUBPASS_CONTENTS_INLINE);

            //Set the dynamic state configuration for the new command buffer
            VkViewport viewport;
            with (viewport)
            {
                x        = 0.0;
                y        = 0.0;
                width    = passInfo.renderArea.extent.width;
                height   = passInfo.renderArea.extent.height;
                minDepth = 0.0;
                maxDepth = 1.0;
            }
            vkCmdSetViewport(_renderCmd[_curFrame], 0, 1, &viewport);
            
            VkRect2D scissor;
            with (scissor)
            {
                offset = VkOffset2D(0,0);
                extent = passInfo.renderArea.extent;
            }
            vkCmdSetScissor(_renderCmd[_curFrame], 0, 1, &scissor);
        }

    package:
        auto pass() { return _pass; }
        auto pipeLayout() { return _pipeLayout; }
        auto descriptorPool() { return _descriptors; }
        auto imageLayout() { return _descriptorLayouts[0]; }
        auto framebuffer() { return _framebuffers[_nextSwapImage]; }
        auto frameCmd() { return _renderCmd[_curFrame]; }
        auto swapImages() { return _framebuffers.length; }
        auto swapImage() { return _nextSwapImage; }
        auto sampler() { return _sampler; }
        auto msaaSamples() { return _msaaSamples; }
        auto mipSupport() { return _mipSupport; }
    
    public:
        ref auto device() { return _device; }
        alias device this;
        
        enum maxTextures = 16;

        auto w() { return _resolution.width; }
        auto h() { return _resolution.height; }
        auto resChanged()
        {
            auto toReturn = _resChanged;
            _resChanged = false;
            return toReturn;
        }
        
        void setVertexData(T)(T[] data ...)
            in (T.sizeof * data.length <= 32)
        {
            vkCmdPushConstants(_renderCmd[_curFrame], _pipeLayout, VK_SHADER_STAGE_VERTEX_BIT, mat4.sizeof, cast(uint)(T.sizeof * data.length), data.ptr);
        }
        void setFragmentData(T)(T[] data ...)
            in (T.sizeof * data.length <= 32)
        {
            vkCmdPushConstants(_renderCmd[_curFrame], _pipeLayout, VK_SHADER_STAGE_FRAGMENT_BIT, mat4.sizeof + 32, cast(uint)(T.sizeof * data.length), data.ptr);
        }
        
        void present()
        {
            //Submit the rendering to be performed for this frame in flight
            vkCmdEndRenderPass(_renderCmd[_curFrame]);
            _device.endCmd(_renderCmd[_curFrame]);
            
            VkSubmitInfo submitInfo;
            with (submitInfo)
            {
                sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
                waitSemaphoreCount   = 1;
                pWaitSemaphores      = &_swapImageObtained[_curFrame];
                signalSemaphoreCount = 1;
                pSignalSemaphores    = &_swapImageRendered[_curFrame];
                pWaitDstStageMask    = cast(const(uint)*)[VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT].ptr;
                commandBufferCount   = 1;
                pCommandBuffers      = &_renderCmd[_curFrame];
            }
            vkQueueSubmit(_device.renderQueue, 1, &submitInfo, _frameDone[_curFrame]);

            //Queue a presentation of the rendered backbuffer to the window surface
            VkPresentInfoKHR presentInfo;
            with (presentInfo)
            {
                sType              = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
                waitSemaphoreCount = 1;
                pWaitSemaphores    = &_swapImageRendered[_curFrame];
                swapchainCount     = 1;
                pSwapchains        = &_swap;
                pImageIndices      = &_nextSwapImage;
            }
            vkQueuePresentKHR(_device.presentQueue, &presentInfo); 
        
            //Get a new swapchain image and move onto the next frame in flight
            _curFrame = (_curFrame + 1) % numFramesInFlight;
            getSwapImage;
        }

        this(ref Window window, bool vsync, uint msaa, uint anisotropy)
            in (msaa > 0 && !((msaa >>> 1) & msaa))
        {
            _device  = Device(window);
            _window  = &window;
            _format  = chooseSwapFormat;
            _depthFormat = chooseFormat(VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_FORMAT_D32_SFLOAT);
            _present = choosePresent(vsync);
            _device.allocCmds(_renderCmd[]);
            
            _msaaSamples = cast(VkSampleCountFlagBits)_device.msaaSupport(msaa);
            logf(LogLevel.warning, _msaaSamples != msaa, "MSAA sample count not supported by logical device, using %s instead", cast(uint)_msaaSamples);
            _mipSupport = cast(bool)chooseFormat(VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT, VK_FORMAT_R8G8B8A8_SRGB);
            log(LogLevel.warning, !_mipSupport, "Linear filtering for mipmap generation not supported by logical device, mipmapping is disabled");

            createRenderPass;
            createSampler(anisotropy);
            createPipeLayout;
            createDescriptorPool;
            createSwapchain;
            createRenderSync;
            getSwapImage;   
        }
        
        @disable this();
        
        ~this() 
        {
            if (_window)
            {
                _device.waitIdle;
                foreach (i; _frameDone)
                {
                    vkDestroyFence(_device, i, null);
                }
                foreach (i; _swapImageRendered)
                {
                    vkDestroySemaphore(_device, i, null);
                }
                foreach (i; _swapImageObtained)
                {
                    vkDestroySemaphore(_device, i, null);
                }
                destroySwapchain;
                vkDestroyDescriptorPool(_device, _descriptors, null);
                vkDestroySampler(_device, _sampler, null);
                vkDestroyRenderPass(_device, _pass, null);
                vkDestroyPipelineLayout(_device, _pipeLayout, null);
                foreach (i; _descriptorLayouts)
                {
                    vkDestroyDescriptorSetLayout(_device, i, null);
                }
            }
        }
}
