module client.gfx.system;

import bindbc.sdl;
import erupted,
       erupted.vulkan_lib_loader;
import std.conv,
       std.exception,
       std.format;
        
package 
{
    private __gshared VkInstance _instance;
    pragma(inline, true) auto instance() { return _instance; }
    
    auto getVkSlice(alias fun, ret, T...)(T args) 
    {
        uint numElements;
        fun(args, &numElements, null);
        ret[] slice;
        slice.length = numElements;
        fun(args, &numElements, slice.ptr);
        return slice;
    }
    
    size_t formatSize(VkFormat format)
    {
        //Return the size of a Vulkan data format passed
        switch (format)
        {
            case VK_FORMAT_R16G16B16A16_SFLOAT: return 8;
            case VK_FORMAT_D32_SFLOAT,
                 VK_FORMAT_R8G8B8A8_UNORM,
                 VK_FORMAT_R8G8B8A8_UINT,
                 VK_FORMAT_B8G8R8A8_SRGB,
                 VK_FORMAT_R8G8B8A8_SRGB: return 4;
            case VK_FORMAT_R8G8B8_UINT: return 3;
            case VK_FORMAT_D16_UNORM: return 2;
            default: throw new Exception(std.format.format!"Unrecognised Vulkan format %s"(format));
        }
    }
    
    auto aspect(VkFormat format)
    {
        //Return an image aspect flag corresponding to the Vulkan data format passed
        switch (format)
        {
            case VK_FORMAT_R16G16B16A16_SFLOAT,
                 VK_FORMAT_R8G8B8A8_UNORM,
                 VK_FORMAT_R8G8B8A8_UINT,
                 VK_FORMAT_B8G8R8A8_SRGB,
                 VK_FORMAT_R8G8B8A8_SRGB: return VK_IMAGE_ASPECT_COLOR_BIT;
            case VK_FORMAT_D32_SFLOAT,
                 VK_FORMAT_D16_UNORM: return VK_IMAGE_ASPECT_DEPTH_BIT;
            default: throw new Exception(std.format.format!"Unrecognised Vulkan format %s"(format));
        }
    }
}

shared static this()
{
    //SDL needs a window to find the instance extensions it needs, even though this is just platform specific. So create a temporary window just for this purpose
    enforce(SDL_Init(SDL_INIT_VIDEO) >= 0, format!"Failed to initialise rendering subsystem: %s"(SDL_GetError.to!(const(char)[])));
    auto window = SDL_CreateWindow("", 1, 1, 1, 1, SDL_WINDOW_HIDDEN | SDL_WINDOW_VULKAN);
    enforce(window, format!"Failed to find extensions needed to initialise Vulkan: %s"(SDL_GetError.to!(const(char)[])));
    auto extensions = getVkSlice!(SDL_Vulkan_GetInstanceExtensions, const(char)*)(window);
    SDL_DestroyWindow(window);

    //Create a Vulkan instance using the extensions found through this window
    enforce(loadGlobalLevelFunctions, "Failed to load Vulkan, your graphics driver may be too old");
    VkInstanceCreateInfo instanceInfo;
    with (instanceInfo) 
    {
        sType                   = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        enabledExtensionCount   = cast(uint)extensions.length;
        ppEnabledExtensionNames = extensions.ptr;
    }
    vkCreateInstance(&instanceInfo, null, &_instance);
    
    loadInstanceLevelFunctions(instance);
    loadDeviceLevelFunctions(instance);
}

shared static ~this()
{
    if (instance)
    {
        vkDestroyInstance(instance, null);
    }
    SDL_QuitSubSystem(SDL_INIT_VIDEO);
}
