module client.gfx.text;

import client.gfx.model,
       client.gfx.pipeline,
       client.gfx.renderer,
       client.gfx.texture;
import dvec.matrix,
       dvec.vector;

struct TextRenderer
{
    private:
        GfxPipeline _pipe;
        Texture* _texture;
        Model!() _model;
        
    public:
        void bind()
        {
            _pipe.bind;
            _texture.bind;
        }
    
        void render(float x, float y, float size, in vec4 color, string text)
        {
            _pipe.renderer.setFragmentData(color);
            auto xScale = ((cast(float)size)/_pipe.renderer.w);
            auto yScale = ((cast(float)size)/_pipe.renderer.h);
            
            size_t lastOffset;
            int cursorX;
            int cursorY = 1;
            foreach (i,e; text)
            {
                //Search the string until a newline character is found or the maximum length that can be output is reached
                auto stretch = i-lastOffset;
                if (e == '\n' || stretch == 31 || i == text.length-1)
                {
                    //Find the normalised device space coordinates to use and set the vertex data
                    auto normPos = normalisePos(_pipe.renderer, ivec2(cast(int)(x + cursorX*size), cast(int)(y - cursorY*(size/2 + 1))));
                    char[32] strBuf;
                    strBuf[0..stretch+1] = text[lastOffset..i+1];
                    _pipe.renderer.setVertexData(strBuf[0 .. (text.length+3) & ~0x3]);
                    
                    //Render as much text as possible and update the cursor
                    _model.renderRange(translate!mat4(vec3(normPos.x, normPos.y, 0)) * scale!mat4(xScale, yScale, 1.0f), cast(uint)(stretch+1)*6);
                    if (e == '\n')
                    {
                        cursorX = 0;
                        cursorY += 1;
                    }
                    else
                    {
                        cursorX += stretch+1;
                    }
                    lastOffset = i+1;
                }
            }
        }
        
        void renderShadowed(float x, float y, float size, float shadowGap, in vec4 color, string text)
        {
            render(x+shadowGap, y-shadowGap, size, vec4(color.r*0.2, color.g*0.2, color.b*0.2, color.a), text);
            render(x, y, size, color, text);
        }
    
        this(ref Renderer renderer, Texture* ascii, ref Shader vert, ref Shader frag)
        {
            renderer.createGfxPipeline(_pipe, GfxPipelineConfig(GfxPipelineFlags.NoDepthTest | GfxPipelineFlags.NoDepthWrite, vert, frag));
            _texture = ascii;
            
            auto staging = StagingModel(renderer, 6*32, 4*32, false);
            auto index  = staging.getIndex(6*32);
            auto vertex = staging.getVertex(4*32);
            foreach (i; 0..32)
            {
                index[6*i .. 6*(i+1)] = cast(ushort[6])[0,1,2,3,2,1];
                index[6*i .. 6*(i+1)] += cast(ushort)(4*i);
                
                vertex[4*i + 0] = Vertex(vec3(i+0,+0,0), vec3(0,0,i));
                vertex[4*i + 1] = Vertex(vec3(i+1,+0,0), vec3(1,0,i));
                vertex[4*i + 2] = Vertex(vec3(i+0,-1,0), vec3(0,1,i));
                vertex[4*i + 3] = Vertex(vec3(i+1,-1,0), vec3(1,1,i));
            }
            _model = Model!()(renderer, staging);
            renderer.waitIdle;
        }
        
        ~this() { _pipe.renderer.waitIdle; }
}
