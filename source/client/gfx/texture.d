module client.gfx.texture;

import client.gfx.buffer,
       client.gfx.renderer;
import client.resource;
import erupted;
import std.exception,
       std.format,
       std.math;
  
/* Types used to manipulate textures consisting of an image object and descriptor used to access them in shaders */     
enum TextureConfig
{
    None            = 0x0,
    MipMapped       = 0x1,
    Cubemap         = 0x2,
    PreTextureArray = 0x4
}

private auto loadBMP(ref Renderer renderer, ref StagingBuffer staging, void[] data)
{
    //Validate the header
    enforce(cast(char[])data[0..2] == "BM", format!"BMP header has magic number %s instead of valid magic number BM"(cast(char[])data[0..2]));
    enforce(*cast(uint*)&data[2] == data.length, "BMP header contains file size which does not match texture resource data size");
    enforce(*cast(uint*)&data[14] >= 40, "BMP header contains invalid size");
    enforce(*cast(ushort*)&data[26] == 1, "BMP header contains invalid number of planes");
    
    auto depth = *cast(ushort*)&data[28];
    if (depth == 24)
    {
        enforce(*cast(uint*)&data[30] == 0, "Unsupported compression scheme used in BMP texture resource data");
    }
    else if (depth == 32)
    {
        enforce(*cast(uint*)&data[30] == 3, "Unsupported compression scheme used in BMP texture resource data");
    }
    else
    {
        throw new Exception("Unsupported bit depth used in BMP texture resource data");
    }
    
    //Read the required values
    auto offset     = *cast(uint*)&data[10];
    auto resolution = *cast(int[2]*)&data[18];
    
    //Load the image data into device local memory
    staging = StagingBuffer(renderer, resolution[0]*resolution[1]*uint.sizeof, false);
    if (depth == 24)
    {
        foreach (i, ref e; cast(uint[])staging.map)
        {
            e = uint.max;
            (cast(ubyte*)&e)[0] = (cast(ubyte[])data)[offset + i*3 + 2];
            (cast(ubyte*)&e)[1] = (cast(ubyte[])data)[offset + i*3 + 1];
            (cast(ubyte*)&e)[2] = (cast(ubyte[])data)[offset + i*3 + 0];
        }
    }
    else
    {
        foreach (i, ref e; cast(uint[])staging.map)
        {
            (cast(ubyte*)&e)[0] = (cast(ubyte[])data)[offset + i*4 + 2];
            (cast(ubyte*)&e)[1] = (cast(ubyte[])data)[offset + i*4 + 1];
            (cast(ubyte*)&e)[2] = (cast(ubyte[])data)[offset + i*4 + 0];
            (cast(ubyte*)&e)[3] = (cast(ubyte[])data)[offset + i*4 + 3];
        }
    }
    return resolution;
}

struct Texture
{
    private:
        Image _img;
        VkDescriptorSet _descriptor;
        Renderer* _renderer;
        
        void allocSet()
        {
            //Allocate a descriptor set for the texture
            VkDescriptorSetAllocateInfo allocInfo;
            auto layout = _renderer.imageLayout;
            with (allocInfo)
            {
                sType              = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
                descriptorPool     = _renderer.descriptorPool;
                descriptorSetCount = 1;
                pSetLayouts        = &layout;
            }
            vkAllocateDescriptorSets(*_renderer, &allocInfo, &_descriptor);
            
            VkDescriptorImageInfo descriptorConfig;
            with (descriptorConfig)
            {
                imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                imageView   = _img.view;
                sampler     = _renderer.sampler;
            }
            
            VkWriteDescriptorSet writeInfo;
            with (writeInfo)
            {
                sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                dstSet          = _descriptor;
                dstBinding      = 0;
                descriptorType  = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                dstArrayElement = 0;
                descriptorCount = 1;
                pImageInfo      = &descriptorConfig;
            }
            vkUpdateDescriptorSets(*_renderer, 1, &writeInfo, 0, null);
        }
        
    public:
        void bind() { vkCmdBindDescriptorSets(_renderer.frameCmd, VK_PIPELINE_BIND_POINT_GRAPHICS, _renderer.pipeLayout, 0, 1, &_descriptor, 0, null); }
        
        this(ref Renderer renderer, TextureConfig config, uint width, uint height, StagingBuffer*[] toPush ...)
            in ((toPush.length > 0) &&
                (!(config & (TextureConfig.Cubemap | TextureConfig.PreTextureArray)) || toPush.length == 1) &&
                (!(config & TextureConfig.MipMapped) || width == height) &&
                (toPush.length <= uint.max))
        {
            _renderer = &renderer;
            auto mipLevels = (cast(bool)(config & TextureConfig.MipMapped) && renderer.mipSupport) ? (cast(uint)log2(width)+1) : 1;
        
            //Create an image object and move the passed staging textures to the allocation array layers of the image object
            assert(toPush[0].map.length == width*height*uint.sizeof, "Staging buffer for texture layer does not match data width for resolution");
			if (config & TextureConfig.Cubemap)
			{
			    enforce(height == width*6, "Invalid resolution for cubemap texture");
				_img = Image(renderer, VK_IMAGE_VIEW_TYPE_CUBE, VK_IMAGE_USAGE_SAMPLED_BIT, VK_FORMAT_R8G8B8A8_SRGB, width, height, 6);
				toPush[0].asyncPush(_img, 0, 6, true);
			}
			else if (config & TextureConfig.PreTextureArray)
			{
			    enforce(height % width == 0, "Invalid resolution for prebuilt texture array");
			    auto layers = height/width;
			    _img = Image(renderer, VK_IMAGE_VIEW_TYPE_2D_ARRAY, VK_IMAGE_USAGE_SAMPLED_BIT, VK_FORMAT_R8G8B8A8_SRGB, width, width, layers, mipLevels);
			    toPush[0].asyncPush(_img, 0, layers, true);
			}
            else
            {
            	_img = Image(renderer, toPush.length > 1 ? VK_IMAGE_VIEW_TYPE_2D_ARRAY : VK_IMAGE_VIEW_TYPE_2D, VK_IMAGE_USAGE_SAMPLED_BIT, VK_FORMAT_R8G8B8A8_SRGB, width, height, cast(uint)toPush.length, mipLevels);

			    //Move the staging buffer data into the layers of the image object
			    foreach (i,e; toPush)
			    {
			        assert(e.map.length == width*height*uint.sizeof, "Staging buffer for texture layer does not match data width for resolution");
			        e.asyncPush(_img, cast(uint)i, 1, i+1 == toPush.length);
                }
			}
            allocSet;
        }
    
        this(ref Renderer renderer, ref StagingBuffer toPush, void[] data, TextureConfig config = TextureConfig.None)
        {
            auto resolution = loadBMP(renderer, toPush, data);
            this(renderer, config, resolution[0], resolution[1], &toPush);
        }
        
        ~this()
        {
            if (_renderer)
            {
                _renderer.waitIdle;
                vkFreeDescriptorSets(*_renderer, _renderer.descriptorPool, 1, &_descriptor);
            }
        }
}

class StagingTextureResource : Resource
{
    private:
        int[2] _resolution;
        
    public:
        StagingBuffer buffer;
        
        alias buffer this;
        
        auto w() { return _resolution[0]; }
        auto h() { return _resolution[1]; }
        
        this(ref Renderer renderer, void[] data)
        {
            _resolution = loadBMP(renderer, buffer, data);
        }
}

class TextureResource : Resource
{
    Texture texture;
    
    alias texture this;
    
    this(ref Renderer renderer, ref StagingBuffer toPush, void[] data, TextureConfig config = TextureConfig.None)
    {
        texture = Texture(renderer, toPush, data, config);
    }
}
