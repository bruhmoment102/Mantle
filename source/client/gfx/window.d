module client.gfx.window;

import bindbc.sdl;
import client.gfx.system;
import erupted,
       erupted.vulkan_lib_loader;
import std.conv,
       std.exception,
       std.format;

struct Window 
{
    private:
        SDL_Window* _window;
        VkSurfaceKHR _surface;
        bool _fullscreen;
        
    public:    
        package auto surface() { return _surface; }
        
        alias surface this;
        
        auto w() 
        {
            int toReturn;
            SDL_GetWindowSize(_window, &toReturn, null);
            return toReturn;
        }
        
        auto h() 
        {
            int toReturn;
            SDL_GetWindowSize(_window, null, &toReturn);
            return toReturn;
        }
        
        static auto trap() { return SDL_GetRelativeMouseMode() == SDL_TRUE; }
        static void trap(bool toSet) { SDL_SetRelativeMouseMode(toSet ? SDL_TRUE : SDL_FALSE); }
        
        void toggleFullscreen()
        { 
            //Set the window resolution to the display resolution when entering fullscreen
            if (!_fullscreen)
            {
                SDL_DisplayMode mode;
                SDL_GetDesktopDisplayMode(0, &mode);
                enforce(mode.w > 0 && mode.h > 0, "Invalid display resolution");
                SDL_SetWindowSize(_window, mode.w, mode.h);
            }
        
            enforce(SDL_SetWindowFullscreen(_window, _fullscreen ? 0 : SDL_WINDOW_FULLSCREEN) >= 0, format!"Failed to toggle fullscreen: %s"(SDL_GetError.to!string));
            _fullscreen = !_fullscreen;
        }
    
        this(string title, uint w, uint h, bool fullscreen)
            in (w > 0 && h > 0, "Invalid window resolution") 
        {
            //Create a window and Vulkan window surface
            _window = SDL_CreateWindow((title ~ '\0').ptr, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, w, h,
                                       SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_VULKAN | (SDL_WINDOW_FULLSCREEN * fullscreen));
            enforce(_window, format!"Failed to create window: %s"(SDL_GetError.to!string));
            
            SDL_Vulkan_CreateSurface(_window, instance, &_surface);
            enforce(_surface, format!"Failed to obtain window surface for rendering: %s"(SDL_GetError.to!string));
            
            _fullscreen = fullscreen;
        }
        
        this(string title, bool fullscreen)
        {
            SDL_DisplayMode mode;
            SDL_GetDesktopDisplayMode(0, &mode);
            enforce(mode.w > 0 && mode.h > 0, "Invalid display resolution");
            this(title, cast(uint)mode.w, cast(uint)mode.h, fullscreen);
        }
        
        @disable this();
        
        ~this() 
        {
            vkDestroySurfaceKHR(instance, _surface, null);
            SDL_DestroyWindow(_window);
        }
}
