module client.gui;

import dvec.vector;
import client.gfx.model,
       client.gfx.pipeline,
       client.gfx.renderer,
       client.gfx.text,
       client.gfx.texture;
import client.resource;
import client.world.renderer;
import common.world.living;

struct GUI
{
    private:
        GfxPipeline _pipe;
        Texture* _texture;
        Model!() _static;
        PerFrameModel _dynamic;
        TextRenderer _text;
        float _scale;
        
        ubyte _lastSelect;
        double _time = 0;
        double _lastSelectTime = 0;
        
        WorldRenderer _renderer;
        LocalPlayer _player;
        
        void build()
        {
            auto staging = StagingModel(_pipe.renderer, 6*10, 4*10, false);            
            auto center = ivec2(_pipe.renderer.w, _pipe.renderer.h) / 2;
            
            //Crosshair
            auto crossSize = cast(int)(10 * _scale) / 2;
            insertBox(_pipe.renderer, staging, ivec2(center.x-crossSize, center.y-crossSize), ivec2(center.x+crossSize, center.y+crossSize),
                                               vec2(16.0/136.0, 6.0/32.0), vec2(26.0/136.0, 0.5));
            
            //On-hand inventory
            auto onHandSize = cast(int)(136 * _scale) / 2;
            insertBox(_pipe.renderer, staging, ivec2(center.x-onHandSize, 0), ivec2(center.x+onHandSize, cast(int)(16 * _scale)),
                                               vec2(0.0, 0.5), vec2(1.0, 1.0));
            
            _static = Model!()(_pipe.renderer, staging);
            _dynamic.build;
            _pipe.renderer.waitIdle;
        }
        
    public:
        void render(double dt)
        {
            if (_pipe.renderer.resChanged)
            {
                build;
            }
            _time += dt;
        
            //Render static UI elements
            _pipe.bind;
            _texture.bind;
            _static.render;
            
            //Update and render dynamic UI elements
            _dynamic.index[] = cast(uint[6])[0,1,2,3,2,1];
            
            auto center = _pipe.renderer.w/2;
            auto inHandPos = cast(int)(center - ((136/2)*_scale));
            auto selectOffset = cast(int)( inHandPos + (_player.onHandSelect*16 - _player.onHandSelect)*_scale );
            auto normPos = normalisePos(_pipe.renderer, ivec2(selectOffset, 0));
            auto normEnd = normalisePos(_pipe.renderer, ivec2(selectOffset + cast(int)(16*_scale), cast(int)(16*_scale)));
            
            auto vertex = _dynamic.vertex;
            vertex[0] = Vertex(vec3(normPos.x, normPos.y, 0), vec3(0, 0, 0), 0, uint.max);
            vertex[1] = Vertex(vec3(normEnd.x, normPos.y, 0), vec3(16.0/136.0, 0, 0), 0, uint.max);
            vertex[2] = Vertex(vec3(normPos.x, normEnd.y, 0), vec3(0, 0.5, 0), 0, uint.max);
            vertex[3] = Vertex(vec3(normEnd.x, normEnd.y, 0), vec3(16.0/136.0, 0.5, 0), 0, uint.max);
            
            _dynamic.render;
            
            //Render items in player inventory
            foreach (i, ref e; _player.onHand)
            {
                _renderer.renderItem(e.item, 
                                     normalisePos(_pipe.renderer, ivec2(center, 0) + ivec2(vec2(16*cast(int)(i-4) + 4 - cast(int)i, 8)*_scale)),
                                     normaliseExtent(_pipe.renderer, ivec2(vec2(8,8)*_scale)));
            }
            
            //Render text
            _text.bind;
            if (_player.onHandSelect != _lastSelect)
            {
                _lastSelect = _player.onHandSelect;
                _lastSelectTime = _time;
            }
            
            auto selectName = _player.onHand[_player.onHandSelect].item.name;
            _text.renderShadowed(center - selectName.length*4*_scale, 32*_scale, 16*_scale, _scale, vec4(1, 1, 1, 1 - (_time-_lastSelectTime)^^2), selectName);
        }
    
        this(WorldRenderer renderer, ref ResourceStore resources, LocalPlayer player, float scale)
        {
            renderer.renderer.createGfxPipeline(_pipe, GfxPipelineConfig(GfxPipelineFlags.NoDepthTest, resources.get!(ShaderResource, "hud.vspv").shader, resources.get!(ShaderResource, "hud.fspv").shader));
            _texture = &resources.get!(TextureResource, "gui.bmp").texture;
            _text = TextRenderer(renderer.renderer, &resources.get!(TextureResource, "ascii.bmp").texture, resources.get!(ShaderResource, "text.vspv").shader, resources.get!(ShaderResource, "text.fspv").shader);
            _scale = scale;
            
            _renderer = renderer;
            _dynamic = PerFrameModel(renderer.renderer, 6, 4);
            _player = player;
            build;
        }
}
