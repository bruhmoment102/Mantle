module client.input;

import bindbc.sdl;
import dvec.vector;
import std.conv,
       std.math;
import std.experimental.logger;

enum Key
{
    Null, Shift, Space,
    Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine,
    F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
    A, B, C, D, E, F, G, H, I, J, K, M, N, L, O, P, Q, R, S, T, U, V, W, X, Y, Z
}

enum ControllerButton
{
    Null, A, B, X, Y, Select, Start, LT, RT
}

enum ControllerAnalog
{
    Null,
    LeftCircleX, LeftCircleY, RightCircleX, RightCircleY,
    LeftTrigger, RightTrigger
}

struct InputHandler
{
    private:
        ubyte _misc;
        ubyte[(Key.max+7)/8] _keys;
        ubyte[(ControllerButton.max+7)/8] _buttons;
        
        int[2] _mouse;
        short[ControllerAnalog.max] _analog;
        
        SDL_GameController* _controller;
        uint _controllerID;
    
        Key toKey(SDL_Keycode keycode)
        {
        	//Convert an SDL keycode to a RealEngine one
            switch (keycode) with (Key)
            {
                case SDLK_LSHIFT,
                     SDLK_RSHIFT              : return Shift;
                case SDLK_SPACE               : return Space;
                case SDLK_0: .. case SDLK_9   : return cast(Key)(Zero + (keycode - SDLK_0));
                case SDLK_F1: .. case SDLK_F12: return cast(Key)(F1 + (keycode - SDLK_F1));
                case SDLK_a: .. case SDLK_z   : return cast(Key)(A + (keycode - SDLK_a));
                default                       : return Null;
            }
        }
        
        auto toButton(SDL_GameControllerButton button)
        {
            switch (button) with (ControllerButton)
            {
                case SDL_CONTROLLER_BUTTON_A    : return A;
                case SDL_CONTROLLER_BUTTON_B    : return B;
                case SDL_CONTROLLER_BUTTON_X    : return X;
                case SDL_CONTROLLER_BUTTON_Y    : return Y;
                case SDL_CONTROLLER_BUTTON_BACK : return Select;
                case SDL_CONTROLLER_BUTTON_START: return Start;
                default                         : return Null;
            }
        }
        
        void setKey(Key key, bool pressed)
        {
            auto toSet = cast(ubyte)( (cast(ubyte)pressed) << (7 - (key%8)) );
            if (pressed)
            {
                _keys[key/8] |= toSet;
            }
            else
            {
                _keys[key/8] &= toSet;
            }
        }
        
        void setButton(ControllerButton button, bool pressed)
        {
            auto toSet = cast(ubyte)( (cast(ubyte)pressed) << (7 - (button%8)) );
            if (pressed)
            {
                _buttons[button/8] |= toSet;
            }
            else
            {
                _buttons[button/8] &= toSet;
            }
        }
        
    public:
        auto toQuit() { return cast(bool)(_misc & 0x80); }
        auto key(Key toCheck) { return cast(bool)(( _keys[toCheck/8] >> (7-(toCheck%8)) ) & 0x1); }
        auto mouseLeft() { return cast(bool)(_misc & 0x1); }
        auto mouseRight() { return cast(bool)(_misc & 0x2); }
        auto mouse()
        {
            auto toReturn = _mouse;
            _mouse = 0;
            return toReturn;
        }
        
        auto button(ControllerButton toCheck) { return cast(bool)(( _buttons[toCheck/8] >> (7-(toCheck%8)) ) & 0x1); }
        auto leftStick() { return vec2(_analog[ControllerAnalog.LeftCircleX-1], _analog[ControllerAnalog.LeftCircleY-1]) / short.max; }
        auto rightStick() { return vec2(_analog[ControllerAnalog.RightCircleX-1], _analog[ControllerAnalog.RightCircleY-1])  / short.max; }
        auto leftTrigger() { return _analog[ControllerAnalog.LeftTrigger-1] / cast(float)short.max; }
        auto rightTrigger() { return _analog[ControllerAnalog.RightTrigger-1] / cast(float)short.max; }
        
        void delegate()[Key] keyDown;
        void delegate()[Key] keyUp;
        
        void rehash()
        {
            keyDown.rehash;
            keyUp.rehash;
        }

        void update()
        {
        	//Process events provided by SDL
            SDL_Event event = void;
            while (SDL_PollEvent(&event)) switch (event.type)
            {
                case SDL_QUIT:
                    _misc |= 0x80;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    switch (event.button.button)
                    {
                        case SDL_BUTTON_LEFT : _misc |= 0x1; break;
                        case SDL_BUTTON_RIGHT: _misc |= 0x2; break;
                        default: break;
                    }
                    break;
                case SDL_MOUSEBUTTONUP:
                    switch (event.button.button)
                    {
                        case SDL_BUTTON_LEFT : _misc &= ~0x1; break;
                        case SDL_BUTTON_RIGHT: _misc &= ~0x2; break;
                        default: break;
                    }
                    break;
                case SDL_MOUSEMOTION:
                    with (event.motion)
                    {
                        _mouse[0] += xrel;
                        _mouse[1] += yrel;
                    }
                    break;
                case SDL_KEYDOWN:
                    auto key = toKey(event.key.keysym.sym);
                    if (key != Key.Null)
                    {
                        setKey(key, true);
                        auto handler = keyDown.get(key, null);
                        if (handler)
                        {
                            handler();
                        }
                    }
                    break;
                case SDL_KEYUP:
                    auto key = toKey(event.key.keysym.sym);
                    if (key != Key.Null)
                    {
                        setKey(key, false);
                        auto handler = keyUp.get(key, null);
                        if (handler)
                        {
                            handler();
                        }
                    }
                    break;
                case SDL_CONTROLLERAXISMOTION:
                    if (event.caxis.which == _controllerID && event.caxis.axis <= SDL_CONTROLLER_AXIS_TRIGGERRIGHT)
                    {
                        _analog[event.caxis.axis] = event.caxis.value;
                        if (abs(_analog[event.caxis.axis]) < 0.2)
                        {
                            _analog[event.caxis.axis] = 0;
                        }
                    }
                    break;
                case SDL_CONTROLLERBUTTONUP, SDL_CONTROLLERBUTTONDOWN:
                    if (event.cbutton.which == _controllerID)
                    {
                        setButton(toButton(cast(SDL_GameControllerButton)event.cbutton.button), event.cbutton.state == SDL_PRESSED);
                    }
                    break;
                case SDL_CONTROLLERDEVICEADDED:
                    if (!_controller)
                    {
                        _controller = SDL_GameControllerOpen(_controllerID);
                        if (_controller)
                        {
                            _controllerID = SDL_JoystickInstanceID(SDL_GameControllerGetJoystick(_controller));
                            logf(LogLevel.info, "Controller %s open for input", SDL_GameControllerName(_controller).to!string);
                        }
                    }
                    break;
                case SDL_CONTROLLERDEVICEREMOVED:
                    if (_controller && _controllerID == event.cdevice.which)
                    {
                        SDL_GameControllerClose(_controller);
                        logf(LogLevel.info, "Controller %s closed for input", SDL_GameControllerName(_controller).to!string);
                    }
                    break;
                default:
                    break;
            }
        }

        static this()  { SDL_InitSubSystem(SDL_INIT_EVENTS | SDL_INIT_GAMECONTROLLER); }
        static ~this() { SDL_QuitSubSystem(SDL_INIT_EVENTS | SDL_INIT_GAMECONTROLLER); }
        
        ~this()
        {
            if (_controller)
            {
                SDL_GameControllerClose(_controller);
            }
        }
}
