module client.main;

import client.audio,
       client.gfx,
       client.gui,
       client.input,
       client.resource,
       client.world;
import common.config;
import dvec.matrix,
       dvec.vector;
import std.algorithm,
       std.exception,
       std.file,
       std.format,
       std.math,
       std.typecons;
import std.datetime.stopwatch;
import std.experimental.logger;

//mingw-w64 libraries do not have this symbol, workaround for windows
version(Windows) extern(C) void __chkstk() {}

void createConfig(ref OptionManager options)
{
    //Create configuration options accessible in a "settings.conf" file
    options = OptionManager("settings.conf");

    with (options)
    {
        auto user = createSaved!"username"("Player");
        enforce(user.length <= Player.maxUsernameLen, "Username too long");
        auto val = createSaved!"resW"(640);
        enforce(val >= 0, "Resolution must be positive");
        val = createSaved!"resH"(480);
        enforce(val >= 0, "Resolution must be positive");
        val = createSaved!"fullscreen"(0);
        enforce(val == 0 || val == 1, "Fullscreen option must be 0 (off) or 1 (on)");
        val = createSaved!"vsync"(1);
        enforce(val == 0 || val == 1, "Vertical synchronisation must be 0 (off) or 1 (on)");
        val = createSaved!"msaa"(8);
        enforce(val >= 1 && val <= 64 && !( ((cast(uint)val) >>> 1) & cast(uint)val ), "MSAA sample count must be a power of 2 from 1 to 64");
        val = createSaved!"guiSize"(2);
        enforce(val >= 1, "GUI size must be 1 or greater");
        
        /*auto active = createSaved!"activeDistance"(8);
        enforce(active >= 3, "Active chunk distance must be 3 chunks or greater");*/
        val = createSaved!"viewDistance"(8);
        enforce(val >= 3, "View distance must be 3 chunks or greater");
        //enforce(val >= active, "View distance must be greater than or equal to active chunk distance");
        
        save;
    }
}

void main()
{
    debug sharedLog.logLevel = LogLevel.all;

    debug logf(LogLevel.info, "Mantle version 0.0.3 debug (%s D-%s)", __VENDOR__, __VERSION__);
    else  logf(LogLevel.info, "Mantle version 0.0.3 release (%s D-%s)", __VENDOR__, __VERSION__);
    
    //Load a graphics configuration file
    OptionManager options;
    try
    {
        options.createConfig;
    }
    catch (Exception error)
    {
        logf(LogLevel.error, "Failed to load settings.conf (%s), reseting config file", error.msg);
        options = OptionManager();
        std.file.remove("settings.conf");
        options.createConfig;
    }

    //Setup a window
    auto window = options.num!("resW", uint) == 0 || options.num!("resH", uint) == 0 ? 
                      Window("Mantle", options.num!("fullscreen", bool)) : 
                      Window("Mantle", options.num!("resW", uint), options.num!("resH", uint), options.num!("fullscreen", bool));
    auto renderer = Renderer(window, options.num!("vsync", bool), options.num!("msaa", uint), 16);
    auto mixer = AudioPlayer(44100, 16);
    auto events = InputHandler();
    window.trap = true;

    //Setup for main loop
    auto resources = ResourceManager!()(renderer, 16, 5, 1, 21, 7);
    resources.loadArchives("base.marc");
    
    auto world = scoped!InternalWorldServer("New World", options.str!"username", options.num!("viewDistance", uint), options.num!("viewDistance", uint));
    auto player = world.player;
    auto worldRenderer = scoped!WorldRenderer(renderer, resources, world, options.num!("viewDistance", uint));
    auto worldAudio = scoped!WorldAudioPlayer(resources, mixer, world);
    auto gui = GUI(worldRenderer, resources, world.player, options.num!("guiSize", float));
    resources.unloadTemp;
    
    with (player)
    {
        onHand[0] = ItemStore(Item(true, blockID!"Stone"     ), 1);
        onHand[1] = ItemStore(Item(true, blockID!"Sand"      ), 1);
        onHand[2] = ItemStore(Item(true, blockID!"Oak Planks"), 1);
        onHand[3] = ItemStore(Item(true, blockID!"Oak Leaves"), 1);
        onHand[4] = ItemStore(Item(true, blockID!"Torch"     ), 1);
    }
    
    float targetFOV = 1.5;
    float height = 0.0;
    auto fov = targetFOV;
    bool showGUI = true;
    
    StopWatch delta;
    delta.start;

    //Main loop
    while (!events.toQuit)
    {
        //Handle user input
        events.update;

        dvec3 playerVelo = 0;
        with (playerVelo)
        {
            auto controllerMove = events.leftStick;
            
            //Player movement keys and stick axes
            x = events.key(Key.D)*1 + events.key(Key.A)*-1;
            if (abs(controllerMove.x) > abs(x)) { x = controllerMove.x; }
            
            y = (events.key(Key.Space) || events.button(ControllerButton.A))*1 + events.key(Key.Q)*-1;
            
            z = events.key(Key.W)*-1 + events.key(Key.S)*1;
            if (abs(controllerMove.y) > abs(z)) { z = controllerMove.y; }
        }
        
        if (events.key(Key.F10)) { showGUI = !showGUI;      }
        if (events.key(Key.F11)) { window.toggleFullscreen; }
        
        foreach (i; Key.One..Key.Nine+1)
        {
            if (events.key(cast(Key)i))
            {
                //For the first digit key from 0 detected as pressed, select the corresponding on-hand inventory slot
                player.onHandSelect = cast(ubyte)(i-Key.One);
                break;
            }
        }

        //Update the camera space origin point and orientation in world space
        auto dt = clamp(delta.peek.total!"nsecs", 0, (10L^^9)/24);
        auto dtf = dt/1e9;
        delta.reset;

        auto mouse = events.mouse;
        auto rightStick = events.rightStick;
        world.movePlayer(playerVelo, (vec2(mouse[1], mouse[0]) * 0.010) + (vec2(rightStick[1], rightStick[0]) * 0.08), events.key(Key.Shift) || events.button(ControllerButton.X), events.key(Key.F));
        auto speed = player.velo.mag;

        //Update the world manager
        height = height*(1-dtf) - sin(((world.time - player.timeLastStatic)/1e9)*3*speed)*0.06*speed*dtf*cast(bool)(player.state & EntityFlags.TouchedGround);
        world.selectBlock;
        worldRenderer.selectBlock = world._selectBlock;
        if (events.mouseLeft || events.rightTrigger > 0.8)
        {
            world.breakSelect;
        }
        else if (events.mouseRight || events.leftTrigger > 0.8)
        {
            world.placeBlock;
        }
        world.update(dt);

        //Render and present
        if (playerVelo.z < 0)
        {
            targetFOV = (player.state & EntityFlags.Running) ? 1.7 : 1.55;
        }
        else
        {
            targetFOV = 1.5;
        }
        fov = fov*(1-(dtf*20)) + targetFOV*dtf*20;

        auto headPos = player.origin + Player.headPos;
        headPos.y += height;
        worldAudio.update(headPos, height < 0 && dvec2(playerVelo.x, playerVelo.z).magSquared > 0 && (player.state & EntityFlags.TouchedGround));
        worldRenderer.render(perspective!mat4(float(renderer.h)/renderer.w, fov, 0.1, worldRenderer.zFar), headPos, player.headOrient);
        if (showGUI)
        {
            gui.render(dtf);
        }
        
        renderer.present;
    }
}
