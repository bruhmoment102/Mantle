module client.resource;

import client.audio,
       client.gfx;
import client.data.marc;
public import common.data.common;
import common.pool;
import core.memory;
import std.conv,
       std.exception,
       std.format,
       std.stdio,
       std.zlib;
import std.experimental.logger;
     
/* A resource to be managed by a resource manager */
abstract class Resource
{
	private:
	    UUID _type;
        MARCResourceLife _lifetime;
        
    public:
        final auto type() { return _type; }
}

/* A collection of resources to be made available to other resources and managed by a resource manager */
struct ResourceStore
{
    private:
        Resource[UUID] _registry;
        
    public:
        void rehash() { _registry.rehash; }
        
        auto get(T)(UUID uuid)
        {
            debug logf(LogLevel.trace, "Fulfilling request for resource with UUID %016X", uuid);
            
            auto resource = _registry.get(uuid, null);
            assert(resource, format!"Attempt to use resource with UUID %016X that is not loaded"(uuid));
            return resource.to!T;
        }
        auto get(T, string name)() { return get!T(strUUID!name); }
        
        void store(UUID uuid, Resource toStore) { _registry[uuid] = toStore; }
}

/* Basic resource loader that uses pool allocators to handle various resources used by the rendering engine */
struct ResourceLoader
{
    private:
        Renderer* _renderer;

        /* Pools used to allocate resource objects */
        PoolAllocator!TextureResource _textures;
        PoolAllocator!ModelResource _models;
        PoolAllocator!ShaderResource _shaders;
        PoolAllocator!AudioResource _audio;
        PoolAllocator!StagingTextureResource _stagingTextures;
        PoolAllocator!StagingBuffer _staging;
        PoolAllocator!StagingModel _stagingModel;
        
        StagingBuffer*[] _stagingBufs;
        StagingModel*[] _stagingModels;
    
    public:
        ref auto renderer() { return *_renderer; }
    
        Resource allocResource(ref ResourceStore store, UUID type, void[] data)
        {
            final switch (type)
            {
                case strUUID!"audio":
                    return _audio.alloc(data);
                case strUUID!"shader_vertex":
                    return _shaders.alloc(_renderer.device, ShaderType.Vertex, data);
                case strUUID!"shader_fragment":
                    return _shaders.alloc(_renderer.device, ShaderType.Fragment, data);
                case strUUID!"block_texture": 
                    return _stagingTextures.alloc(*_renderer, data);
                case strUUID!"model":
                    _stagingModels ~= &(_stagingModel.alloc());
                    return _models.alloc(*_renderer, *_stagingModels[$-1], store, data); 
                case strUUID!"texture":
                    _stagingBufs ~= &(_staging.alloc());
                    return _textures.alloc(*_renderer, *_stagingBufs[$-1], data, TextureConfig.None);
                case strUUID!"texture_array":
                    _stagingBufs ~= &(_staging.alloc());
                    return _textures.alloc(*_renderer, *_stagingBufs[$-1], data, TextureConfig.PreTextureArray);
            }
        }
        
        void deallocResource(Resource resource)
        {
            final switch (resource.type)
            {
                case strUUID!"audio":
                    _audio.free(resource.to!AudioResource);
                    break;
                case strUUID!"shader_vertex",
                     strUUID!"shader_fragment":
                     _shaders.free(resource.to!ShaderResource);
                     break;
                case strUUID!"block_texture":
                    _renderer.waitIdle;
                    _stagingTextures.free(resource.to!StagingTextureResource);
                    break;
                case strUUID!"model":
                    _renderer.waitIdle;
                    _models.free(resource.to!ModelResource);
                    break;
                case strUUID!"texture",
                     strUUID!"texture_array":
                    _renderer.waitIdle;
                    _textures.free(resource.to!TextureResource);
                    break;
            }
        }
        
        void postload()
        {
            //Destroy allocated staging buffer objects after all asynchronous operations are done, as well as shader objects that are not needed after pipeline creation
            _renderer.waitIdle;
            _stagingTextures.destroy;
            
            foreach (i; _stagingModels)
            {
                _stagingModel.free(*i);
            }
            GC.free(_stagingModels.ptr);
            _stagingModel.destroy;
            
            foreach (i; _stagingBufs)
            {
                _staging.free(*i);
            }
            GC.free(_stagingBufs.ptr);
            _staging.destroy;
            
            if (_shaders.allocCount == 0)
            {
                _shaders.destroy;
            }
        }

        this(ref Renderer renderer, uint stagingTextureLimit, uint textureLimit, uint modelLimit, uint shaderLimit, uint audioLimit)
        {
            _renderer = &renderer;
            
            //Allocate pool memory
            _stagingTextures = typeof(_stagingTextures)(stagingTextureLimit);
            _textures        = typeof(_textures)(textureLimit);
            _staging         = typeof(_staging)(textureLimit + modelLimit);
            _stagingModel    = typeof(_stagingModel)(modelLimit);
            _models          = typeof(_models)(modelLimit);
            _shaders         = typeof(_shaders)(shaderLimit);
            _audio           = typeof(_audio)(audioLimit);
            
            _stagingBufs.reserve(_staging.size);
            _stagingModels.reserve(_stagingModel.size);
        }
}

/* Resource manager framework type that manages resources in a resource store by use of a loader for individual resources */
struct ResourceManager(Loader = ResourceLoader)
{
    private:
        Loader _loader;
        
        void unloadResource(UUID uuid, string archive = null)
        {
            debug
            {
                if (archive is null)
                {
                    logf(LogLevel.trace, "Unloading resource with UUID %016X", uuid);
                }
                else
                {
                    logf(LogLevel.trace, "Unloading resource with UUID %016X from archive %s", uuid, archive);
                }
            }

            _loader.deallocResource(store._registry[uuid]);
            store._registry.remove(uuid);
        }

    public:
        ResourceStore store;
        alias store this;
        
        void unloadResource(string name)() { unloadResource(strUUID!name); }
        void unloadType(string name)() { unloadType(strUUID!name); }
    
        void loadArchives(string[] paths ...)
        {
            //Load resources from all resource archive files
            foreach (path; paths)
            {
                auto archive = File(path, "r");
                debug log(LogLevel.trace, "Loading resources from archive ", path);
                
                //Read and validate the header
                MARCHeader header;
                rawRead(archive, header);
                enforce(header.valid, format!"Invalid header for resource archive file %s"(path));
                archive.seek(header.numResource * ulong.sizeof * 2, SEEK_CUR);

                //Load the resources in the archive
                void[] resourceData;
                foreach (i; 0..header.numResource)
                {
                    //Read the metadata header for the resource
                    MARCResourceHeader meta;
                    rawRead(archive, meta);
                    
                    //Read the name for the resource and throw an exception if it is of an unregistered type
                    char[MARCResourceHeader.maxNameChars] nameBuf;
                    auto resourceName = nameBuf[];
                    enforce(archive.readln(resourceName, '\0') != 0, format!"Invalid name for resource with UUID %016X"(meta.uuid));
                    enforce(resourceName.length <= MARCResourceHeader.maxNameChars, format!"Name is too long for resource with UUID %016X"(meta.uuid));
                    resourceName = resourceName[0..$-1];
                    
                    //Log the resource being loaded in debug builds, and then load the resource data
                    debug logf(LogLevel.trace, "Loading resource %s (UUID %016X, lifetime %s)", resourceName, meta.uuid, meta.lifetime);
                    resourceData.length = meta.compressedLen;
                    try
                    {
                        auto uncompressed = uncompress(archive.rawRead(resourceData), meta.uncompressedLen);
                        
                        auto resource = _loader.allocResource(store, meta.typeUUID, uncompressed);
                        resource._type = meta.typeUUID;
                        resource._lifetime = meta.lifetime;
                        store.store(meta.uuid, resource);
                        
                        GC.free(uncompressed.ptr);
                    }
                    catch (Exception exception)
                    {
                        throw new Exception(format!"Failed to load resource %s of archive %s (%s)"(resourceName, path, exception.msg));
                    }
                }
                debug log(LogLevel.trace, "Finished loading resources from archive ", path);
            }
        }
        
        void unloadTemp()
        {
            //Unload temporaries and rehash the resource registry
            foreach (i,e; store._registry)
            {
                if (e._lifetime == MARCResourceLife.Temporary)
                {
                    unloadResource(i);
                }
            }
            
            _loader.postload;
            store.rehash;
        }
        
        void unloadArchives(string[] paths ...)
        {
	        foreach (path; paths)
	        {
	            auto archive = File(path, "r");
	            
	            //Read and validate the header
	            MARCHeader header;
	            rawRead(archive, header);
	            enforce(header.valid, format!"Invalid header for resource file %s"(path));
	            
	            //Iterate through all resource UUIDs at the table in the beginning of the archive
	            foreach (i; 0..header.numResource)
	            {
		            UUID uuid;
		            archive.rawRead((&uuid)[0..1]);
		            archive.seek(UUID.sizeof, SEEK_CUR);
		            unloadResource(uuid, path);
	            }
            }
        }
        
        void unloadAll()
        {
            foreach (i,e; store._registry)
            {
                unloadResource(i);
            }
        }
        
        this(T...)(auto ref T args) { _loader = Loader(args); }
        
        ~this() { unloadAll; }
}
