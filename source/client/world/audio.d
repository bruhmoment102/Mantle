module client.world.audio;

import client.audio,
       client.resource;
import common.world.chunk,
       common.world.living,
       common.world.manager;
import dvec.vector;
import std.algorithm,
       std.math;

class WorldAudioPlayer : WorldListener
{
    private:
        dvec3 _pos;
        AudioPlayer* _audio;
        ResourceStore* _resources;
        WorldManager _world;
        Player _player;
        ulong _lastWalkSound;
        
    public:
        override void blockUpdate(in ivec3 pos, uint oldType, uint newType)
        {
            auto volume = 5/(dvec3(pos)-_pos).magSquared;
            if (oldType != blockID!"Air" && newType == blockID!"Air")
            {
                //Play a block breaking sound using 4 possible channels
                _audio.playStream(3, 7, _resources.get!(AudioResource, "break_grass.wav").audio, volume);
            }
            else if (oldType != newType)
            {
                //Play a block placing sound using 4 possible channels
                _audio.playStream(7, 11, _resources.get!(AudioResource, "place.wav").audio, volume);
            }
        }
        
        override void lightUpdate(in ivec3 minPos, in ivec3 maxPos) {}
    
        final void update(in dvec3 pos, bool walking)
        {
            //Save the current position and play ambient sounds
            _pos = pos;
            
            //TODO: make this more efficient instead of locking and unlocking the audio thread repeatedly
            auto dayVolume = clamp(1 - (3.5 * abs(_world.time/(24*60*1e9) - 0.5))^^4, 0, 1);
            _audio.playStream(0, _resources.get!(AudioResource, "plains_day.wav"  ).audio, dayVolume);
            _audio.setVolumeTarget(0, dayVolume);
            _audio.playStream(1, _resources.get!(AudioResource, "plains_night.wav").audio, 1-dayVolume);
            _audio.setVolumeTarget(1, 1-dayVolume);

            auto runtime = _world.runtime;
            if (walking && (runtime-_lastWalkSound) > 10^^8)
            {
                //Find the position of the block under the player
                auto playerPos = _player.origin;
                playerPos.y -= 1;
                foreach (ref i; playerPos)
                {
                    i = floor(i);
                }
                
                //Play a sound when the player is walking
                auto playerSpeed = _player.velo.mag;
                AudioResource toPlay;
                switch (_world.getBlockPtr(ivec3(_player.origin) - ivec3(0,1,0)).type)
                {
                    case blockID!"Stone": toPlay = _resources.get!(AudioResource, "walk_stone.wav"); break;
                    case blockID!"Grass": toPlay = _resources.get!(AudioResource, "walk_grass.wav"); break;
                    case blockID!"Sand" : toPlay = _resources.get!(AudioResource, "walk_sand.wav") ; break;
                    default: break;
                }
                
                if (toPlay)
                {
                    _lastWalkSound = runtime;
                    _audio.playStream(2, toPlay.audio, playerSpeed/Player.runSpeed);
                }
            }
        }
    
        this(ref ResourceStore resources, ref AudioPlayer mixer, WorldManager world, uint playerID = 0)
        {
            _audio = &mixer;
            _resources = &resources;
            _world = world;
            _player = world.player(playerID);
            world.register(this);
        }
}
