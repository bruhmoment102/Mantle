module client.world.client;

import common.data.mserv;
import common.pool;
import common.world.chunk,
       common.world.entity,
       common.world.manager,
       common.world.living,
       common.world.server;
import core.memory;
import dvec.vector;
import etc.c.zlib;
import std.algorithm,
       std.conv,
       std.exception,
       std.format,
       std.math,
       std.socket,
       std.typecons;
import std.experimental.logger;

class WorldClient : WorldManager
{
    private:
        PoolAllocator!Chunk _chunks;
        typeof(scoped!TcpSocket()) _client;
        typeof(scoped!LocalPlayer("", dvec3())) _player = void;
        int _playerChunkLoad;

        /* Member variables used in receiving data from server */
        z_stream _inflation;
        uint _receiveOffset;
        uint _chunkRecSize;
        MServDataType _receiving;
        
        MServChunkHeader _chunkRecHeader;
        ubyte* _zlibBuf;
        
        /* Member variables used to send data to server */
        double _lastServerSend;
        dvec3 _lastPlayerMovement;
        
    public:
        override LocalPlayer player(uint id = 0) in (id == 0) { return _player; }
        override ulong runtime() { return 0; }
        
        override void movePlayer(in dvec3 movement, in vec2 headRot, bool running, bool toggleFlying, uint playerID = 0)
        {
            //Update player movements locally
            assert(playerID == 0);
            super.movePlayer(movement, headRot, running, toggleFlying);
            _lastPlayerMovement = movement;
        }
        
        override void onHandSelect(ubyte slot, uint id = 0) in (id == 0) { _player.onHandSelect = slot; }
        
        override void message(string msg)
        {
            assert(msg.length <= MServChatMessageHeader.maxLength, "Chat message length exceeds limit");
            auto header = MServChatMessageHeader(cast(ubyte)msg.length);   
            _client.send((&header)[0..1]);
            _client.send(msg);     
        }
        
        override void update(ulong nsDT)
        {
            //A day is equivalent to 24 minutes
            _dt = nsDT;
            _time = (_time+nsDT)%(24*60*(10^^9));
            
            //Update player
            _player.velo.y -= 14*(nsDT/1e9);
            if (move(player, player.velo*(nsDT/1e9)))
            {
                _player.state |= EntityFlags.TouchedGround;
                _player.velo.y = 0;
            }
            else
            {
                _player.state &= ~EntityFlags.TouchedGround;
            }
            
            //Unload chunks which are out of the load distance
            auto center = _player.pos.chunkCoord;
            foreach (i; _chunkLookup.byKey)
            {
                if ((i-center).magSquared > finalDistanceSq)
                {
                    _chunks.free(*_chunkLookup[i]);
                    _chunkLookup.remove(i);
                }
            }
            
            //Process data incoming from the server
            while (true)
            {
                if (_receiving == MServDataType.None)
                {
                    if (_client.receive((&_receiving)[0..1]) > 0)
                    {
                        _receiveOffset = MServDataType.sizeof;
                    }
                    else
                    {
                        break;
                    }
                }

                //Update data being received
                switch (_receiving)
                {
                    case MServDataType.Disconnect:
                        throw new Exception("Server has shut down");
                    case MServDataType.Chunk:
                        //Receive chunk data
                        if (_receiveOffset < MServChunkHeader.sizeof)
                        {
                            //Receive the header
                            _receiveOffset += _client.receive((cast(void[])((&_chunkRecHeader)[0..1]))[_receiveOffset .. $]);
                            if (_receiveOffset == MServChunkHeader.sizeof)
                            {
                                _chunkRecSize = 0;
                            }
                        }
                        if (_receiveOffset == MServChunkHeader.sizeof)
                        {
                            //Receive the compressed chunk block data
                            _chunkRecSize += _client.receive(_zlibBuf[_chunkRecSize.._chunkRecHeader.len]).clamp(0, uint.max);
                            if (_chunkRecSize == _chunkRecHeader.len)
                            {
                                //If this chunk is still in the load distance then save it, otherwise discard it
                                auto magSquared = dvec3(_chunkRecHeader.coord.x-center.x, _chunkRecHeader.coord.x-center.x, _chunkRecHeader.coord.x-center.x).magSquared;
                                if (magSquared <= finalDistanceSq)
                                {
                                    auto chunk = &(_chunks.alloc());
                                    scope(failure) _chunks.free(*chunk);
                                
                                    inflateReset(&_inflation);
                                    with (_inflation)
                                    {
                                        next_in   = _zlibBuf;
                                        avail_in  = _chunkRecSize;
                                        next_out  = cast(ubyte*)&chunk.blocks;
                                        avail_out = cast(uint)ChunkBlocks.sizeof;
                                    }
                                    auto ret = inflate(&_inflation, Z_NO_FLUSH);
                                    enforce(ret == Z_STREAM_END, format!"Failed to decompress chunk data: inflate returned %s"(ret));
                                    
                                    _chunkLookup[_chunkRecHeader.coord] = chunk;
                                    _playerChunkLoad = max(_playerChunkLoad, cast(int)sqrt(magSquared));
                                }
                                _receiving = MServDataType.None;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            
            //Send data to the server at a predetermined rate
            if (_time-_lastServerSend > 1.0/20.0)
            {
                auto toSend = MServPlayerMovement(_player.pos, _player.headOrient, _lastPlayerMovement, cast(bool)(_player.state & EntityFlags.Running));
                _client.send((&toSend)[0..1]);
                _lastServerSend = _time;
            }
        }
    
        this(string host, string username, uint activeDistance, uint viewDistance)
            in (activeDistance > 2 && activeDistance <= int.max)
        {
            //Connect to a server
            log(LogLevel.info, "Client connecting to ", host);
            auto resolver = scoped!InternetHost;
            enforce(resolver.getHostByName(host), format!"Failed to resolve hostname %s"(host));
            
            auto addr = scoped!InternetAddress(resolver.addrList[0], WorldServer.defaultPort);
            _client = scoped!TcpSocket(addr);
            auto auth = MServAuth(activeDistance, viewDistance, "Player");
            _client.send((&auth)[0..1]);
            
            //Check if the connection request was successful and receive world state data
            MServAuthResponse response;
            _client.receive((&response)[0..1]);
            
            logf(LogLevel.warning, response.actualActiveDistance != activeDistance, "Server has selected %s view distance instead of default %s", response.actualActiveDistance, activeDistance);
            super(response.actualActiveDistance+2);
            _chunks = typeof(_chunks)(finalDistance.radiusChunks);
            
            final switch (response.code) with (MServAuthResponseCode)
            {
                case Ok     : log(LogLevel.info, "Authentification successful"); break;
                case BadAuth: throw new Exception("Failed to connect: authentification failed");
            }
            
            MServWorldState state;
            _client.receive((&state)[0..1]);
            _time = state.runtime % (60*24*(10L^^9));
            _lastServerSend = state.runtime;

            PlayerSaveState playerState = void;
            _client.receive((&playerState)[0..1]);
            _player = scoped!LocalPlayer(username, playerState);
            _client.blocking = false;
            
            //Create zlib streams for use in chunk data decompression
            enforce(inflateInit(&_inflation) == Z_OK, format!"Failed to initialise zlib stream for chunk decompression: %s"(_inflation.msg.to!string));
            _zlibBuf = new ubyte[ChunkBlocks.sizeof].ptr;
        }
        
        ~this()
        {
            //Send a disconnect signal to the server
            if (_client)
            {
                auto disconnect = MServDataType.Disconnect;
                _client.send((&disconnect)[0..1]);
            }
            
            //Free memory allocations and cleanup
            foreach (i; _chunkLookup)
            {
                _chunks.free(*i);
            }
            if (!_inflation.msg)
            {
                inflateEnd(&_inflation);
            }
            GC.free(_zlibBuf);
        }
}
