module client.world.renderer;

import client.gfx.buffer,
       client.gfx.model,
       client.gfx.pipeline,
       client.gfx.renderer,
       client.gfx.texture;
import client.resource;
import common.pool,
       common.queue;
import common.world.chunk,
       common.world.manager;
import dvec.matrix,
       dvec.noise,
       dvec.vector;
import std.algorithm,
       std.exception,
       std.format,
       std.math;
       
private 
{
    /* Colors used in skybox gradient */
    struct SkyColor
    {
        align(16) vec3 top;
        align(16) vec3 bottom;
    }

    shared immutable SkyColor[3] skyColors = [SkyColor(vec3(0.01, 0.02, 0.07), vec3(0.03, 0.05, 0.12)),  //Night
                                              SkyColor(vec3(0.16, 0.26, 0.38), vec3(1.00, 0.80, 0.33)),  //Evening/morning
                                              SkyColor(vec3(0.06, 0.16, 1.00), vec3(0.38, 1.00, 1.00))]; //Midday
                                              
    /* Chunk model type which has a counter for removal that considers frames in flight */
    struct ChunkModel
    {
        size_t frameID;
        ChunkModel* old;
        bool inGen;
        ubyte removalTimer;
        Model!2 model;
        
        alias model this;
        
        this(ref Renderer renderer, ref StagingModel staging, uint fluidOffset) { model = Model!2(renderer, staging, fluidOffset); }
    }
    
    struct ChunkModelStaging
    {
        ivec3 chunk;
        ChunkModel* target;
        StagingModel staging;
        
        alias staging this;
    }
}                           

/* World renderer type that manages chunk models for rendering */
class WorldRenderer : WorldListener
{
    private:
        //Allocators used for model and staging buffer objects as well as world block data
        PoolAllocator!ChunkModel _chunkModels;
        ChunkModelStaging[8] _chunkStaging;
        Queue!ivec3 _toUpdate;

        ivec3 _modelCenter;
        int _modelRadius;
        
        //Graphics pipelines used in rendering
        enum PipeIn
        {
            Sky, Block, Entity, Sun, Cloud, Fluid, Outline, Fill, DisplayBlock, Depth
        }
        GfxPipeline[PipeIn.max+1] _pipes;
        
        Model!1 _skyModel;
        Model!1 _sunModel;
        Model!2 _cubeModel;
        Texture _blockAtlas;
        Texture*[2] _sunTextures;
        ResourceStore* _resources;
        
        //Associative arrays used to access allocated model and block data
        ChunkModel*[ivec3] _modelLookup;
        
        WorldManager _world;
        size_t _curFrame;
        immutable int _viewDistance;
        immutable int _viewDistanceSq;
        float _zFar;
        public ivec3 selectBlock;
        
        auto insertFaceIndex(ref StagingModel staging)
        {
            //Generate the indices for a face to use in an index buffer
            auto rel = staging.curVertexLength;
            auto index = staging.getIndex(6);
            index[0] = cast(uint)(rel-4);
            index[1] = cast(uint)(rel-3);
            index[2] = cast(uint)(rel-2);
            index[3] = cast(uint)(rel-3);
            index[4] = cast(uint)(rel-1);
            index[5] = cast(uint)(rel-2);
        }
        
        auto lightAt(dvec3 pos, float skyLight)
        {
            //The light intensity stored for each block is at the center
            pos -= dvec3(1.0/2, 1.0/2, 1.0/2);
        
            //Find the block that the input position falls into
            auto rel = pos % 1;
            foreach (ref i; rel)
            {
                if (i < 0)
                {
                    i = 1+i;
                }
            }
            auto box = ivec3(pos - rel);
        
            //Interpolate the adjacent light intensities
            vec4[2][2] light = void;
            foreach (x; 0..2)
            {
                foreach (y; 0..2)
                {
                    light[x][y] = ((vec4(_world.readBlock(box + ivec3(x,y,0)).light.blockRGBSVec)/15) * (1-rel.z)) + ((vec4(_world.readBlock(box + ivec3(x,y,1)).light.blockRGBSVec)/15) * rel.z);
                }
            }
            auto lightFinal = ( ((light[0][0] * (1-rel.y)) + (light[0][1] * rel.y)) * (1-rel.x) ) + ( ((light[1][0] * (1-rel.y)) + (light[1][1] * rel.y)) * rel.x);
            
            //Apply sky lighting to the RGB components
            skyLight *= lightFinal.a^^2;
            foreach (ref i; lightFinal)
            {
                i = max(i^^2, skyLight);
            }
            return vec3(lightFinal.r, lightFinal.g, lightFinal.b);
        }
                
        void setNorm(Vertex[] toSet, uint normal)
        {
            foreach (ref i; toSet)
            {
                i.norm = normal;
            }
        }
        
        auto calcVertLighting(in ivec3 chunkWorldPos, BlockSide dir, Vertex[] vertex)
        {
            //Calculate the block range to consider based on the vertex normal
            ivec3[2] ranges = cast(ivec3[2])[ivec3(-1,-1,-1), ivec3(+1,+1,+1)];
            switch (dir)
            {
                case BlockSide.PlusY : ranges[0].y += 1; break;
                case BlockSide.MinusX: ranges[1].x -= 1; break;
                case BlockSide.PlusX : ranges[0].x += 1; break;
                case BlockSide.MinusZ: ranges[1].z -= 1; break;
                case BlockSide.PlusZ : ranges[0].z += 1; break;
                case BlockSide.MinusY: ranges[1].y -= 1; break;
                default              : break;
            }
        
            //Iterate through each vertex to consider
            foreach (ref i; vertex)
            {
                //Find the total light color and sky light exposure for each block adjacent to a vertex
                auto color = vec4(0,0,0,0);
                uint numConsidered;
                
                foreach (x; ranges[0].x..ranges[1].x)
                {
                    foreach (y; ranges[0].y..ranges[1].y)
                    {
                        foreach (z; ranges[0].z..ranges[1].z)
                        {
                            auto block = _world.readBlock(ivec3(cast(int)i.pos.x+x, cast(int)i.pos.y+y, cast(int)i.pos.z+z) + chunkWorldPos);
                            if ((blockTypes[block.type].light >>> 12) < 15)
                            {
                                color += vec4((block.light >>> 12) & 0xF, (block.light >>> 8) & 0xF, (block.light >>> 4) & 0xF, block.light & 0xF);
                                numConsidered += 1;
                            }
                        }
                    }
                }
                
                //Find the number of surrounding blocks that are solid for edge shadowing
                uint numSolid;
                foreach (x; -1..1)
                {
                    foreach (y; -1..1)
                    {
                        foreach (z; -1..1)
                        {
                            if ((blockTypes[_world.readBlock(ivec3(cast(int)i.pos.x+x, cast(int)i.pos.y+y, cast(int)i.pos.z+z) + chunkWorldPos).type].light >>> 12) == 15)
                            {
                                numSolid += 1;
                            }
                        }
                    }
                }
                
                //Average the total light color and sky light exposure, make edges that are less exposed have darker lighting
                color /= numConsidered;
                i.color = ((6-(clamp(numSolid, 4, 8)/2)) << 16) | (cast(uint)color.r << 12) | (cast(uint)color.g << 8) | (cast(uint)color.b << 4) | (cast(uint)color.a);
            }
        }
        
        auto setupVert(in ivec3 chunkWorldPos, ref Vertex[4] vertex, uint texture, uint normal)
        {
            //Setup texture coordinates based on the allowed rotation and flipping for the texture
            uint rot;
            final switch (blockTextures[texture].rotate)
            {
                case BlockTextureRotate.None:
                    rot = 0;
                    break;
                case BlockTextureRotate.Horizontal:
                    rot = cast(uint)( white(3, (cast(double)vertex[0].pos.x)+chunkWorldPos.x, (cast(double)vertex[0].pos.y)+chunkWorldPos.y, (cast(double)vertex[0].pos.z)+chunkWorldPos.z)*2 );
                    break;
                case BlockTextureRotate.All:
                    rot = cast(uint)( white(3, (cast(double)vertex[0].pos.x)+chunkWorldPos.x, (cast(double)vertex[0].pos.y)+chunkWorldPos.y, (cast(double)vertex[0].pos.z)+chunkWorldPos.z)*16 );
                    break;
            }
            assert(rot < 16, "Invalid block rotation index");
            
            auto indices = ( cast(ubyte[4][4])[[0,1,2,3], [2,0,3,1], [3,2,1,0], [1,3,0,2]] )[rot/4];
            final switch (rot % 4)
            {
                case 0:
                    vertex[indices[0]].tex = vec3(0,0,texture);
                    vertex[indices[1]].tex = vec3(1,0,texture);
                    vertex[indices[2]].tex = vec3(0,1,texture);
                    vertex[indices[3]].tex = vec3(1,1,texture);
                    break;
                case 1:
                    vertex[indices[0]].tex = vec3(1,0,texture);
                    vertex[indices[1]].tex = vec3(0,0,texture);
                    vertex[indices[2]].tex = vec3(1,1,texture);
                    vertex[indices[3]].tex = vec3(0,1,texture);
                    break;
                case 2:
                    vertex[indices[0]].tex = vec3(0,1,texture);
                    vertex[indices[1]].tex = vec3(1,1,texture);
                    vertex[indices[2]].tex = vec3(0,0,texture);
                    vertex[indices[3]].tex = vec3(1,0,texture);
                    break;
                case 3:
                    vertex[indices[0]].tex = vec3(1,1,texture);
                    vertex[indices[1]].tex = vec3(0,1,texture);
                    vertex[indices[2]].tex = vec3(1,0,texture);
                    vertex[indices[3]].tex = vec3(0,0,texture);
                    break;
            }
            setNorm(vertex, normal);
        
            //Calculate per-vertex lighting
            BlockSide side;
            switch (normal)
            {
                case vertNorm!(vec3(+0,+1,+0)): side = BlockSide.PlusY ; break;
                case vertNorm!(vec3(-1,+0,+0)): side = BlockSide.MinusX; break;
                case vertNorm!(vec3(+1,+0,+0)): side = BlockSide.PlusX ; break;
                case vertNorm!(vec3(+0,+0,-1)): side = BlockSide.MinusZ; break;
                case vertNorm!(vec3(+0,+0,+1)): side = BlockSide.PlusZ ; break;
                case vertNorm!(vec3(+0,-1,+0)): side = BlockSide.MinusY; break;
                default                       : side = BlockSide.Inside; break;
            }
            calcVertLighting(chunkWorldPos, side, vertex);
        }
        
        auto genChunkModel(ref ChunkModelStaging target, in ivec3 chunk)
        {
            target.reset;
            auto pos = chunk.chunkWorldPos;

            //Add non-fluid blocks to the model
            foreach (int x; 0..chunkSize)
            {
                foreach (int y; 0..chunkSize)
                {
                    foreach (int z; 0..chunkSize)
                    {
                        //Insert faces for each visible side of the current block
                        auto blockType = _world.readBlock(pos + ivec3(x,y,z)).type;
                        if (blockType != blockID!"Air")
                        {
                            if (blockTypes[blockType].shape == BlockShape.Cube)
                            {
                                if ((blockTypes[_world.readBlock(pos + ivec3(x,y+1,z)).type].light >>> 12) < 15)
                                {
                                    auto vertices = target.getVertex(4);
                                    auto texture = blockTypes[blockType].faceTextures[0];
                                    vertices[0] = Vertex(vec3(x+0, y+1, z+0));
                                    vertices[1] = Vertex(vec3(x+1, y+1, z+0));
                                    vertices[2] = Vertex(vec3(x+0, y+1, z+1));
                                    vertices[3] = Vertex(vec3(x+1, y+1, z+1));
                                    setupVert(pos, *cast(Vertex[4]*)(vertices.ptr), texture, vertNorm!(vec3(+0,+1,+0)));
                                    insertFaceIndex(target);
                                }
                                if ((blockTypes[_world.readBlock(pos + ivec3(x,y,z-1)).type].light >>> 12) < 15)
                                {
                                    auto vertices = target.getVertex(4);
                                    auto texture = blockTypes[blockType].faceTextures[1];
                                    vertices[0] = Vertex(vec3(x+0, y+0, z+0));
                                    vertices[1] = Vertex(vec3(x+1, y+0, z+0));
                                    vertices[2] = Vertex(vec3(x+0, y+1, z+0));
                                    vertices[3] = Vertex(vec3(x+1, y+1, z+0));
                                    setupVert(pos, *cast(Vertex[4]*)(vertices.ptr), texture, vertNorm!(vec3(+0,+0,-1)));
                                    insertFaceIndex(target);
                                }
                                if ((blockTypes[_world.readBlock(pos + ivec3(x,y,z+1)).type].light >>> 12) < 15)
                                {
                                    auto vertices = target.getVertex(4);
                                    auto texture = blockTypes[blockType].faceTextures[2];
                                    vertices[0] = Vertex(vec3(x+1, y+0, z+1));
                                    vertices[1] = Vertex(vec3(x+0, y+0, z+1));
                                    vertices[2] = Vertex(vec3(x+1, y+1, z+1));
                                    vertices[3] = Vertex(vec3(x+0, y+1, z+1));
                                    setupVert(pos, *cast(Vertex[4]*)(vertices.ptr), texture, vertNorm!(vec3(+0,+0,+1)));
                                    insertFaceIndex(target);
                                }
                                if ((blockTypes[_world.readBlock(pos + ivec3(x-1,y,z)).type].light >>> 12) < 15)
                                {
                                    auto vertices = target.getVertex(4);
                                    auto texture = blockTypes[blockType].faceTextures[3];
                                    vertices[0] = Vertex(vec3(x+0, y+0, z+1));
                                    vertices[1] = Vertex(vec3(x+0, y+0, z+0));
                                    vertices[2] = Vertex(vec3(x+0, y+1, z+1));
                                    vertices[3] = Vertex(vec3(x+0, y+1, z+0));
                                    setupVert(pos, *cast(Vertex[4]*)(vertices.ptr), texture, vertNorm!(vec3(-1,+0,+0)));
                                    insertFaceIndex(target);
                                }
                                if ((blockTypes[_world.readBlock(pos + ivec3(x+1,y,z)).type].light >>> 12) < 15)
                                {
                                    auto vertices = target.getVertex(4);
                                    auto texture = blockTypes[blockType].faceTextures[4];
                                    vertices[0] = Vertex(vec3(x+1, y+0, z+0));
                                    vertices[1] = Vertex(vec3(x+1, y+0, z+1));
                                    vertices[2] = Vertex(vec3(x+1, y+1, z+0));
                                    vertices[3] = Vertex(vec3(x+1, y+1, z+1));
                                    setupVert(pos, *cast(Vertex[4]*)(vertices.ptr), texture, vertNorm!(vec3(+1,+0,+0)));
                                    insertFaceIndex(target);
                                }
                                if ((blockTypes[_world.readBlock(pos + ivec3(x,y-1,z)).type].light >>> 12) < 15)
                                {
                                    auto vertices = target.getVertex(4);
                                    auto texture = blockTypes[blockType].faceTextures[5];
                                    vertices[0] = Vertex(vec3(x+0, y+0, z+1));
                                    vertices[1] = Vertex(vec3(x+1, y+0, z+1));
                                    vertices[2] = Vertex(vec3(x+0, y+0, z+0));
                                    vertices[3] = Vertex(vec3(x+1, y+0, z+0));
                                    setupVert(pos, *cast(Vertex[4]*)(vertices.ptr), texture, vertNorm!(vec3(+0,-1,+0)));
                                    insertFaceIndex(target);
                                }
                            }
                            else if (blockTypes[blockType].shape == BlockShape.Torch)
                            {
                                auto start = vec3(x,y,z) + vec3(7,0,7)/16;
                                auto end   = start + vec3(2,14,2)/16;
                                
                                //Insert torch model vertices
                                auto vertex = target.getVertex(4*4);
                                vertex[0 ] = Vertex(vec3(end.x, start.y, end.z), vec3(0,0,0));
                                vertex[1 ] = Vertex(vec3(end.x, start.y, start.z), vec3(0,0,0));
                                vertex[2 ] = Vertex(vec3(end.x, end.y, end.z  ), vec3(0,1,0));
                                vertex[3 ] = Vertex(vec3(end.x, end.y, start.z), vec3(0,1,0));
                                
                                vertex[4 ] = Vertex(vec3(start.x, start.y, end.z), vec3(0,0,0));
                                vertex[5 ] = Vertex(vec3(start.x, start.y, start.z), vec3(0,0,0));
                                vertex[6 ] = Vertex(vec3(start.x, end.y, end.z), vec3(0,1,0));
                                vertex[7 ] = Vertex(vec3(start.x, end.y, start.z), vec3(0,1,0));
                                
                                vertex[8 ] = Vertex(vec3(start.x, start.y, end.z), vec3(0,0,0));
                                vertex[9 ] = Vertex(vec3(start.x, start.y, start.z), vec3(0,0,0));
                                vertex[10] = Vertex(vec3(end.x, end.y, end.z), vec3(0,1,0));
                                vertex[11] = Vertex(vec3(end.x, end.y, start.z), vec3(0,1,0));
                                
                                vertex[12] = Vertex(vec3(end.x, start.y, end.z), vec3(0,0,0));
                                vertex[13] = Vertex(vec3(end.x, start.y, start.z), vec3(0,0,0));
                                vertex[14] = Vertex(vec3(start.x, end.y, end.z), vec3(0,1,0));
                                vertex[15] = Vertex(vec3(start.x, end.y, start.z), vec3(0,1,0));
                                
                                //Insert indices
                                target.getIndex(6*2)[] = cast(uint[6*2])[0,1,2,1,2,0,4,5,6,5,6,4];
                                
                                calcVertLighting(pos, BlockSide.Inside, vertex);
                            }
                            else if (blockTypes[blockType].shape == BlockShape.Foliage)
                            {
                                auto offset = vec2(0,0);
                                if (blockType == blockID!"Tall Grass")
                                {
                                    //Apply pseudo-random offsets for tall grass blocks
                                    auto worldPos = pos + ivec3(x,y,z);
                                    offset.x += (white(1, cast(double)worldPos.x, cast(double)worldPos.y, cast(double)worldPos.z)-0.5)*0.4;
                                    offset.y += (white(2, cast(double)worldPos.x, cast(double)worldPos.y, cast(double)worldPos.z)-0.5)*0.4;
                                }
                                
                                auto vertices = target.getVertex(8);
                                auto texture = blockTypes[blockType].faceTextures[0];
                                vertices[0] = Vertex(vec3(x+0+offset.x, y+0, z+0+offset.y), vec3(0, 0, texture));
                                vertices[1] = Vertex(vec3(x+1+offset.x, y+0, z+1+offset.y), vec3(1, 0, texture));
                                vertices[2] = Vertex(vec3(x+0+offset.x, y+1, z+0+offset.y), vec3(0, 1, texture));
                                vertices[3] = Vertex(vec3(x+1+offset.x, y+1, z+1+offset.y), vec3(1, 1, texture));
                                vertices[4] = Vertex(vec3(x+0+offset.x, y+0, z+1+offset.y), vec3(0, 0, texture));
                                vertices[5] = Vertex(vec3(x+1+offset.x, y+0, z+0+offset.y), vec3(1, 0, texture));
                                vertices[6] = Vertex(vec3(x+0+offset.x, y+1, z+1+offset.y), vec3(0, 1, texture));
                                vertices[7] = Vertex(vec3(x+1+offset.x, y+1, z+0+offset.y), vec3(1, 1, texture));
                                calcVertLighting(pos, BlockSide.Inside, vertices);
                                setNorm(vertices, vertNorm!(vec3(+0,+1,+0)));
                                
                                auto indices = target.getIndex(24);
                                indices[] = cast(uint[24])[0,1,2,1,3,2,4,5,6,5,7,6,1,0,3,0,2,3,5,4,7,4,6,7];
                                indices[] += cast(uint)(target.curVertexLength - vertices.length);
                            }
                        }
                    }
                }
            }
            
            //Add fluid blocks to the model
            auto fluidOffset = target.curIndexLength;
            foreach (int x; 0..chunkSize)
            {
                foreach (int y; 0..chunkSize)
                {
                    foreach (int z; 0..chunkSize)
                    {
                        //Insert faces for each visible side of the current block
                        auto blockType = _world.readBlock(pos + ivec3(x,y,z)).type;
                        if (blockTypes[blockType].shape == BlockShape.Fluid)
                        {
                            ushort adjType;
                            
                            adjType = _world.readBlock(pos + ivec3(x,y+1,z)).type;
                            if (adjType != blockType && (blockTypes[adjType].shape == BlockShape.Cube || (blockTypes[adjType].light >>> 12) < 15))
                            {
                                auto vertices = target.getVertex(4);
                                auto texture = blockTypes[blockType].faceTextures[0];
                                vertices[0] = Vertex(vec3(x+0, y+0.9, z+0), vec3(0, 0, texture));
                                vertices[1] = Vertex(vec3(x+1, y+0.9, z+0), vec3(1, 0, texture));
                                vertices[2] = Vertex(vec3(x+0, y+0.9, z+1), vec3(0, 1, texture));
                                vertices[3] = Vertex(vec3(x+1, y+0.9, z+1), vec3(1, 1, texture));
                                
                                calcVertLighting(pos, BlockSide.PlusY, vertices);
                                setNorm(vertices, vertNorm!(vec3(+0,+1,+0)));
                                insertFaceIndex(target);
                            }
                            adjType = _world.readBlock(pos + ivec3(x,y,z-1)).type;
                            if (adjType != blockType && (blockTypes[adjType].light >>> 12) < 15)
                            {
                                auto vertices = target.getVertex(4);
                                auto texture = blockTypes[blockType].faceTextures[1];
                                vertices[0] = Vertex(vec3(x+0, y+0.0, z+0), vec3(0, 0.0, texture));
                                vertices[1] = Vertex(vec3(x+1, y+0.0, z+0), vec3(1, 0.0, texture));
                                vertices[2] = Vertex(vec3(x+0, y+0.9, z+0), vec3(0, 0.9, texture));
                                vertices[3] = Vertex(vec3(x+1, y+0.9, z+0), vec3(1, 0.9, texture));
                                
                                calcVertLighting(pos, BlockSide.MinusZ, vertices);
                                setNorm(vertices, vertNorm!(vec3(+0,+0,-1)));
                                insertFaceIndex(target);
                            }
                            adjType = _world.readBlock(pos + ivec3(x,y,z+1)).type;
                            if (adjType != blockType && (blockTypes[adjType].light >>> 12) < 15)
                            {
                                auto vertices = target.getVertex(4);
                                auto texture = blockTypes[blockType].faceTextures[2];
                                vertices[0] = Vertex(vec3(x+1, y+0.0, z+1), vec3(0, 0.0, texture));
                                vertices[1] = Vertex(vec3(x+0, y+0.0, z+1), vec3(1, 0.0, texture));
                                vertices[2] = Vertex(vec3(x+1, y+0.9, z+1), vec3(0, 0.9, texture));
                                vertices[3] = Vertex(vec3(x+0, y+0.9, z+1), vec3(1, 0.9, texture));
                                
                                calcVertLighting(pos, BlockSide.PlusZ, vertices);
                                setNorm(vertices, vertNorm!(vec3(+0,+0,+1)));
                                insertFaceIndex(target);
                            }
                            adjType = _world.readBlock(pos + ivec3(x-1,y,z)).type;
                            if (adjType != blockType && (blockTypes[adjType].light >>> 12) < 15)
                            {
                                auto vertices = target.getVertex(4);
                                auto texture = blockTypes[blockType].faceTextures[3];
                                vertices[0] = Vertex(vec3(x+0, y+0.0, z+1), vec3(0, 0.0, texture));
                                vertices[1] = Vertex(vec3(x+0, y+0.0, z+0), vec3(1, 0.0, texture));
                                vertices[2] = Vertex(vec3(x+0, y+0.9, z+1), vec3(0, 0.9, texture));
                                vertices[3] = Vertex(vec3(x+0, y+0.9, z+0), vec3(1, 0.9, texture));
                                
                                calcVertLighting(pos, BlockSide.MinusX, vertices);
                                setNorm(vertices, vertNorm!(vec3(-1,+0,+0)));
                                insertFaceIndex(target);
                            }
                            adjType = _world.readBlock(pos + ivec3(x+1,y,z)).type;
                            if (adjType != blockType && (blockTypes[adjType].light >>> 12) < 15)
                            {
                                auto vertices = target.getVertex(4);
                                auto texture = blockTypes[blockType].faceTextures[4];
                                vertices[0] = Vertex(vec3(x+1, y+0.0, z+0), vec3(0, 0.0, texture));
                                vertices[1] = Vertex(vec3(x+1, y+0.0, z+1), vec3(1, 0.0, texture));
                                vertices[2] = Vertex(vec3(x+1, y+0.9, z+0), vec3(0, 0.9, texture));
                                vertices[3] = Vertex(vec3(x+1, y+0.9, z+1), vec3(1, 0.9, texture));
                                
                                calcVertLighting(pos, BlockSide.PlusX, vertices);
                                setNorm(vertices, vertNorm!(vec3(+1,+0,+0)));
                                insertFaceIndex(target);
                            }
                            adjType = _world.readBlock(pos + ivec3(x,y-1,z)).type;
                            if (adjType != blockType && (blockTypes[adjType].light >>> 12) < 15)
                            {
                                auto vertices = target.getVertex(4);
                                auto texture = blockTypes[blockType].faceTextures[5];
                                vertices[0] = Vertex(vec3(x+0, y+0.0, z+1), vec3(0, 0, texture));
                                vertices[1] = Vertex(vec3(x+1, y+0.0, z+1), vec3(1, 0, texture));
                                vertices[2] = Vertex(vec3(x+0, y+0.0, z+0), vec3(0, 1, texture));
                                vertices[3] = Vertex(vec3(x+1, y+0.0, z+0), vec3(1, 1, texture));
                                
                                calcVertLighting(pos, BlockSide.MinusY, vertices);
                                setNorm(vertices, vertNorm!(vec3(+0,-1,+0)));
                                insertFaceIndex(target);
                            }
                        }
                    }
                }
            }
            
            target.chunk = chunk;
            target.target = &(_chunkModels.alloc(_pipes[PipeIn.Block].renderer, target, fluidOffset));
            target.target.frameID = _curFrame;
            target.target.inGen = true;
            return target.target;
        }
        
        void cleanChunkModels()
        {
            //Iterate over all chunk model positions
            foreach (pos, latest; _modelLookup)
            {
                assert(latest);
                immutable outOfRange = ivec3(pos-_modelCenter).magSquared > _viewDistanceSq;

                //Keep track of the first chunk model after the latest one in use
                ChunkModel* cur;
                ChunkModel* prev;

                //Ignore the newest chunk model if in-generation
                if (latest.inGen)
                {
                    auto modelInUse = latest.old;
                    assert(!modelInUse || !modelInUse.inGen);
                    
                    //Keep count of how many frames the latest chunk model and front active chunk model are out of distance
                    if (outOfRange)
                    {
                        latest.removalTimer += latest.removalTimer < Renderer.numFramesInFlight;
                        
                        if (modelInUse)
                        {
                            modelInUse.removalTimer += modelInUse.removalTimer < Renderer.numFramesInFlight;
                        
                            //If the front active chunk model goes then all further ones do too
                            if (modelInUse.removalTimer >= Renderer.numFramesInFlight)
                            {
                                auto next = modelInUse.old;
                            
                                _chunkModels.free(*modelInUse);
                                latest.old = null;
                                
                                cur = next;
                                prev = latest;
                            }
                            else
                            {
                                cur = modelInUse.old;
                                prev = modelInUse;        
                            }
                        }
                    }
                    else
                    {
                        latest.removalTimer = 0;
                        
                        if (modelInUse)
                        {
                            modelInUse.removalTimer = 0;
                            
                            cur = modelInUse.old;
                            prev = modelInUse;
                        }
                    }
                }
                else
                {
                    //Keep count of how many frames the front chunk model is out of distance
                    if (outOfRange)
                    {
                        //If the front active chunk model goes then all further ones do too
                        latest.removalTimer += latest.removalTimer < Renderer.numFramesInFlight;
                            
                        if (latest.removalTimer >= Renderer.numFramesInFlight)
                        {
                            auto next = latest.old;
                            _chunkModels.free(*latest);
                            _modelLookup.remove(pos);
                            cur = next;
                        }
                        else
                        {
                            cur = latest.old;
                            prev = latest;
                        }
                    }
                    else
                    {
                        latest.removalTimer = 0;
                        cur = latest.old;
                        prev = latest;
                    }
                }

                //If there are active chunk models to consider then update their removal counters unconditionally
                while (cur)
                {
                    assert(!cur.inGen);
                    
                    auto next = cur.old;
                    cur.removalTimer += 1;
                    
                    if (cur.removalTimer >= Renderer.numFramesInFlight)
                    {
                        _chunkModels.free(*cur);
                        
                        //End the linked list at the last remaining chunk model
                        if (prev)
                        {
                            prev.old = null;
                            prev = null;
                        }
                    }
                    else
                    {
                        prev = cur;
                    }

                    cur = next;
                }
            }
            
            //Check that the number of chunk models being managed matches the number of model allocations
            debug
            {
                size_t count;
                foreach (i; _modelLookup)
                {
                    for (ChunkModel* cur = i; cur; cur = cur.old)
                    {
                        count += 1;
                    }
                }
                debug assert(count == _chunkModels.allocCount, format!"Chunk model memory leak detected: %s in management, %s allocated in pool"(count, _chunkModels.allocCount));
            }
        }

        auto findNextStaging(uint index)
        {
            //Return the index number of the staging buffer to use for chunk models
            foreach (i, ref e; _chunkStaging[index..$])
            {
                if (e.finished)
                {
                    if (e.target)
                    {
                        e.target.inGen = false;
                    }
                    return cast(int)(index+i);
                }
            }
            return -1;
        }
        
        void invalidateChunkModel(in ivec3 chunk)
        {
            if (_world.isSurrounded(chunk))
            {
                auto model = _modelLookup.get(chunk, null);
                if (model)
                {
                    if (!model.inGen && model.frameID != _curFrame)
                    {
                        //If the latest model is not in-gen or was created this frame then find a staging buffer to use
                        int staging = findNextStaging(0);
                        if (staging >= 0)
                        {
                            //If there is a staging buffer then rebuild the model
                            auto newModel = genChunkModel(_chunkStaging[staging], chunk);
                            newModel.old = model;
                            _modelLookup[chunk] = newModel;
                        }
                        else
                        {
                            //Otherwise queue the model for rebuilding
                            _toUpdate.put(chunk);
                        }
                    }
                    else
                    {
                        //Otherwise queue the model for rebuilding
                        _toUpdate.put(chunk);
                    }
                }
            }
        }
        
        void load(in ivec3 center)
        {
            if (_modelCenter != center)
            {
                //If the center chunk has changed then reset the minimum distance and do a full sweep of the view distance to find chunks needing model generation
                _modelRadius -= min(_modelRadius, (_modelCenter - center).mag+1);
                _modelCenter = center;
            }
            
            //Update the chunk model generation queue
            cleanChunkModels;
            auto updateLen = _toUpdate.length;
            foreach (i; 0..updateLen)
            {
                invalidateChunkModel(_toUpdate.front);
                _toUpdate.popFront;
            }
            
            //Generate new models by iterating through all possible radii
            int staging = findNextStaging(0);
            if (staging < 0)
            {
                //All staging buffers are busy being copied from
                return;
            }

            /*TODO: make this calculate the surface positions of the spheres searched rather than brute-forcing every chunk in a square area
             *I tried this and it was too difficult
             */
            foreach (immutable radius; _modelRadius.._viewDistance+1)
            {
                foreach (x; -radius..radius+1)
                {
                    foreach (y; -radius..radius+1)
                    {
                        foreach (z; -radius..radius+1)
                        {
                            auto xyz = ivec3(x,y,z);
                            auto magSquared = xyz.magSquared;
                            
                            //For every radius until the maximum, consider chunks with the distance [r, r+1).
                            //At the maximum only consider chunks whose distance from the center chunk is exactly the radius.
                            if ((radius < _viewDistance && magSquared >= radius^^2 && magSquared < (radius+1)^^2) ||
                                (magSquared == radius^^2))
                            {
                                auto pos = center+xyz;
                                if (pos !in _modelLookup)
                                {
                                    if (_world.isSurrounded(pos))
                                    {
                                        //If the chunk model does not exist create it if possible
                                        _modelLookup[pos] = genChunkModel(_chunkStaging[staging], pos);
                                        staging = findNextStaging(staging);
                                        if (staging < 0)
                                        {
                                            //All staging buffers are busy being copied from
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        //If not possible to create a model then exit the loop
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
                _modelRadius += 1;
            }
        }
        
    public:
        ref auto renderer() { return _pipes[0].renderer; }
        auto zFar() { return _zFar * (sqrt(2.0f)+0.5f); }
        
        override void blockUpdate(in ivec3 pos, uint oldType, uint newType) {}
                
        override void lightUpdate(in ivec3 min, in ivec3 max)
        {
            //Find the relevant models to update when a block update occurs
            auto minChunk = min.chunkCoord;
            auto maxChunk = max.chunkCoord;
            auto minRel = min - minChunk.chunkWorldPos;
            auto maxRel = max - maxChunk.chunkWorldPos;
            
            foreach (axis; 0..3)
            {
                if (minRel[axis] == 0)
                {
                    minChunk[axis] -= 1;
                }
                if (maxRel[axis] == chunkSize-1)
                {
                    maxChunk[axis] += 1;
                }
            }

            //Iterate over the selected models and invalidate them
            maxChunk += 1;
            foreach (x; minChunk.x .. maxChunk.x)
            {
                foreach (y; minChunk.y .. maxChunk.y)
                {
                    foreach (z; minChunk.z .. maxChunk.z)
                    {
                        invalidateChunkModel(ivec3(x,y,z));
                    }
                }
            }
        }
        
        final void renderItem(in Item item, in vec2 pos, in vec2 extent)
        {
            if (item.isBlock)
            {
                _blockAtlas.bind;
                _pipes[PipeIn.DisplayBlock].bind;
                
                if (blockTypes[item.id].shape == BlockShape.Cube)
                {
                    //If the item has a standard cube shape then render it as a cube
                    renderer.setVertexData(cast(uint[3])[blockTypes[item.id].faceTextures[BlockSide.PlusX],
                                                         blockTypes[item.id].faceTextures[BlockSide.PlusY],
                                                         blockTypes[item.id].faceTextures[BlockSide.PlusZ]]);
                    
                    _cubeModel.render(translate!mat4(vec3(pos.x, pos.y, 0)) * scale!mat4(extent.x, -extent.y, 0) 
                                    * pitch!mat4(PI/8) * yaw!mat4(-PI/4) * translate!mat4(vec3(1,1,1) * (-1.02/2)), 1);
                }
                else
                {
                    //Otherwise render it as one of its sides
                    renderer.setVertexData(blockTypes[item.id].faceTextures[0]);
                    _cubeModel.render(translate!mat4(vec3(pos.x, pos.y, 0)) * scale!mat4(extent.x, -extent.y, 0)
                                    * pitch!mat4(-PI/2) * translate!mat4(vec3(1,1,1) * (-1.02/2)));
                }
            }
        }
        
        final void render(in mat4 camToClip, in dvec3 pos, in vec2 orient)
        {
            auto center = pos.chunkCoord;
            load(center);
            
            //Set the sky light brightness. The sun travels anticlockwise on the XY plane.
            auto timef = _world.time/1e9;
            auto timeScale = (timef / (60*24))*2*PI - (PI/2);
            auto timeSin = cast(float)clamp(sin(timeScale), 0, 1);
            
            auto sunLight = timeSin;
            auto moonLight = cast(float)clamp(sin(timeScale+PI), 0, 1) * 0.01;
            auto skyLight = sunLight + moonLight;
            
            struct BlockData
            {
                vec3 sunLightDir;
                float sunLight;
                vec3 moonLightDir;
                float moonLight;
            }

            with (_pipes[PipeIn.Block])
            {
                renderer.setVertexData(BlockData(vec3(cos(timeScale), sin(timeScale), 0), sunLight,
                                                 vec3(cos(timeScale+PI), sin(timeScale+PI), 0), moonLight));  
                renderer.setFragmentData(cast(float)timef);
                bind;
            }
            _blockAtlas.bind;
            
            //Render all generated chunk models from a world space position with a given transformation
            auto centerPos = center.chunkWorldPos;
            auto chunkRel = vec3(-pos.x+centerPos.x, -pos.y+centerPos.y, -pos.z+centerPos.z);
            mat4 transform = camToClip * scale!mat4(1/1.1) * pitch!mat4(orient.pitch) * yaw!mat4(orient.yaw);
            mat4 chunkTransform = transform * translate!mat4(chunkRel);

            foreach (x; -_viewDistance.._viewDistance+1)
            {
                auto rangeYSq = _viewDistanceSq - x^^2;
                auto rangeY = cast(int)sqrt(cast(real)rangeYSq);

                foreach (y; -rangeY..rangeY+1)
                {
                    auto rangeZ = rangeYSq - y^^2;
                    if (rangeZ >= 0)
                    {
                        rangeZ = cast(int)sqrt(cast(real)rangeZ);
                    
                        foreach (z; -rangeZ..rangeZ+1)
                        {
                            auto chunk = _modelLookup.get(center+ivec3(x,y,z), null);

                            if (chunk)
                            {
                                if (chunk.inGen)
                                {
                                    chunk = chunk.old;
                                }
                                
                                if (chunk)
                                {
                                    chunk.render(chunkTransform * translate!mat4(vec3(x,y,z)*chunkSize));
                                }
                            }
                        }
                    }
                }
            }
            
            //Render entities
            struct EntityData
            {
                uint color;
                float skyLight;
            }
            auto entity = EntityData(0, skyLight);
            
            _pipes[PipeIn.Entity].bind;
            foreach (i; _world.entities)
            {
                auto relPos = vec3(i.pos - pos);
                if (relPos.magSquared <= (_viewDistance*chunkSize)^^2)
                {
                    auto block = _world.readBlockPtr(ivec3(i.pos));
                    if (block)
                    {
                        auto light = cast(uint)(block.light);
                        entity.color = (((light >>> 12) & 0xF) << 24) | (((light >>> 8) & 0xF) << 16) | (((light >>> 4) & 0xF) << 8) | (light & 0xF);
                        _pipes[PipeIn.Entity].renderer.setFragmentData(entity);
                        _resources.get!ModelResource(i.model).render(transform * translate!mat4(vec3(i.pos - pos)) * yaw!mat4(i.direction));
                    }
                }
            }
            
            //Render the block selection cube
            _pipes[PipeIn.Outline].bind;
            auto rel = dvec3(selectBlock) - pos;
            chunkRel = vec3(rel);
            _cubeModel.render(transform * translate!mat4(chunkRel));
            
            //Calculate and set the skybox colors to use
            struct SkyData
            {
                vec3 top;
                float time;
                vec3 bottom;
                float cloud;
            }
            SkyData sky;
            sky.time = cast(float)timef;
            sky.cloud = skyLight;

            if (timeSin >= 2.0/3.0)
            {
                //Day sky
                sky.top = skyColors[2].top;
                sky.bottom = skyColors[2].bottom;
                _pipes[PipeIn.Sky].renderer.setFragmentData(sky);
            }
            else 
            {
                if (_world.time < 12*60*(10^^9))
                {
                    //First half of day
                    if (timeSin <= 1.0/3.0)
                    {
                        //Night to morning sky
                        auto factor = timeSin*3;
                        sky.top = skyColors[0].top*(1-factor) + skyColors[1].top*factor;
                        sky.bottom = skyColors[0].bottom*(1-factor) + skyColors[1].bottom*factor;
                    }
                    else if (timeSin <= 2.0/3.0)
                    {
                        //Morning to day sky
                        auto factor = (timeSin - (1.0/3.0))*3;
                        sky.top = skyColors[1].top*(1-factor) + skyColors[2].top*factor;
                        sky.bottom = skyColors[1].bottom*(1-factor) + skyColors[2].bottom*factor;
                    }
                }
                else
                {
                    //Second half of day
                    if (timeSin <= 1.0/3.0)
                    {
                        //Morning to night sky
                        auto factor = timeSin*3;
                        sky.top = skyColors[1].top*factor + skyColors[0].top*(1-factor);
                        sky.bottom = skyColors[1].bottom*factor + skyColors[0].bottom*(1-factor);
                    }
                    else if (timeSin <= 2.0/3.0)
                    {
                        //Day to morning sky
                        auto factor = (timeSin - (1.0/3.0))*3;
                        sky.top = skyColors[2].top*factor + skyColors[1].top*(1-factor);
                        sky.bottom = skyColors[2].bottom*factor + skyColors[1].bottom*(1-factor);
                    }
                }
                _pipes[PipeIn.Sky].renderer.setFragmentData(sky);
            }

            //Render the skybox
            auto skyTransform = transform * scale!mat4(_zFar);  
            _pipes[PipeIn.Sky].bind;          
            _skyModel.render(skyTransform);
            
            //Render the fluid portions of the generated chunk models
            struct WaterData
            {
                ivec3 chunkPos;
                float time;
            }
            auto water = WaterData(ivec3(0,0,0), cast(float)timef);
            
            _pipes[PipeIn.Fluid].bind;
            _blockAtlas.bind;
            
            foreach (x; -_viewDistance.._viewDistance+1)
            {
                auto rangeYSq = _viewDistanceSq - x^^2;
                auto rangeY = cast(int)sqrt(cast(real)rangeYSq);

                foreach (y; -rangeY..rangeY+1)
                {
                    auto rangeZ = rangeYSq - y^^2;
                    if (rangeZ >= 0)
                    {
                        rangeZ = cast(int)sqrt(cast(real)rangeZ);
                    
                        foreach (z; -rangeZ..rangeZ+1)
                        {
                            auto chunkCoord = center+ivec3(x,y,z);
                            auto chunk = _modelLookup.get(chunkCoord, null);
                            
                            if (chunk)
                            {
                                if (chunk.inGen)
                                {
                                    chunk = chunk.old;
                                }
                                
                                if (chunk)
                                {
                                    water.chunkPos = chunkCoord.chunkWorldPos;
                                    _pipes[PipeIn.Fluid].renderer.setFragmentData(water);
                                    chunk.render(chunkTransform * translate!mat4(vec3(x,y,z)*chunkSize), 1);
                                }
                            }
                        }
                    }
                }
            }
            
            //Render the sun, moon and clouds
            _pipes[PipeIn.Depth].bind;
            _pipes[PipeIn.Depth].renderRange(skyTransform, 3);
            
            _pipes[PipeIn.Sun].bind;
            _sunTextures[0].bind;
            _sunModel.render(transform * scaleY!mat4(-1) * roll!mat4(timeScale) * scale!mat4(_zFar));
            _sunTextures[1].bind;
            _sunModel.render(transform * scaleY!mat4(-1) * roll!mat4(timeScale+PI) * scale!mat4(_zFar));
            
            _pipes[PipeIn.Sky].renderer.setFragmentData(sky);
            _pipes[PipeIn.Cloud].bind;
            _skyModel.render(skyTransform);
            
            //Render an overlay if the camera space origin point is in a water block
            if (_world.isSurrounded(center) && _world.readBlock(ivec3(dvec3(floor(pos.x), floor(pos.y), floor(pos.z)))).type == blockID!"Water")
            {
                struct FillData
                {
                    align(16) vec3 start;
                    align(16) vec3 end;
                }
                auto texID = blockTypes[blockID!"Water"].faceTextures[0];
                auto fill = FillData(vec3(0, 0, texID), vec3(1, 1, texID));
            
                _blockAtlas.bind;
                with (_pipes[PipeIn.Fill])
                {
                    bind;
                    renderer.setVertexData(fill);
                    renderer.setFragmentData(lightAt(pos, skyLight));
                    render(3);
                }
            }
            
            //Update the frame counter
            _curFrame += 1;
        }

        this(ref Renderer renderer, ref ResourceStore resources, WorldManager world, uint viewDistance)
            in (viewDistance >= 3 && viewDistance < sqrt(cast(real)int.max))
        {
            _toUpdate = Queue!ivec3(8);
            _world = world;
            world.register(this);

            //Use a view distance slightly higher than the one specified to make sure each chunk has adjacent chunks for model generation
            _viewDistance = viewDistance;
            _viewDistanceSq = viewDistance^^2;
            _zFar = sqrt(cast(float)(2*((chunkSize*viewDistance)^^2)));
            _resources = &resources;
            
            /*Set up allocators for chunks and chunk models, as well as staging buffers for chunk model data
             *Make sure enough allocations for each frame in flight as well as models in-generation are possible
             */
            _chunkModels = typeof(_chunkModels)((viewDistance.radiusChunks * Renderer.numFramesInFlight) + _chunkStaging.length);
            foreach (ref i; _chunkStaging)
            {
                i.staging = StagingModel(renderer, (chunkSize^^3)*6*6, (chunkSize^^3)*6*4, true);
            }
            
            //Create a model for arbitrary block rendering and block outlines
            auto outlineStaging = StagingModel(renderer, 24 + 6*3, 14, false);
            outlineStaging.getIndex(24)[]  = cast(uint[24])[0,2,0,3,1,3,2,1,5,1,7,3,4,2,6,0,4,5,4,6,5,7,6,7];
            outlineStaging.getIndex(6*3)[] = cast(uint[18])[0,1,2,1,0,3, 5,8,7,7,8,9, 10,11,12,12,11,13];
            
            auto vertex = outlineStaging.getVertex(14);
            vertex[0]  = Vertex(vec3(+1.01, +1.01, +1.01), vec3(1,1,2), vertNorm!(vec3(+1,+0,+0)));
            vertex[1]  = Vertex(vec3(+1.01, -0.01, -0.01), vec3(0,0,2), vertNorm!(vec3(+1,+0,+0)));
            vertex[2]  = Vertex(vec3(+1.01, -0.01, +1.01), vec3(1,0,2), vertNorm!(vec3(+1,+0,+0)));
            vertex[3]  = Vertex(vec3(+1.01, +1.01, -0.01), vec3(0,1,2), vertNorm!(vec3(+1,+0,+0)));
            vertex[4]  = Vertex(vec3(-0.01, -0.01, +1.01), vec3(0,0,0), vertNorm!(vec3(+0,+0,+0)));
            vertex[5]  = Vertex(vec3(-0.01, -0.01, -0.01), vec3(0,0,0), vertNorm!(vec3(+0,+0,-1)));
            vertex[6]  = Vertex(vec3(-0.01, +1.01, +1.01), vec3(0,0,0), vertNorm!(vec3(+0,+0,+0)));
            vertex[7]  = Vertex(vec3(-0.01, +1.01, -0.01), vec3(0,1,0), vertNorm!(vec3(+0,+0,-1)));
            vertex[8]  = Vertex(vec3(+1.01, -0.01, -0.01), vec3(1,0,0), vertNorm!(vec3(+0,+0,-1)));
            vertex[9]  = Vertex(vec3(+1.01, +1.01, -0.01), vec3(1,1,0), vertNorm!(vec3(+0,+0,-1)));
            vertex[10] = Vertex(vec3(-0.01, +1.01, -0.01), vec3(0,0,1), vertNorm!(vec3(+0,+1,+0)));
            vertex[11] = Vertex(vec3(+1.01, +1.01, -0.01), vec3(1,0,1), vertNorm!(vec3(+0,+1,+0)));
            vertex[12] = Vertex(vec3(-0.01, +1.01, +1.01), vec3(0,1,1), vertNorm!(vec3(+0,+1,+0)));
            vertex[13] = Vertex(vec3(+1.01, +1.01, +1.01), vec3(1,1,1), vertNorm!(vec3(+0,+1,+0)));

            _cubeModel = Model!2(renderer, outlineStaging, 24);
            
            //Create a skybox model
            auto skyStaging = StagingModel(renderer, 36, 8, false);
            skyStaging.getIndex(36)[] = cast(uint[36])[0,1,2,1,3,2,4,0,6,0,2,6,5,4,7,4,6,7,1,5,3,5,7,3,6,2,7,2,3,7,5,1,4,1,0,4];
            skyStaging.insertCubeVertex(vec3(-1,-1,-1), vec3(1,1,1));
            _skyModel = Model!1(renderer, skyStaging);
            
            auto sunStaging = StagingModel(renderer, 6, 4, false);
            sunStaging.getIndex(6)[] = [0,1,2,1,3,2];
            
            enum sunCos = cos(PI/48);
            enum sunSin = sin(PI/48);
            sunStaging.getVertex(4)[] = cast(Vertex[4])[Vertex(vec3(sunCos, +sunSin, -sunSin), vec3(0,1,0)),
                                                        Vertex(vec3(sunCos, +sunSin, +sunSin), vec3(1,1,0)),
                                                        Vertex(vec3(sunCos, -sunSin, -sunSin), vec3(0,0,0)),
                                                        Vertex(vec3(sunCos, -sunSin, +sunSin), vec3(1,0,0))];
            _sunModel = Model!1(renderer, sunStaging);
            
            _sunTextures[0] = &(resources.get!(TextureResource, "sun.bmp" ).texture);
            _sunTextures[1] = &(resources.get!(TextureResource, "moon.bmp").texture);
            
            //Create a texture array to use in rendering blocks
            StagingBuffer*[blockTextures.length] textures;
            auto first = resources.get!StagingTextureResource(blockTextures[0].texture);
            foreach (i,e; blockTextures)
            {
                textures[i] = &resources.get!StagingTextureResource(e.texture).buffer;
            }
            _blockAtlas = Texture(renderer, TextureConfig.MipMapped, first.w, first.h, textures);
            
            //Create graphics pipelines
            createGfxPipelines(renderer, _pipes, GfxPipelineConfig(GfxPipelineFlags.NoDepthWrite, resources.get!(ShaderResource, "sky.vspv").shader    , resources.get!(ShaderResource, "sky.fspv").shader   ),
                                                 GfxPipelineConfig(GfxPipelineFlags.None        , resources.get!(ShaderResource, "block.vspv").shader  , resources.get!(ShaderResource, "block.fspv").shader ),
                                                 GfxPipelineConfig(GfxPipelineFlags.None        , resources.get!(ShaderResource, "entity.vspv").shader , resources.get!(ShaderResource, "entity.fspv").shader),
                                                 GfxPipelineConfig(GfxPipelineFlags.NoDepthWrite, resources.get!(ShaderResource, "sun.vspv").shader    , resources.get!(ShaderResource, "sun.fspv").shader   ),
                                                 GfxPipelineConfig(GfxPipelineFlags.None        , resources.get!(ShaderResource, "sky.vspv").shader    , resources.get!(ShaderResource, "clouds.fspv").shader),
                                                 GfxPipelineConfig(GfxPipelineFlags.NoCulling   , resources.get!(ShaderResource, "block.vspv").shader  , resources.get!(ShaderResource, "water.fspv").shader ),
                                                 GfxPipelineConfig(GfxPipelineFlags.Line        , resources.get!(ShaderResource, "line.vspv").shader   , resources.get!(ShaderResource, "line.fspv").shader  ),
                                                 GfxPipelineConfig(GfxPipelineFlags.NoVertexData|
                                                                   GfxPipelineFlags.NoDepthWrite, resources.get!(ShaderResource, "fill.vspv").shader   , resources.get!(ShaderResource, "fill.fspv").shader  ),
                                                 GfxPipelineConfig(GfxPipelineFlags.NoDepthTest |
                                                                   GfxPipelineFlags.NoDepthWrite, resources.get!(ShaderResource, "display.vspv").shader, resources.get!(ShaderResource, "block.fspv").shader ),
                                                 GfxPipelineConfig(GfxPipelineFlags.NoVertexData, resources.get!(ShaderResource, "depth.vspv").shader  , resources.get!(ShaderResource, "depth.fspv").shader ));
            renderer.waitIdle;
        }
        
        ~this()
        {
            //Free all chunk model allocations
            _pipes[PipeIn.Block].renderer.waitIdle;
            foreach (i; _modelLookup)
            {
                auto cur = i;
                do
                {
                    auto next = cur.old;
                    _chunkModels.free(*cur);
                    cur = next;
                }
                while (cur);
            }
        }
}
