module common.config;

import common.data.common;
import std.algorithm,
       std.conv,
       std.exception,
       std.file,
       std.format,
       std.stdio,
       std.string;

/* Configuration option which may string-based or numeric */
private struct Option
{
    string name;
    bool toSave;
    bool isNumber;
    union
    {
        string str;
        double num;
    }
}

/* Manager of configuration options, which may or may not be saved into a text configuration file */
struct OptionManager
{
    private:
        Option[UUID] _options;
        string _configFile;
        bool _toSave;
        
        auto getOption(UUID uuid, bool isNumber)
        {
            //Obtain a configuration option, ensuring that it exists and the right type was selected by the caller
            enforce(uuid in _options, "Attempt to obtain configuration option that does not exist");
            
            auto option = &_options[uuid];
            enforce(option.isNumber == isNumber, option.isNumber ? format!"Attempt to obtain \"%s\" as string configuration option when it is a number"(option.name)
                                                                 : format!"Attempt to obtain \"%s\" as number configuration option when it is a string"(option.name));
            return option;
        }
        
        auto create(T)(string name, UUID uuid, T value, bool toSave = false)
            if (is(T==string) || is(T==double))
            in (uuid == name.toUUID)
        {
            //Create a new option with a specific associated value if it does not already exist
            if (uuid !in _options)
            {
                _options[uuid] = Option(name, toSave, is(T==double));
                auto option = &_options[uuid];
                
                if (toSave)
                {
                    _toSave = true;
                }
                
                //Set and return
                static if (is(T==double))
                {
                    option.num = value;
                    return option.num;
                }
                else
                {
                    option.str = value;
                    return option.str;
                }
            }
            else
            {
                //Return the value that is already associated with a configuration option
                static if (is(T==double))
                {
                    return _options[uuid].num;
                }
                else
                {
                    return _options[uuid].str;
                }
            }
        }
        
        void set(T)(UUID uuid, T value)
            if (is(T==string) || is(T==double))
        {
            //Set the associated value of an existing option
            auto option = getOption(uuid, is(T==double));
            
            static if (is(T==double))
            {
                option.num = value;
            }
            else
            {
                option.str = value;
            }
                
            if (option.toSave)
            {
                _toSave = true;
            }
        }
        
    public: 
        auto str(string name, T = string)() { return cast(T)getOption(strUUID!name, false).str; }
        auto num(string name, T = double)() { return cast(T)getOption(strUUID!name, true ).num; }

        void rehash() { _options.rehash; }

        /* Functions that create new string or numerical options if they don't exist */
        auto create(string name)(string value) { return create(name, strUUID!name, value, false); }
        auto createSaved(string name)(string value) { return create(name, strUUID!name, value, true); }
        
        auto create(string name)(double value) { return create(name, strUUID!name, value, false); }
        auto createSaved(string name)(double value) { return create(name, strUUID!name, value, true); }

        /* Functions for setting existing string or numerical options */
        void set(string name)(string value) { set(strUUID!name, value); }
        void set(string name)(double value) { set(strUUID!name, value); }

        /* Functions for loading and saving text configuration files containing name:value option strings */
        void load(string file)
        {
            //Remove all existing options and associate a new configuration file for this option manager
            _options = null;
            _configFile = file;
            
            //Load it if it exists
            if (file.exists)
            {
                auto save = File(file, "r");
                
                while (!save.eof)
                {
                    //Read a configuration option per line
                    string name;
                    string value;
                    save.readf("%s:%s\n", name, value);
                    name  = name.strip;
                    value = value.strip;

                    //Store the configuration option into the associative array
                    if (name.length > 0)
                    {
                        if (value[0] == '"' && value[$-1] == '"')
                        {
                            create(name, name.toUUID, value[1..$-1], true);
                        }
                        else
                        {
                            create(name, name.toUUID, value.to!double, true);
                        }
                    }
                }
                
                _toSave = false;
            }
        }
        
        void save()
        {
            assert(_configFile !is null, "Attempt to save options for option manager with no configuration file");
        
            rehash;
            if (_toSave)
            {
                _toSave = false;
            
                //Remove the configuration file for this option manager and create a new one with the relevant options in it
                if (_configFile.exists)
                {
                    remove(_configFile);
                }
                auto file = File(_configFile, "w");
                
                //For every option to save, write a name:value string into the configuration file
                foreach (i; _options)
                {
                    if (i.toSave)
                    {
                        file.write(i.name, ":");
                        if (i.isNumber)
                        {
                            file.writeln(i.num);
                        }
                        else
                        {
                            file.writefln("\"%s\"", i.str);
                        }
                    }
                }
            }
        }
        
        this(string file) { load(file); }
        
        ~this()
        {
            if (_configFile)
            {
                save;
            }
        }
}
