module common.data.common;

import std.stdio;

/* Convenience functions for reading and writing singular values to and from files */
void rawRead(T)(ref File file, ref T val) { file.rawRead((&val)[0..1]); }
void rawWrite(T)(ref File file, in T val) { file.rawWrite((&val)[0..1]); }

/* UUID type used to represent internal strings for efficient comparison at runtime */
alias UUID = ulong;
auto toUUID(const(char)[] str)
{
    UUID toReturn;

    foreach (i,e; str)
    {
        toReturn += i*e;
    }
    return toReturn;
}
enum strUUID(const(char)[] val) = val.toUUID;

enum nullUUID = UUID(0);
