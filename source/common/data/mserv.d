module common.data.mserv;

public import common.data.common;
import common.world.chunk,
       common.world.living;
import dvec.vector;

enum MServTimeout = 30;

struct MServAuth
{
    int activeDistance;
    int viewDistance;
    ubyte usernameLen;
    char[Player.maxUsernameLen] usernameBuf;
    
    bool valid() { return activeDistance > 0 && usernameLen <= Player.maxUsernameLen; }
    auto username() { return usernameBuf[0..usernameLen]; }
    
    this(int activeDistance, int viewDistance, string username)
    {
        assert(username.length <= Player.maxUsernameLen, "Username too long");
        this.activeDistance = activeDistance;
        this.viewDistance = viewDistance;
        
        usernameLen = cast(ubyte)username.length;
        usernameBuf[0..usernameLen] = username[];
        assert(valid);
    }
}

enum MServAuthResponseCode : ubyte
{
    Ok,
    BadAuth
}

struct MServAuthResponse
{
    int actualActiveDistance;
    MServAuthResponseCode code;
}

enum MServDataType : ubyte
{
    None,
    WorldState,
    Disconnect,
    Chunk,
    PlayerMovement,
    ChatMessage,
    AcceptExtraChunk
}

struct MServWorldState
{
    const type = MServDataType.WorldState;
    ulong runtime;

    this(ulong runtime) { this.runtime = runtime; }
}

struct MServChunkHeader
{
    const type = MServDataType.Chunk;
    ivec3 coord;
    uint len;
    
    this(in ivec3 coord, uint len)
    {
        this.coord = coord;
        this.len = len;
    }
}

struct MServPlayerMovement
{
    const type = MServDataType.PlayerMovement;
    dvec3 pos;
    vec2 headOrient;
    dvec3 movement;
    bool running;
    
    this(in dvec3 pos, in vec2 headOrient, in dvec3 movement, in bool running)
    {
        this.pos = pos;
        this.headOrient = headOrient;
        this.movement = movement;
        this.running = running;
    }
}

struct MServChatMessageHeader
{
    const type = MServDataType.ChatMessage;
    ubyte len;
    
    enum maxLength = ubyte.max;
    
    this(ubyte len) { this.len = len; }
}

struct MServChatMessageBuf
{
    MServChatMessageHeader header;
    char[MServChatMessageHeader.maxLength] chars;
    
    alias header this;
}
