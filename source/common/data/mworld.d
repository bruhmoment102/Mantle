module common.data.mworld;

public import common.data.common;
import common.world.chunk,
       common.world.living;
import dvec.vector;
import std.math;

struct MWorldHeader
{
    enum validMagic = "MWLDVER1";

    char[8] magic;
    uint seed;
    ivec3 player;
    ulong runtime;
    ulong firstPlayer;
    
    bool valid() { return magic == validMagic; }
}

/* Chunks are stored as part of 32*32*32 regions. Each region metadata structure is connected as part of a linked list */
enum mworldRegionSize = 32;

auto regionCoord(t)(in t chunk)
{
    //Convert a world space coordinate to an integer chunk coordinate
    ivec3 toReturn;
    foreach (i, ref e; chunk)
    {
        toReturn[i] = cast(int)floor((cast(float)e)/mworldRegionSize);
    }
    return toReturn;
}

auto regionChunkPos(t)(in t region)
{
    //Convert a region coordinate to a chunk coordinate
    ivec3 toReturn;
    foreach (i, ref e; region)
    {
        toReturn[i] = e*mworldRegionSize;
    }
    return toReturn;
}

struct MWorldRegionHeader
{
    ulong next;
    ivec3 region;
}
alias MWorldRegionOffsets = ulong[mworldRegionSize][mworldRegionSize][mworldRegionSize];

struct MWorldPlayer
{
    ulong nextPlayer;
    UUID nameUUID;
    char[16] name;
    ubyte usernameLen;
}

struct MWorldChunkHeader
{
    uint size;
    ChunkState state;
}
