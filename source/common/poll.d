module common.poll;

import core.atomic;
import std.experimental.logger;

/* Type implementing a compact binary semaphore entirely in user-space with polling */
struct PollLock
{
    private:
        bool _lock;
        
    public:
        auto tryLock() { return cas(&_lock, false, true); }
    
        void lock()
        {
            atomicFence;
            while (!cas(&_lock, false, true)) {}
        }
        
        void unlock()
        {
            atomicFence;
            atomicStore(_lock, false);
        }
}
