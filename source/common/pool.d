module common.pool;

import core.memory;
import std.array,
       std.conv,
       std.format,
       std.traits;

/* Pool allocator for values of any type, or instance objects of any class type */
struct PoolAllocator(T)
    if ((!is(T == class) && T.sizeof >= size_t.sizeof) ||
        ( is(T == class) && __traits(classInstanceSize,T) >= size_t.sizeof))
{
    private:
        static if (is(T == class))
        {
            struct Instance
            {
                align(classInstanceAlignment!T) void[__traits(classInstanceSize, T)] block;
            }
            Instance[] _pool;
        }
        else
        {
            T[] _pool;
        }
        size_t _freeIndex;
        size_t _allocCount;

    public:
        auto size() { return _pool.length; }
        auto allocCount() { return _allocCount; }
    
        static if (is(T == class))
        {
            T alloc(V...)(auto ref V args)
            {
                assert(_freeIndex < _pool.length, "No memory left in pool allocator for new allocation");
            
                //Create an allocation at the current free index, load the new free index
                T toReturn = cast(T)&_pool[_freeIndex];
                auto oldFreeIndex = _freeIndex;
                _freeIndex = *(cast(size_t*)toReturn);
                emplace!T((cast(void*)toReturn)[0..Instance.sizeof], args);
                
                //If the allocation was unsuccessful then restore the old free index
                scope (failure)
                {
                    *(cast(size_t*)toReturn) = _freeIndex;
                    _freeIndex = oldFreeIndex;
                }
                scope (success)
                {
                    _allocCount += 1;
                }
                
                return toReturn;
            }
            
            void free(T toFree)
                in (((cast(void*)toFree) - (cast(void*)_pool.ptr)) % Instance.sizeof == 0)
            {
                toFree.destroy;
            
                //Add the current free index to the linked list, set the current free index to the one to be freed
                *(cast(size_t*)toFree) = _freeIndex;
                _freeIndex = ((cast(void*)toFree) - (cast(void*)_pool.ptr)) / Instance.sizeof;
                _allocCount -= 1;
            }
        }
        else
        {
            ref T alloc()
            {
                assert(_freeIndex < _pool.length, format!"No memory left in pool allocator for new allocation (pool size %s)"(_pool.length));
            
                //Create an allocation at the current free index, load the new free index
                T* toReturn = &_pool[_freeIndex];
                auto oldFreeIndex = _freeIndex;
                _freeIndex = *(cast(size_t*)toReturn);
                *toReturn = T.init;
                
                //If the allocation was unsuccessful then restore the old free index
                scope (failure)
                {
                    *(cast(size_t*)toReturn) = _freeIndex;
                    _freeIndex = oldFreeIndex;
                }
                scope (success)
                {
                    _allocCount += 1;
                }
                
                return *toReturn;
            }
            
            ref T alloc(V...)(auto ref V args)
                if (is(T == struct))
            {
                assert(_freeIndex < _pool.length, format!"No memory left in pool allocator for new allocation (pool size %s)"(_pool.length));

                //Create an allocation at the current free index, load the new free index
                T* toReturn = &_pool[_freeIndex];
                auto oldFreeIndex = _freeIndex;
                _freeIndex = *(cast(size_t*)toReturn);
                *toReturn = T(args);
                
                //If the allocation was unsuccessful then restore the old free index
                scope (failure)
                {
                    *(cast(size_t*)toReturn) = _freeIndex;
                    _freeIndex = oldFreeIndex;
                }
                scope (success)
                {
                    _allocCount += 1;
                }
                
                return *toReturn;
            }
            
            void free(ref T toFree)
                in (((cast(void*)&toFree) - (cast(void*)_pool.ptr)) % T.sizeof == 0)
            {
                toFree.destroy;

                //Add the current free index to the linked list, set the current free index to the one to be freed
                auto a = _freeIndex;
                *(cast(size_t*)&toFree) = _freeIndex;
                _freeIndex = ((cast(void*)&toFree) - (cast(void*)_pool.ptr)) / T.sizeof;
                _allocCount -= 1;
            }
        }
                
        this(size_t size)
            in (size < size_t.max-1, "Allocator size exceeds maximum allowed")
        {
            _pool = uninitializedArray!(typeof(_pool))(size);
            static if (!is(T == class))
            {
                GC.clrAttr(_pool.ptr, GC.BlkAttr.FINALIZE);
            }
            
            //Create a linked list for all free elements
            foreach (i, ref e; _pool)
            {
                *(cast(size_t*)&e) = i+1;
            }
        }
        
        @disable this();
        
        /*debug ~this()
        {
            scope(exit) _pool = null;
        
            if (_pool)
            {
                //In debug builds traverse the free list to ensure that there are no memory leaks
                size_t numUnallocated = _pool.length - _allocCount;
                while (numUnallocated > 0)
                {
                    //Count the number of free blocks
                    _freeIndex = *(cast(size_t*)&_pool[_freeIndex]);
                    numUnallocated -= 1;
                    if (numUnallocated == 0 && _freeIndex > _pool.length)
                    {
                        //If the last block has been reached and the free list has ended then return
                        return;
                    }
                }
                assert(false, format!"Not all memory allocations created with pool allocator %s of type %s have been freed"(&this, typeof(this).stringof));
            }
        }*/
}
