module common.queue;

import core.memory;
import std.math;

//FIFO queue types using ring buffer array
struct Queue(T)
{
    private:
        T[] _elements;
        size_t _startOffset;
        size_t _remaining;
        
    public:
        void put(in T toPush)
        {
            //Resize the ring buffer array if full
            if (_remaining == _elements.length)
            {
                T[] newElements = new T[_elements.length*2];
                
                auto toEnd = _elements.length - _startOffset;
                if (toEnd < _remaining)
                {
                    //If the current array queue wraps around the end of the array to the beginning
                    newElements[0..toEnd] = _elements[_startOffset..$];
                    newElements[toEnd.._remaining] = _elements[0.._remaining-toEnd];
                }
                else
                {
                    //If the current array queue is stored contiguously in memory
                    newElements[0.._remaining] = _elements[_startOffset.._startOffset+_remaining];
                }
                
                GC.free(_elements.ptr);
                _startOffset = 0;
                _elements = newElements;
            }
        
            //Push an element onto the queue
            _elements[(_startOffset+_remaining) % $] = cast(T)toPush;
            _remaining += 1;
        }
        
        void popFront()
        {
            _startOffset = (_startOffset+1) % _elements.length;
            _remaining -= 1;
        }
        
        auto empty() { return !_remaining; }
        
        auto length() { return _remaining; }
        
        ref auto front() { return _elements[_startOffset]; }
        
        this(size_t initialSize) { _elements = new T[initialSize]; }
        
        @disable this(this);
        
        ~this() { GC.free(_elements.ptr); }
}
