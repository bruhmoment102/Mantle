module common.world.chunk;

import common.data.common;
import dvec.vector;
import std.algorithm,
       std.math;

/* Information about block types */
enum BlockShape : ubyte
{
    None,
    Cube,
    Foliage,
    Fluid,
    Torch
}

enum BlockSide : ubyte
{
    PlusY,
    MinusX,
    PlusX,
    MinusZ,
    PlusZ,
    MinusY,
    Inside
}

enum BlockTextureRotate
{
    None,
    Horizontal,
    All
}

struct BlockTexture
{
    UUID texture;
    BlockTextureRotate rotate;
}

struct BlockType
{
    string name;
    short hardness;
    ushort light;
    BlockShape shape;
    uint[6] faceTextures;
    
    this(string name, short hardness, ushort light, ubyte lightFilter, BlockShape shape, uint allFaces = 0)
        in (!(light & 0xF000) && lightFilter < 16)
    {
        this.name = name;
        this.shape = shape;
        this.hardness = hardness;
        
        //Set the light intensity emitted by this block type, as well as light filtering (upper 4 bits)
        this.light = cast(ushort)((light & 0xFFF) | (lightFilter << 12));
        
        faceTextures[] = allFaces;
    }
    this(string name, short hardness, BlockShape shape, uint allFaces = 0) { this(name, hardness, 0, 15, shape, allFaces); }
    this(string name, short hardness, ubyte lightFilter, BlockShape shape, uint allFaces = 0) { this(name, hardness, 0, lightFilter, shape, allFaces); }
    
    this(string name, short hardness, ushort light, ubyte lightFilter, BlockShape shape, ushort color, uint top, uint sides, uint bottom)
    {
        this(name, hardness, light, lightFilter, shape, sides);
        faceTextures[BlockSide.PlusY] = top;
        faceTextures[BlockSide.MinusY] = bottom;
    }
    this(string name, short hardness, ushort light, ubyte lightFilter, BlockShape shape, uint top, uint sides, uint bottom)
    {
        this(name, hardness, light, lightFilter, shape, sides);
        faceTextures[BlockSide.PlusY] = top;
        faceTextures[BlockSide.MinusY] = bottom;
    }
    this(string name, short hardness, ushort light, ubyte lightFilter, uint top, uint sides, uint bottom) { this(name, hardness, light, lightFilter, BlockShape.Cube, top, sides, bottom); }
    this(string name, short hardness, uint top, uint sides, uint bottom) { this(name, hardness, 0, 15, top, sides, bottom); }
}

shared immutable BlockTexture[13] blockTextures = [BlockTexture(strUUID!"stone.bmp"        , BlockTextureRotate.All       ),
                                                   BlockTexture(strUUID!"dirt.bmp"         , BlockTextureRotate.All       ),
                                                   BlockTexture(strUUID!"grass_top.bmp"    , BlockTextureRotate.All       ),
                                                   BlockTexture(strUUID!"grass_side.bmp"   , BlockTextureRotate.Horizontal),
                                                   BlockTexture(strUUID!"sand.bmp"         , BlockTextureRotate.None      ),
                                                   BlockTexture(strUUID!"torch_side.bmp"   , BlockTextureRotate.None      ),
                                                   BlockTexture(strUUID!"tall_grass.bmp"   , BlockTextureRotate.Horizontal),
                                                   BlockTexture(strUUID!"water.bmp"        , BlockTextureRotate.None      ),
                                                   BlockTexture(strUUID!"oak_top.bmp"      , BlockTextureRotate.None      ),
                                                   BlockTexture(strUUID!"oak_side.bmp"     , BlockTextureRotate.Horizontal),
                                                   BlockTexture(strUUID!"oak_leaves.bmp"   , BlockTextureRotate.All       ),
                                                   BlockTexture(strUUID!"oak_planks.bmp"   , BlockTextureRotate.Horizontal),
                                                   BlockTexture(strUUID!"skystone.bmp"     , BlockTextureRotate.All       )];
                                  
auto textureID(string name)
{
    auto uuid = name.toUUID;
    foreach (i, ref e; blockTextures)
    {
        if (e.texture == uuid)
        {
            return cast(uint)i;
        }
    }
    assert(false, "Failed to find block texture");
}     

shared immutable BlockType[11] blockTypes = [BlockType("Air"       , -1,        0, BlockShape.None),
                                             BlockType("Stone"     , 16,           BlockShape.Cube   , "stone.bmp".textureID),
                                             BlockType("Dirt"      ,  8,           BlockShape.Cube   , "dirt.bmp".textureID),
                                             BlockType("Grass"     ,  8,                               "grass_top.bmp".textureID, "grass_side.bmp".textureID, "dirt.bmp".textureID),
                                             BlockType("Water"     , -1, 0x000, 2, BlockShape.Fluid  , "water.bmp".textureID),
                                             BlockType("Sand"      ,  8,           BlockShape.Cube   , "sand.bmp".textureID),
                                             BlockType("Torch"     ,  1, 0xF80, 0, BlockShape.Torch  , "torch_side.bmp".textureID),
                                             BlockType("Tall Grass",  1,        4, BlockShape.Foliage, "tall_grass.bmp".textureID),
                                             BlockType("Oak Log"   ,  8,                               "oak_top.bmp".textureID, "oak_side.bmp".textureID, "oak_top.bmp".textureID),
                                             BlockType("Oak Leaves",  2, 0,     1, BlockShape.Cube   , "oak_leaves.bmp".textureID),
                                             BlockType("Oak Planks",  4,           BlockShape.Cube   , "oak_planks.bmp".textureID)];
static assert(blockTypes.length < ushort.max, "Number of block types exceeds maximum");

/* Conversion of block types to IDs to use with the block type table */
auto toBlockID(string name)
{
    foreach (i, ref e; blockTypes)
    {
        if (e.name == name)
        {
            return cast(ushort)i;
        }
    }
    assert(false, "Failed to find block type");
}
enum blockID(string name) = name.toBlockID;

/* Block type storing data about the blocks of the world */
struct Block
{
    //Block type ID
    ushort type;
    
    //Light in the block: RRRR GGGG BBBB SSSS (Red, green, blue, sky light)
    ushort light;
    
    //Lower 4 bits are used for orientation, upper 4 bits for fluid block height or damage with other blocks
    ubyte extra;
    
    //Temperature and humidity used in terrain generation to calculate biome
    ubyte temp;
    ubyte humidity;
}

/* Functions for manipulating block light levels */
auto blockRGBS(in uvec4 col) { return cast(ushort)( (col.r << 12) | (col.g << 8) | (col.b << 4) | (col.a) ); }

auto blockRGBSVec(ushort light) { return uvec4(light >> 12, light >> 8, light >> 4, light) & 0xF; }

/* The world is managed using chunks, which are areas of 16*16*16 blocks */
struct Item
{
    bool isBlock;
    ushort id;
    
    auto name() { return isBlock ? blockTypes[id].name : ""; }
}

struct ItemStore
{
    Item item;
    ubyte count;
}

enum ChunkState : ubyte
{
    Sculpted,
    Decorated,
    Final,
    ToLight,
    Invalid,
    NotPresent
}

enum chunkSize = 16;
alias ChunkBlocks = Block[chunkSize][chunkSize][chunkSize];
struct Chunk
{
    uint allocID;
    auto state = ChunkState.Sculpted;
    bool toSave;
    ChunkBlocks blocks;
    
    alias blocks this;
}

auto chunkCoord(t)(in t pos)
{
    //Convert a world space coordinate to an integer chunk coordinate
    ivec3 toReturn;
    foreach (i, ref e; pos)
    {
        toReturn[i] = cast(int)floor((cast(float)e)/chunkSize);
    }
    return toReturn;
}

auto chunkWorldPos(t)(in t chunk)
{
    //Convert an integer chunk coordinate back to world space
    ivec3 toReturn;
    foreach (i, ref e; chunk)
    {
        toReturn[i] = e*chunkSize;
    }
    return toReturn;
}

auto radiusChunks(uint radius) { return cast(size_t)(ceil((4.0/3.0) * PI * (radius^^3))); }
