module common.world.entity;

import common.data.common;
import common.world.chunk;
import dvec.vector;
import std.algorithm,
       std.socket;
       
struct AABBCollision
{
    double toi;
    Axis axisFlag;
    ubyte axisIndex;
    ubyte numAxis;
}

struct AABB
{
    dvec3 origin;
    dvec3 extent;
    
    invariant
    {
        foreach (i; extent)
        {
            assert(i >= 0, "Extent coordinates must be greater than or equal to 0");
        }
    }
    
    final auto opBinary(string op)(in dvec3 velo)
        if (op == "+")
    {
        //Create a shape suitable for broadphase collision testing
        auto broad = this;
        foreach (i; 0..3)
        {
            if (velo[i] > 0)
            {
                broad.extent[i] += velo[i];
            }
            else
            {
                broad.origin[i] += velo[i];
                broad.extent[i] -= velo[i];
            }
        }
        return broad;
    }
    
    final auto collides(in AABB other)
    {
        foreach (i; 0..3)
        {
            if (origin[i]+extent[i] < other.origin[i] || origin[i] > other.origin[i]+other.extent[i])
            {
                return false;
            }
        }
        return true;
    }
    
    final auto sweptNoBroadphase(in dvec3 velo, in AABB other)
    {
        //Find the proportion of each velocity component needed for a collision entry and exit to occur
        double[3] entryDist;
        double[3] exitDist;
        foreach (i; 0..3)
        {
            if (velo[i] > 0)
            {
                entryDist[i] = other.origin[i] - (origin[i]+extent[i]);
                exitDist[i]  = (other.origin[i]+other.extent[i]) - origin[i];
            }
            else if (velo[i] == 0)
            {
                entryDist[i] = -double.infinity;
                exitDist[i]  = +double.infinity;
                continue;
            }
            else
            {
                entryDist[i] = (other.origin[i]+other.extent[i]) - origin[i];
                exitDist[i]  = other.origin[i] - (origin[i]+extent[i]);
            }
            entryDist[i] /= velo[i];
            exitDist[i]  /= velo[i];
        }

        //A collision will occur if there is at least one entry distance greater than 0, all are less than 1, and the overall entry time is less than the exit time
        auto maxEntry = entryDist.reduce!max;
        auto collisionAxis = ubyte.max;
        ubyte numAxis;
        
        if (maxEntry < 1 && maxEntry < exitDist.reduce!min)
        {
            foreach (ubyte i; 0..3)
            {
                if (entryDist[i] >= 0)
                {
                    if (collisionAxis > 2 || entryDist[i] < entryDist[collisionAxis])
                    {
                        collisionAxis = i;
                        numAxis = 1;
                    }
                    else if (entryDist[i] == entryDist[collisionAxis])
                    {
                        numAxis += 1;
                    }
                }
            }
            
            if (collisionAxis < 3)
            {
                return AABBCollision(entryDist[collisionAxis], collisionAxis.toAxisFlag(velo[collisionAxis] < 0), collisionAxis, numAxis);
            }
        }
        return AABBCollision(double.infinity);
    }
    
    final auto swept(in dvec3 velo, in AABB other)
    {
        auto broad = this + velo;
        if (!broad.collides(other))
        {
            return AABBCollision(double.infinity);
        }
        else
        {
            return sweptNoBroadphase(velo, other);
        }
    }
}

enum EntityFlags : ubyte
{
    None          = 0x00,
    TouchedGround = 0x01,
    TouchedSide   = 0x02,
    InFluid       = 0x04,
    Running       = 0x08,
    Flying        = 0x10
}

struct EntityState
{
    dvec3 origin;
    dvec3 velo;
    float direction;
    EntityFlags state;
}

class Entity
{
    dvec3 origin;
    auto velo = dvec3(0,0,0);
    float direction = 0.0;
    EntityFlags state;
    
    abstract ref immutable(dvec3) extent() const;
    abstract UUID model();
    final auto pos() { return origin + (extent/2); }
    final auto pos(in dvec3 newPos) { origin = newPos - (extent/2); }
    final auto aabb() { return AABB(origin, extent); }
    
    alias aabb this;
    
    void update(double dt)
    {
        if (!(state & EntityFlags.Flying))
        {
            velo.y -= 12*dt;
        }
    }
    
    this() {}
    
    this(in EntityState state)
    {
        origin = state.origin;
        velo = state.velo;
        direction = state.direction;
        this.state = state.state;
    }
}

class BlockEntity : Entity
{
    private static immutable _extent = dvec3(1,1,1);
    override ref immutable(dvec3) extent() const { return _extent; }
    
    this(in dvec3 origin) { this.origin = origin; }
}
