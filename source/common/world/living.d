module common.world.living;

import common.data.common;
import common.world.chunk,
       common.world.entity;
import dvec.matrix,
       dvec.vector;
import std.math,
       std.random;

/* Living entity type used to represent an entity with a head orientation */
struct LivingState
{
    EntityState entity;
    vec2 headOrient;
    ushort hp;
}

class LivingEntity : Entity
{
    vec2 headOrient;
    ushort hp;
    
    enum walkSpeed = 3;
    enum runSpeed  = 5;
    enum jumpSpeed = 6;
    enum swimSpeed = 14;
    
    this() {}
    
    this(in LivingState state)
    {
        super(state.entity);
        headOrient = state.headOrient;
        hp = state.hp;
    }
}

/* Player type representing a player with an inventory */
enum PlayerMode : ubyte
{
    Sandbox,
    Survival
}

struct PlayerServerState
{
    LivingState living;
    ItemStore onHand;
}

class Player : LivingEntity
{
    private:
        public enum maxUsernameLen = 16;
        char[maxUsernameLen] _username;
        ubyte _usernameLen;
        
        __gshared static immutable _extent = dvec3(0.8, 1.8, 0.8);
        
    public:
        final auto username() { return _username[0.._usernameLen]; }
        
        __gshared static immutable headPos = dvec3(_extent.x/2, _extent.y * 0.75, _extent.z/2);
        override ref immutable(dvec3) extent() const { return _extent; }
        override UUID model() { return strUUID!"player.mobj"; }
        
        this() {}
        
        this(const(char)[] username, in dvec3 origin)
            in (username.length <= maxUsernameLen)
        {
            super();
            _username[0..username.length] = username[];
            _usernameLen = cast(ubyte)username.length;
            this.origin = origin;
            this.velo = dvec3(0,0,0);
            this.headOrient = vec2(0,0);
        }
        
        this(const(char)[] username, in LivingState state)
        {
            _username[0..username.length] = username[];
            _usernameLen = cast(ubyte)username.length;
            super(state);
        }
        
        this(const(char)[] username, in PlayerServerState state)
        {
            _username[0..username.length] = username[];
            _usernameLen = cast(ubyte)username.length;
            super(state.living);
        }
}

struct PlayerSaveState
{
    LivingState living;
    PlayerMode mode;
    ItemStore[9*3] inventory;
    ItemStore[9] onHand;
}

class LocalPlayer : Player
{
    public:
        PlayerMode mode;
        ItemStore[9*3] inventory;
        ItemStore[9] onHand;
        ulong timeLastForward;
        ulong timeLastStatic;
        ubyte onHandSelect;
        
        final auto saveState()
        {
            PlayerSaveState state;
            state.living.entity.origin = origin;
            state.living.entity.velo = velo;
            state.living.headOrient = headOrient;
            return state;
        }
        
        this(const(char)[] username, in dvec3 origin) { super(username, origin); }
        
        this(const(char)[] username, in PlayerSaveState state)
        {
            super(username, state.living);
            mode = state.mode;
            inventory = state.inventory;
            onHand = state.onHand;
        }
}

/* Animal types */
enum AnimalBehaviour : ubyte
{
    Still,
    Walking
}

class Animal : LivingEntity
{
    private:
        AnimalBehaviour behaviour;
        double stateTimer = 0.0;
        
        __gshared static immutable _extent = dvec3(0.8, 0.8, 0.8);
    
    public:
        override ref immutable(dvec3) extent() const { return _extent; }
    
        override void update(double dt)
        {
            super.update(dt);
        
            //Basic animal behaviour: alternate between walking and staying still for a few seconds
            stateTimer -= dt;
            if (stateTimer <= 0)
            {
                if (behaviour == AnimalBehaviour.Still)
                {
                    //Choose a direction to start walking in
                    behaviour = AnimalBehaviour.Walking;
                    direction = uniform01 * PI * 2;
                }
                else
                {
                    //Stay still
                    behaviour = AnimalBehaviour.Still;
                    velo = dvec3(0,0,0);
                }
                
                //Stay in this new state for a generated time period
                stateTimer = 3 + uniform01*2;
            }
            
            if (behaviour == AnimalBehaviour.Walking)
            {
                auto xzVelo = roll!mat2(direction) * vec2(0, walkSpeed);
                velo.x = xzVelo.x;
                velo.z = xzVelo.y;
            }
            
            //Jump on reaching side blocks or swim to the surface
            if ((state & EntityFlags.TouchedSide) && (state & EntityFlags.TouchedGround))
            {
                velo.y = LivingEntity.jumpSpeed;
            }
            else if (state & EntityFlags.InFluid)
            {
                velo.y += LivingEntity.swimSpeed*dt;
            }
        }
        
        this() {}
}

class Pig : Animal
{
    override UUID model() { return strUUID!"pig.mobj"; }

    this(in dvec3 origin) { this.origin = origin; }
}
