module common.world.manager;

import common.pool,
       common.queue;
import common.world.chunk,
       common.world.entity,
       common.world.living;
import dvec.matrix,
       dvec.noise,
       dvec.vector;
import std.algorithm,
       std.math,
       std.typecons;

/* World listener type. Objects can be registered as listeners to a world manager to get notified of events */
abstract class WorldListener
{
    private:
        WorldListener next;
        
    public:
        void blockUpdate(in ivec3 pos, uint oldType, uint newType);
        void lightUpdate(in ivec3 min, in ivec3 max);
}

/* Types used in entity management. Entity objects are stored as part of a linked list */
private class EntityNode
{
    EntityNode nextGlobal;
    EntityNode prevGlobal;
    EntityNode nextInChunk;
    
    abstract Entity entity();
    alias entity this;
}
       
private class EntityInstance(t) : EntityNode
    if (is(t : Entity))
{
    typeof(scoped!t(dvec3())) instance;
    override Entity entity() { return instance; }
    
    this(a...)(in a args) { instance = scoped!t(args); }
}

private struct EntityRange
{
    private:
        EntityNode _next;
        
        this(EntityNode first) { _next = first; }
        
    public:
        auto empty() { return _next is null; }
        auto front() { return _next.entity; }
        void popFront()
        {
            if (_next)
            {
                _next = _next.nextGlobal;
            }
        }
}
       
/* Abstract world manager class, used to represent a world management system. This may be a world server or a client to a world server */
abstract class WorldManager
{
    protected:
        Queue!ivec3 lightUpdates;
        
        ulong _dt;
        ulong _time;
        ulong _lastBlockOp;

        Chunk*[ivec3] _chunkLookup;
        WorldListener _observers;
        EntityNode _entityList;
        public ivec3 _selectBlock;
        BlockSide _selectSide;
        
        immutable int finalDistance;
        immutable int finalDistanceSq;
        
        //Move an entity by some velocity
        final auto move(Entity entity, in dvec3 velo)
        {   
            auto entityAABB = entity.aabb;
        
            //Create a broadphase AABB rounded to include all blocks
            auto broad = entityAABB + velo;
            foreach (i; 0..3)
            {
                with (broad)
                {
                    auto originDiff = origin[i] - floor(origin[i]);
                    origin[i] -= originDiff;
                    extent[i] = ceil(extent[i] + originDiff);
                }
            }
            
            //Test each possible collidable block and find the one with the earliest TOI
            auto earliest = AABBCollision(double.infinity);
            ivec3 earliestBlock;

            foreach (x; broad.origin.x .. broad.origin.x+broad.extent.x)
            {
                foreach (y; broad.origin.y .. broad.origin.y+broad.extent.y)
                {
                    foreach (z; broad.origin.z .. broad.origin.z+broad.extent.z)
                    {
                        auto pos = ivec3(cast(int)x,cast(int)y,cast(int)z);
                        auto chunkBlock = readBlockPtr(pos);
                        if (!chunkBlock)
                        {
                            //If a block couldn't be obtained the entity is in an unloaded chunk and should not be allowed to move
                            entity.velo = dvec3(0,0,0);
                            return false;
                        }
                        else if (blockTypes[chunkBlock.type].shape == BlockShape.Cube)
                        {
                            AABB block;
                            block.origin = dvec3(x,y,z);
                            block.extent = dvec3(1,1,1);
                            
                            auto collision = entityAABB.sweptNoBroadphase(velo, block);
                            if (collision.toi < earliest.toi || (collision.toi == earliest.toi && collision.numAxis < earliest.numAxis))
                            {
                                //Save this collision if it is the earliest, or the same as the current earliest but with fewer axes of collision
                                earliest = collision;
                                earliestBlock = pos;
                            }
                        }
                    }   
                }
            }

            //Make the entity slide against the block collided
            if (earliest.toi != double.infinity)
            {
                //Move the entity to the time of impact and round its coordinates to the block it collided with
                entity.origin += velo * earliest.toi;
                
                FloatingPointControl fpctrl;
                fpctrl.rounding(FloatingPointControl.roundDown);
                entity.origin[earliest.axisIndex] = velo[earliest.axisIndex] < 0 ? (earliestBlock[earliest.axisIndex] + 1)
                                                                                 : (earliestBlock[earliest.axisIndex] - entity.extent[earliest.axisIndex]);
                
                //Prepare the next move and repeat
                dvec3 nextMove = velo * (1-earliest.toi);
                nextMove[earliest.axisIndex] = 0;
                entity.velo[earliest.axisIndex] = 0;
                if (nextMove.magSquared != 0)
                {
                    return move(entity, nextMove) | earliest.axisFlag;
                }
            }
            else
            {
                entity.origin += velo;
            }
            return earliest.axisFlag;
        }
        
        //Cast a sky light at a given block, which may involve removing light underneath it
        final void castSkyLight(in ivec3 block)
        {
            ivec3 nextPos = block;
            while (true)
            {
                nextPos.y += 1;
                auto next = readBlockPtr(nextPos);

                if (!next || (next.light & 0xF) > 0)
                {
                    //Cast sky light downwards. If no block that blocks skylight was found then use the maximum brightness possible
                    ubyte brightness = next ? (next.light & 0xF) : 0xF;
                    while (true)
                    {
                        nextPos.y -= 1;
                        auto prev = getBlockPtr(nextPos);
                        if (!prev)
                        {
                            //If this block does not exist then stop casting
                            return;
                        }
                        else
                        {                            
                            //If this block does exist then give it light and consider its filtering in further blocks
                            brightness -= min(brightness, blockTypes[prev.type].light >>> 12);
                            prev.light = (prev.light & 0xFFF0) | brightness;
                        }
                    }
                }
            }
        }
        
        final void processLightUpdates()
        {
            //Keep track of the minimum and maximum positions to broadcast to listeners
            if (lightUpdates.empty)
            {
                return;
            }
            ivec3 minPos = lightUpdates.front;
            ivec3 maxPos = minPos;
            
            while (!lightUpdates.empty)
            {
                auto toUpdate = lightUpdates.front;
                lightUpdates.popFront;
            
                //Ignore solid and unloaded blocks
                auto block = getBlockPtr(toUpdate);
                if (!block)
                {
                    continue;
                }
                
                auto attenuation = (blockTypes[block.type].light >>> 12) + 1;
                if (attenuation == 0x10 && block.light == 0)
                {
                    continue;
                }
            
                //Find the brightest adjacent block lights
                uvec4 toSet;
                foreach (axis; 0..3)
                {
                    foreach (side; 0..2)
                    {
                        //Find the coordinate of the current block to check
                        auto adjPos = toUpdate;
                        adjPos[axis] += side ? +1 : -1;
                        
                        //Find the maximum for each component light with adjacent blocks that are loaded
                        auto adj = getBlockPtr(adjPos);
                        if (adj)
                        {
                            foreach (i,e; adj.light.blockRGBSVec)
                            {
                                //Sky light has no downward vertical attenuation and is only affected by block filtering
                                toSet[i] = max(toSet[i], e - min(e, attenuation - (i == 3 && axis == 1 && side)));
                            }
                        }
                    }
                }
                
                //Special case: if the block above if not obtainable then skylight should be unaffected
                if (!readBlockPtr(toUpdate + ivec3(0,1,0)))
                {
                    toSet.a = block.light & 0xF;
                }
                
                //Get the light cast by the type of the block
                foreach (i,e; (cast(ushort)(blockTypes[block.type].light << 4)).blockRGBSVec)
                {
                    toSet[i] = max(toSet[i], e);
                }
                auto toAssign = toSet.blockRGBS;

                //If the current light on the block to update is different then propogate further
                if (block.light != toAssign)
                {
                    block.light = toAssign;
                    foreach (axis; 0..3)
                    {
                        foreach (side; 0..2)
                        {
                            auto adjPos = toUpdate;
                            adjPos[axis] += side ? +1 : -1;
                            lightUpdates.put(adjPos);
                        }
                    }
                    
                    //Update the minimum and maximum positions
                    foreach (i,e; toUpdate)
                    {
                        if (e < minPos[i])
                        {
                            minPos[i] = e;
                        }
                        else if (e > maxPos[i])
                        {
                            maxPos[i] = e;
                        }
                    }
                }
            }

            //Notify observers of updates
            for (auto observer = _observers; observer; observer = observer.next)
            {
                observer.lightUpdate(minPos, maxPos);
            }
        }

        final void lightUpdate(in ivec3 pos)
        {
            lightUpdates.put(pos);
            processLightUpdates;
        }

        //Update an entity
        final void updateEntity(Entity toUpdate, ulong nsDT)
        {
            auto dtf = nsDT/1e9;
            toUpdate.update(dtf);
            
            //Check if the player is in a fluid block
            auto aabb = toUpdate.aabb;
            foreach (e; 0..3)
            {
                with (aabb)
                {
                    auto originDiff = origin[e] - floor(origin[e]);
                    origin[e] -= originDiff;
                    extent[e] = ceil(extent[e] + originDiff);
                }
            }
            
            toUpdate.state &= ~EntityFlags.InFluid;
            outer: foreach (x; aabb.origin.x .. aabb.origin.x+aabb.extent.x)
            {
                foreach (y; aabb.origin.y .. aabb.origin.y+aabb.extent.y)
                {
                    foreach (z; aabb.origin.z .. aabb.origin.z+aabb.extent.z)
                    {
                        auto chunkBlock = getBlockPtr(ivec3(cast(int)x,cast(int)y,cast(int)z));
                        if (chunkBlock && blockTypes[chunkBlock.type].shape == BlockShape.Fluid)
                        {
                            //Create a drag force against the entity velocity
                            toUpdate.velo -= toUpdate.velo*0.6*dtf;
                            toUpdate.state |= EntityFlags.InFluid;
                            break outer;
                        }
                    }
                }
            }
            
            //Update the player velocity and move the player
            auto touched = move(toUpdate, toUpdate.velo*dtf);
            if (touched & Axis.MinusY)
            {
                toUpdate.state |= EntityFlags.TouchedGround;
            }
            else
            {
                toUpdate.state &= ~EntityFlags.TouchedGround;
            }

            if (touched & (Axis.MinusX | Axis.PlusX | Axis.MinusZ | Axis.PlusZ))
            {
                toUpdate.state |= EntityFlags.TouchedSide;
            }
            else
            {
                toUpdate.state &= ~EntityFlags.TouchedSide;
            }
        }
        
        //Add entities to the global linked list
        final void integrateEntity(EntityNode toAdd)
        {
            if (_entityList)
            {
                //If there are existing mobs in the global linked list, then update the links
                toAdd.nextGlobal = _entityList;
                _entityList.prevGlobal = toAdd;
            }
            _entityList = toAdd;
        }
        
        final void createEntity(t)(in dvec3 pos)
            if (is(t : Entity))
        {
            //Create a new entity and add it to the global linked list
            auto toCreate = new EntityInstance!t(pos);
            integrateEntity(toCreate);
        }
        
    public:
        //Read-only block getter functions
        final const readBlockPtr(in ivec3 blockPos)
        {
            auto pos = blockPos.chunkCoord;
            auto rel = blockPos - pos.chunkWorldPos;
            auto chunk = _chunkLookup.get(pos, null);
            return chunk ? &((*chunk)[rel.x][rel.y][rel.z]) : null;
        }

        final ref const readBlock(in ivec3 blockPos) { return *readBlockPtr(blockPos); }
        
        //Read-write block getter functions that cause affected chunks to be scheduled for saving
        final auto getBlockPtr(in ivec3 blockPos)
        {
            auto pos = blockPos.chunkCoord;
            auto rel = blockPos - pos.chunkWorldPos;
            
            auto chunk = _chunkLookup.get(pos, null);
            if (chunk)
            {
                chunk.toSave = true;
                return &((*chunk)[rel.x][rel.y][rel.z]);
            }
            else
            {
                return null;
            }
        }

        final ref auto getBlock(in ivec3 blockPos) { return *getBlockPtr(blockPos); }
    
        //Input range for iterating over global entity linked list
        final auto entities() { return EntityRange(_entityList); }
        
        //Register a world listener
        final void register(WorldListener toAdd)
        {
            toAdd.next = _observers;
            _observers = toAdd;
        }
    
        //Check if a chunk is surrounded
        final auto isSurrounded(in ivec3 pos)
        {
            foreach (x; -1..2)
            {
                foreach (y; -1..2)
                {
                    foreach (z; -1..2)
                    {
                        if ((ivec3(x,y,z) + pos) !in _chunkLookup)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        
        //Retrieve the chunk at the specified position
        final ref const getChunk(in ivec3 pos) { return *_chunkLookup[pos]; }
        
        //Select a block
        final void selectBlock(uint id = 0)
        {
            auto player = player(id);
            auto pos = player.origin + Player.headPos;

            auto dir = yaw!dmat3(player.headOrient.yaw) * pitch!dmat3(player.headOrient.pitch) * dvec3(0,0,1);
            while (true)
            {
                //Find the axis where the relative distance towards the next block face is the least
                uint minAxis;
                auto minAxisDist = double.infinity;
                foreach (i; 0..3)
                {
                    double thisAxisDist;
                    if (dir[i] > 0)
                    {
                        thisAxisDist = ceil(pos[i]) - pos[i];
                        if (pos[i] % 1 == 0)
                        {
                            thisAxisDist += 1;
                        }
                    }
                    else
                    {
                        thisAxisDist = floor(pos[i]) - pos[i];
                        if (pos[i] % 1 == 0)
                        {
                            thisAxisDist += -1;
                        }
                    }
                    thisAxisDist /= dir[i];
                    
                    if (thisAxisDist < minAxisDist)
                    {
                        minAxis = i;
                        minAxisDist = thisAxisDist;
                    }
                }
                
                //Move the position being tested to the nearest block face along the direction being looked at
                pos += dir * minAxisDist;
                if ((pos-player.pos).magSquared > 5^^2)
                {
                    _selectBlock = ivec3(int.max, int.max, int.max);
                    return;
                }
                else
                {
                    //Find the block to obtain
                    ivec3 blockPos;
                    foreach (i,e; pos)
                    {
                        blockPos[i] = cast(int)(floor(e));
                    }
                    
                    if (dir[minAxis] < 0)
                    {
                        blockPos[minAxis] -= 1;
                    }
                    
                    //Select it if possible
                    auto block = readBlockPtr(blockPos);
                    if (block && block.type != blockID!"Air" && blockTypes[block.type].shape != BlockShape.Fluid)
                    {
                        _selectBlock = blockPos;
                        if (blockTypes[block.type].shape != BlockShape.Cube)
                        {
                            _selectSide = BlockSide.Inside;
                        }
                        else
                        {
                            final switch (minAxis) with (BlockSide)
                            {
                                case 0: _selectSide = dir[minAxis] < 0 ? MinusX : PlusX; break;
                                case 1: _selectSide = dir[minAxis] < 0 ? MinusY : PlusY; break;
                                case 2: _selectSide = dir[minAxis] < 0 ? MinusZ : PlusZ; break;
                            }
                        }
                        return;
                    }
                }
            }
        }
        
        //Break and replace the selected block
        void breakSelect()
        {
            //For a valid block selection
            auto runtime = this.runtime;
            if ((runtime - _lastBlockOp) > 3*((10L^^8)/2) && _selectBlock != ivec3(int.max, int.max, int.max))
            {
                //Break the block and the block above if it is a foliage block
                auto block = getBlockPtr(_selectBlock);
                auto oldType = block.type;
                block.type = blockID!"Air";
                auto aboveBlock = getBlockPtr(_selectBlock + ivec3(0,1,0));

                if (aboveBlock && blockTypes[aboveBlock.type].shape == BlockShape.Foliage)
                {
                    aboveBlock.type = blockID!"Air";
                }
                lightUpdate(_selectBlock);
                
                //Notify observers of the block update
                _lastBlockOp = runtime;
                for (auto observer = _observers; observer; observer = observer.next)
                {
                    observer.blockUpdate(_selectBlock, oldType, blockID!"Air");
                }
            }
        }
        
        void placeBlock()
        {
            //For a valid block selection
            auto runtime = this.runtime;
            if ((runtime - _lastBlockOp) > 3*((10L^^8)/2) && _selectBlock != ivec3(int.max, int.max, int.max))
            {
                //Decide which adjacent block to replace
                ivec3 toReplace = _selectBlock;
                final switch (_selectSide) with (BlockSide)
                {
                    case MinusX: toReplace.x += 1; break;
                    case PlusX : toReplace.x -= 1; break;
                    case MinusY: toReplace.y += 1; break;
                    case PlusY : toReplace.y -= 1; break;
                    case MinusZ: toReplace.z += 1; break;
                    case PlusZ : toReplace.z -= 1; break;
                    case Inside: break;
                }
                
                //Do not place the block if there is an entity in the way
                auto aabb = AABB(dvec3(toReplace), dvec3(1,1,1));
                foreach (i; entities)
                {
                    if (aabb.collides(i))
                    {
                        return;
                    }
                }
                
                //Update the block to replace
                auto block = &getBlock(toReplace);
                auto oldType = block.type;
                block.type = player.onHand[player.onHandSelect].item.id;
                lightUpdate(toReplace);

                //Notify observers of the block update
                _lastBlockOp = runtime;
                for (auto observer = _observers; observer; observer = observer.next)
                {
                    observer.blockUpdate(toReplace, oldType, block.type);
                }
            }
        }
        
        //Get data about the world
        LocalPlayer player(uint id = 0);
        final ulong time() { return _time; }
        ulong runtime();
        
        //Update the world
        void onHandSelect(ubyte slot, uint id = 0);
        void message(string msg);
        void update(ulong nsDT);
        
        //Update the movement of a player
        void movePlayer(in dvec3 movement, in vec2 headRot, bool running, bool toggleFlying, uint playerID = 0)
        {
            with (player(playerID))
            {
                //Update the player run state if they are on the ground or in fluid
                if ((state & (EntityFlags.TouchedGround | EntityFlags.InFluid | EntityFlags.Flying)) && running)
                {
                    state |= EntityFlags.Running;
                }
                else if (!running)
                {
                    state &= ~EntityFlags.Running;
                }
                
                //Toggle flying for the player
                if (toggleFlying)
                {
                    if (state & EntityFlags.Flying)
                    {
                        state &= ~EntityFlags.Flying;
                    }
                    else
                    {
                        state |= EntityFlags.Flying;
                    }
                }
            
                //If the passed movement direction vector is actually a zero vector then do not move the player
                if (movement.magSquared)
                {
                    //Update the player velocity
                    auto moveUnit = movement.magSquared > 1 ? movement.unit : movement;
                    
                    if (state & EntityFlags.Flying)
                    {
                        if (moveUnit.y > 0)
                        {
                            velo.y = LivingEntity.jumpSpeed;
                        }
                        else if (moveUnit.y < 0)
                        {
                            velo.y = -LivingEntity.jumpSpeed;
                        }
                        else
                        {
                            velo.y = 0;
                        }
                    }
                    else if (moveUnit.y > 0)
                    {
                        //Make the player jump or swim
                        timeLastForward = time;
                        if (state & EntityFlags.InFluid)
                        {
                            velo.y += LivingEntity.swimSpeed*(_dt/1e9);
                        }
                        else if (state & EntityFlags.TouchedGround)
                        {
                            velo.y = LivingEntity.jumpSpeed;
                        }
                    }
                    
                    //Decide on the walking/running speed and then update the player velocity
                    float speed = (state & EntityFlags.Running) ? LivingEntity.runSpeed : LivingEntity.walkSpeed;
                    if (state & EntityFlags.Flying)
                    {
                        speed *= 2;
                    }
                    
                    auto worldMove = roll!mat2(headOrient.yaw) * vec2(moveUnit.x, moveUnit.z) * speed;
                    velo.x = worldMove.x;
                    velo.z = worldMove.y;
                }
                else
                {
                    //The player is not moving
                    velo.x = 0;
                    if (state & EntityFlags.Flying)
                    {
                        velo.y = 0;
                    }
                    velo.z = 0;
                    timeLastStatic = time;
                }
                
                headOrient += headRot;
                headOrient.pitch = headOrient.pitch.clamp(-PI/2, PI/2);
            }
        }
        
        this(uint finalDistance)
        {
            lightUpdates = Queue!ivec3(16384);
            this.finalDistance = finalDistance;
            finalDistanceSq = finalDistance^^2;
        }
}                 
