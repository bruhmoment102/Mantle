module common.world.save;

import common.data.common,
       common.data.mworld;
import common.world.chunk,
       common.world.living;
import core.memory;
import dvec.vector;
import etc.c.zlib;
import std.conv,
       std.exception,
       std.file,
       std.format,
       std.random,
       std.stdio;
import std.experimental.logger;

/* World saved to a file */
struct Save
{
    private:
        /* Data streams and buffer used in chunk compression/decompression */
        z_stream _inflation;
        z_stream _deflation;
        ubyte* _zlibBuf;
        
        File _file;
        public ulong runtime;
        
        void createFileRegion(in ivec3 pos)
        {
            //Create a new region data structure in the savefile using the region coordinate passed
            _file.seek(0, SEEK_END);
            
            MWorldRegionHeader header;
            header.region = pos;
            rawWrite(_file, header);
            foreach (i; 0..mworldRegionSize^^3)
            {
                rawWrite(_file, 0UL);
            }
            _file.seek(-(mworldRegionSize^^3)*ulong.sizeof, SEEK_CUR);
        }
        
        auto seekRegion(in ivec3 pos)
        {
            _file.seek(MWorldHeader.sizeof);
            if (_file.eof)
            {
                return false;
            }
            
            while (true)
            {
                //Iterate through the linked list elements storing data about chunk regions
                MWorldRegionHeader header;
                rawRead(_file, header);
                if (header.region == pos)
                {
                    return true;
                }
                else
                {
                    //If this region data structure references no other structure then no chunk data will be loaded
                    if (header.next == 0)
                    {
                        return false;
                    }
                    _file.seek(header.next);
                }
            }
        }
        
        auto loadFile(string name)
        {
            //Create a save directory if one does not exist
            if (!"saves".exists)
            {
                mkdir("saves");
            }
            
            //Open a world file
            auto file = format!"saves/%s.mworld"(name);
            if (file.exists)
            {
                //Load an existing save using the given world name
                _file = File(file, "r+b");
                MWorldHeader header;
                rawRead(_file, header);
                
                enforce(header.valid, format!"Invalid header for world file %s"(file));
                runtime = header.runtime;
                logf(LogLevel.info, "Loaded world \"%s\"", name);
                return header.seed;
            }
            else
            {
                //Create a brand new world save
                _file = File(file, "w+b");
                runtime = 60*8*(10L^^9);
                auto primarySeed = uniform(0, uint.max);
                
                MWorldHeader header;
                with (header)
                {
                    magic   = MWorldHeader.validMagic;
                    seed    = primarySeed;
                    player  = 0;
                    runtime = this.runtime;
                }
                
                rawWrite(_file, header);
                logf(LogLevel.info, "Created new world \"%s\"", name);
                
                //Create the first region
                createFileRegion(ivec3(0,0,0));
                
                return primarySeed;
            }
        }
        
        void closeFile()
        {
            logf(LogLevel.info, "Closing world");
        
            //Modify and save the MWorld file header
            _file.seek(0);
            MWorldHeader header;
            rawRead(_file, header);
            header.runtime = runtime;
            
            _file.seek(0);
            rawWrite(_file, header);
        }
        
    public:
        immutable uint primarySeed;
        
        auto isOpen() { return _file.isOpen; }
        
        auto compress(in Chunk target)
        {
            deflateReset(&_deflation);
            with (_deflation)
            {
                next_in   = cast(ubyte*)&target.blocks;
                avail_in  = cast(uint)ChunkBlocks.sizeof;
                next_out  = _zlibBuf;
                avail_out = avail_in;
            }
            auto ret = deflate(&_deflation, Z_FINISH);
            enforce(ret == Z_STREAM_END, format!"Failed to compress chunk data: deflate returned %s"(ret));
            return _zlibBuf[0..ChunkBlocks.sizeof-_deflation.avail_out];
        }
        
        auto savePlayer(const(char)[] username, in PlayerSaveState state)
            in (username.length <= Player.maxUsernameLen)
        {
            auto uuid = username.toUUID;
        
            //Read the file header to find an offset to the first player structure
            _file.seek(0);
            MWorldHeader header;
            rawRead(_file, header);
            
            MWorldPlayer player;
            if (!header.firstPlayer)
            {
                //If no players exist in the world save file then create a new one at the end of the file
                header.firstPlayer = _file.size;
                _file.seek(0);
                rawWrite(_file, header);
            }
            else
            {
                //Try to find an existing player with a matching username
                uint i;
                for (auto next = header.firstPlayer; next; next = player.nextPlayer)
                {
                    _file.seek(next);
                    rawRead(_file, player);
                    if (player.nameUUID == uuid)
                    {
                        rawWrite(_file, state);
                        return;
                    }
                }
                
                //If one does not exist then create a new one
                _file.seek(-MWorldPlayer.sizeof, SEEK_CUR);
                player.nextPlayer = _file.size;
                rawWrite(_file, player);
            }
            
            //If no matching player exists then create a new one
            _file.seek(0, SEEK_END);
            with (player)
            {
                nextPlayer = 0;
                nameUUID = uuid;
                name[0..username.length] = username[];
                usernameLen = cast(ubyte)username.length;
            }
            rawWrite(_file, player);
            rawWrite(_file, state);
        }
        
        auto getPlayer(const(char)[] username)
            in (username.length <= Player.maxUsernameLen)
        {
            auto uuid = username.toUUID;
            _file.seek(0);
            MWorldHeader header;
            rawRead(_file, header);
            
            //Search for a player with the given username
            MWorldPlayer player;
            for (auto next = header.firstPlayer; next; next = player.nextPlayer)
            {
                _file.seek(next);
                rawRead(_file, player);
                if (player.nameUUID == uuid)
                {
                    PlayerSaveState state;
                    rawRead(_file, state);
                    return state;
                }
            }
            
            //Return a vector with components of NaN if no player was found
            return PlayerSaveState();
        }
        
        auto loadChunk(in ivec3 pos, ref Chunk target)
        {
            auto region = pos.regionCoord;
            if (!seekRegion(region))
            {
                return ChunkState.NotPresent;
            }

            //If this region corresponds to the one that the requested chunk is in, then try to find the chunk data offset
            auto regionPos = pos - region.regionChunkPos;
            _file.seek(ulong.sizeof*((mworldRegionSize^^2)*regionPos.z + mworldRegionSize*regionPos.y + regionPos.x), SEEK_CUR);
            ulong offset;
            rawRead(_file, offset);
            
            //If this chunk is not present then return false
            if (offset == 0)
            {
                return ChunkState.NotPresent;
            }
            _file.seek(offset);
            
            //Otherwise load the chunk data and return true
            try
            {
                MWorldChunkHeader header;
                rawRead(_file, header);
                target.state = header.state;
                
                _file.rawRead(_zlibBuf[0..header.size]);
                inflateReset(&_inflation);
                with (_inflation)
                {
                    next_in   = _zlibBuf;
                    avail_in  = cast(uint)header.size;
                    next_out  = cast(ubyte*)&target.blocks;
                    avail_out = cast(uint)ChunkBlocks.sizeof;
                }
                auto ret = inflate(&_inflation, Z_NO_FLUSH);
                enforce(ret == Z_STREAM_END, format!"Failed to decompress chunk data: inflate returned %s"(ret));
                
                return header.state;
            }
            catch (Exception exception)
            {
                //If an exception occurs when loading chunk data then the chunk is invalid
                return ChunkState.Invalid;
            }
        }
        
        void saveChunk(in ivec3 pos, in Chunk chunk)
        {
            auto region = pos.regionCoord;
            if (!seekRegion(region))
            {
                //Create a new region
                _file.seek(-MWorldRegionHeader.sizeof, SEEK_CUR);
                rawWrite(_file, cast(ulong)_file.size);
                createFileRegion(region);
            }
            
            //Find the chunk data offset
            auto regionPos = pos - region.regionChunkPos;
            _file.seek(ulong.sizeof*((mworldRegionSize^^2)*regionPos.z + mworldRegionSize*regionPos.y + regionPos.x), SEEK_CUR);
            ulong offset;
            auto regionTableOffset = _file.tell;
            rawRead(_file, offset);

            //Seek to the chunk data, creating new data if not already present
            if (offset == 0)
            {
                _file.seek(-8, SEEK_CUR);
                rawWrite(_file, cast(ulong)_file.size);
                _file.seek(0, SEEK_END);
            }
            else
            {
                _file.seek(offset);
            }
            
            //Save the chunk data to the file
            MWorldChunkHeader header;
            if (offset != 0)
            {
                rawRead(_file, header);
            }
            auto oldSize = header.size;
            auto compressed = compress(chunk);
            with (header)
            {
                size  = cast(uint)compressed.length;
                state = chunk.state;
            }
            
            if (offset != 0)
            {
                if (oldSize < header.size)
                {
                    //If the existing chunk data is too short then abandon it
                    _file.seek(regionTableOffset);
                    rawWrite(_file, cast(ulong)_file.size);
                    _file.seek(0, SEEK_END);
                }
                else
                {
                    _file.seek(-typeof(header).sizeof, SEEK_CUR);
                }
            }
            rawWrite(_file, header);
            _file.rawWrite(compressed);
        }
        
        this(string name)
        {
            primarySeed = loadFile(name);
            
            //Prepare zlib streams for use in compressing and decompressing chunk data
            enforce(inflateInit(&_inflation) == Z_OK, format!"Failed to initialise zlib stream for chunk decompression: %s"(_inflation.msg.to!string));
            enforce(deflateInit(&_deflation, 9) == Z_OK, format!"Failed to initialise zlib stream for chunk compression: %s"(_deflation.msg.to!string));
            _zlibBuf = new ubyte[ChunkBlocks.sizeof].ptr;
        }
        
        ~this()
        {
            if (isOpen)
            {
                closeFile;
            }
            if (!_inflation.msg)
            {
                inflateEnd(&_inflation);
            }
            if (!_deflation.msg)
            {
                deflateEnd(&_deflation);
            }
            GC.free(_zlibBuf);
        }
}
