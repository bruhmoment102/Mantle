module common.world.server;

import common.data.mserv;
import common.poll,
       common.pool,
       common.queue;
import common.world.chunk,
       common.world.entity,
       common.world.manager,
       common.world.living,
       common.world.save;
import core.atomic,
       core.memory,
       core.thread,
       core.time;
import dvec.matrix,
       dvec.noise,
       dvec.vector;
import std.algorithm,
       std.exception,
       std.format,
       std.math,
       std.parallelism,
       std.socket,
       std.typecons;
import std.experimental.logger;

private
{
    enum WorldConnectionState : ubyte
    {
        ToAuth,
        Auth
    }
    
    /*Differences between the different distances used in world generation
     *The distance for final chunks is the final distance. Chunks in generation distance are decorated but not necessarily final. Chunks in load distance are sculpted
     */
    enum finalToGen  = 2;
    enum finalToLoad = finalToGen+2;

    /* World connection type used to manage a socket and associated player.
     * If the socket handle is a null value then the player is assumed to be local.
     */
    struct WorldConnection
    {
        LocalPlayer player;
        Socket client;
        
        uint received;
        WorldConnectionState state;
        MServDataType receiving;
        union
        {
            MServPlayerMovement playerMovement;
            MServChatMessageBuf chatMessage;
        }
        
        alias client this;
        
        this(Socket socket, LocalPlayer player)
        {
            //Save the socket
            client = socket;

            //Save a handle to the player and give them a default position if the current one is invalid
            this.player = player;
            if (player.pos.magSquared.isNaN)
            {
                player.pos = dvec3(0,500,0);
            }
        }
    }
    
    /* Type used to represent a loading area */
    struct LoadArea
    {
        PoolAllocator!Chunk chunks;
        Chunk*[ivec3] lookup;
        immutable int finalDistance;
        immutable int viewDistance;
        ivec3 genCenter;
        int genRadius = 1;
        
        this(int activeDistance, int viewDistance)
        {
            finalDistance = activeDistance+2;
            this.viewDistance = viewDistance;
        
            //Create a chunk allocator capable of allocating all chunks in the load distance and for loading singular arbitrary chunks
            chunks = typeof(chunks)((finalDistance+finalToLoad).radiusChunks 
                                   +(finalToLoad.radiusChunks * (finalDistance > activeDistance)));
        }
    }
    
    /* Type used to represent a chunk load for a player */
    struct NewChunk
    {
        Chunk* ptr;
        ivec3 pos;
        uint player;
    }
}

/* World server type that manages a save with terrain generation */
class WorldServer : WorldManager
{
    private:
        //Chunks being managed as in-generation
        Chunk*[ivec3] _allLookup;
        typeof(scoped!Thread(function {})) _genTask;
        bool _genTaskQuit;
        
        //Sockets used in network communication
        typeof(scoped!TcpSocket()) _server;
        typeof(scoped!SocketSet()) _socketSet;
        PoolAllocator!LocalPlayer _players;
        
        WorldConnection[] _clients;
        LoadArea[] _clientAreas;
        Queue!NewChunk _newChunks;
        uint _maxPlayers;
        
        //Locks used to synchronise the main thread and main generation thread
        PollLock _clientAreaLock;
        PollLock _newChunkLock;
        
        //World data
        Save _save;
        double _runtime = 0.0;
        
        uint _heightSeed;
        uint _heightSeed2;
        uint _secondarySeed;
        uint _tertiarySeed;
        uint _tempSeed;
        uint _humiditySeed;
        uint _mobSeed;
        
        enum chunkUpdateRange = 3;
        
        ref const readGenBlock(in ivec3 blockPos)
        {
            auto chunk = blockPos.chunkCoord;
            auto rel = blockPos - chunk.chunkWorldPos;
            assert(chunk in _allLookup, format!"Chunk %s for block %s is not loaded"(chunk, blockPos));
            assert(_allLookup[chunk].state != ChunkState.Final && _allLookup[chunk].state != ChunkState.ToLight, "Attempt to retrieve block of final chunk as in-generation");
            return (*_allLookup[chunk])[rel.x][rel.y][rel.z];
        }
        
        ref auto getGenBlock(in ivec3 blockPos)
        {
            auto pos = blockPos.chunkCoord;
            auto rel = blockPos - pos.chunkWorldPos;
            
            auto chunk = _allLookup.get(pos, null);
            assert(chunk, format!"Chunk %s for block %s is not loaded"(pos, blockPos));
            assert(chunk.state != ChunkState.Final && chunk.state != ChunkState.ToLight, "Attempt to retrieve block of final chunk as in-generation");
            
            chunk.toSave = true;
            return (*chunk)[rel.x][rel.y][rel.z];
        }
        
        void unload()
        {
            //Search all loaded chunks and perform unloading operations
            outer: foreach (i, chunk; _allLookup)
            {
                bool inLoadDistance;
                bool inFinalDistance;
                
                foreach (e, ref player; _clientAreas) with (player)
                {
                    auto magSquared = dvec3(i-genCenter).magSquared;
                    if (magSquared > finalDistance^^2)
                    {
                        //Remove chunks that are out of the final distance of players from their AAs
                        lookup.remove(i);
                    }
                    else
                    {
                        inFinalDistance = true;
                    }
                
                    if (!inLoadDistance && magSquared <= (finalDistance+finalToLoad)^^2)
                    {
                        //If this player is in distance then the chunk is to be kept
                        inLoadDistance = true;
                    
                        //Check if this chunk is within range of the player whose pool allocator is used
                        auto hostPlayer = &_clientAreas[chunk.allocID];
                        if (e != chunk.allocID && dvec3(i - hostPlayer.genCenter).magSquared > (finalDistance+finalToLoad)^^2)
                        {
                            //If it isn't then move the chunk data to the pool allocator of the player which was in distance
                            /* Possible bug: pool allocator to move to might be full itself. */
                            auto newChunk = &(_clientAreas[e].chunks.alloc());
                            _allLookup[i] = newChunk;
                            (cast(void[])(newChunk[0..1]))[] = (cast(void[])(chunk[0..1]))[];
                            hostPlayer.chunks.free(*chunk);
                            newChunk.allocID = cast(uint)e;
                        }
                    }
                }
                
                if (!inLoadDistance)
                {
                    if (chunk.toSave)
                    {
                        _save.saveChunk(i, *chunk);
                    }
                    _clientAreas[chunk.allocID].chunks.free(*chunk);
                    _allLookup.remove(i);
                }
                else if (!inFinalDistance)
                {
                    _chunkLookup.remove(i);
                }
            }
        }
        
        void saveAll()
        {
            foreach (i,e; _allLookup)
            {
                if (e.toSave)
                {
                    _save.saveChunk(i,*e);
                    e.toSave = false;
                }
            }
        }
        
        void lightChunk(in ivec3 chunk)
        {
            //If there is no above chunk then the top layer of this chunk must be fully lit
            auto blocks = _chunkLookup[chunk];
            auto chunkWorldPos = chunk.chunkWorldPos;
            
            if ((chunk + ivec3(0,1,0)) !in _chunkLookup)
            {
                foreach (x; 0..chunkSize)
                {
                    foreach (z; 0..chunkSize)
                    {
                        auto block = &((*blocks)[x][$-1][z]);
                        block.light = cast(ushort)( (block.light & 0xFFF0) | (0xF - (blockTypes[block.type].light >>> 12)) );
                    }
                }
            }
            
            //Create light updates for each block that acts as a light source
            foreach_reverse (y; 0..chunkSize)
            {
                foreach (x; 0..chunkSize)
                {
                    foreach (z; 0..chunkSize)
                    {
                        lightUpdates.put(chunkWorldPos + ivec3(x,y,z));
                    }
                }
            }
        }
        
        void updateChunk(in ivec3 chunk)
        {
            auto baseWorldPos = chunk.chunkWorldPos;
            ubyte[chunkSize/8][chunkSize][chunkSize] doNotUpdate;
            
            foreach (x; 0..chunkSize)
            {
                foreach (y; 0..chunkSize)
                {
                    foreach (z; 0..chunkSize)
                    {
                        auto pos = baseWorldPos + ivec3(x,y,z);
                        auto block = &readBlock(pos);
                        if (!(doNotUpdate[x][y][z/8] & (1 << 7-(z % 8))))
                        {
                            //Update this block if it has not been marked to skip
                            /*if (blockTypes[block.type].shape == BlockShape.Fluid && block.fluidHeight != 1)
                            {
                                //Make fluid blocks flow to surrounding blocks
                                foreach (adjX; -1..1)
                                {
                                    foreach (adjY; -1..1)
                                    {
                                        foreach (adjZ; -1..1)
                                        {
                                            auto adjBlock = getBlockPtr(pos + ivec3(adjX, adjY, adjZ));
                                            if (adjBlock && (adjBlock.type == block.type || adjBlock.type == blockID!"Air") && adjBlock.fluidHeight < block.fluidHeight-1)
                                            {
                                                adjBlock.type = block.type;
                                                adjBlock.fluidHeight = cast(ubyte)(block.fluidHeight-1);
                                                doNotUpdate[x][y][z/8] |= (1 << 7-(z % 8));
                                            }
                                        }
                                    }
                                }
                            }*/
                        }
                    }
                }
            }
        }
        
        void sculpt(Chunk* chunk, in ivec3 pos)
        {
            //Initial terrain shaping generation phase: all blocks are stone. Does not affect adjacent chunks, is safe to perform in multithreading
            auto baseWorldPos = dvec3(pos.chunkWorldPos);
            
            short[chunkSize][chunkSize] height;
            foreach (x; 0..chunkSize)
            {
                foreach (z; 0..chunkSize)
                {
                    auto xz = dvec2(baseWorldPos.x + x, baseWorldPos.z + z);
                    height[x][z] = cast(short)( perlin(_heightSeed, xz*0.00025)*512 + perlin(_heightSeed2, xz*0.0001)*2048 - 1400);
                }
            }

            foreach (x; 0..chunkSize)
            {
                foreach (y; 0..chunkSize)
                {
                    foreach (z; 0..chunkSize)
                    {
                        auto blockPos = baseWorldPos + dvec3(x,y,z);
                        auto block = &((*chunk)[x][y][z]);
                        auto temp = 0.1 + perlin(_tempSeed, blockPos*0.001)*0.9;
                        block.temp = cast(ubyte)(temp*255);
                        
                        //Calculate a threshold for the block to be solid
                        double threshold;
                        if (blockPos.y < 4096)
                        {
                            threshold = clamp((blockPos.y - height[x][z])*0.01*temp, 0, 1);
                        }
                        else
                        {
                            threshold = 1-clamp((blockPos.y-4096)*0.02*temp, 0, 0.35);
                        }
                        
                        //Calculate a density value and use it with the threshold to calculate if the block is solid
                        if (threshold < 1)
                        {
                            auto density = (perlin(_save.primarySeed, blockPos*0.01) + perlin(_secondarySeed, blockPos*0.05)*0.1)/1.1;
                            if (density > threshold)
                            {
                                block.type = blockID!"Stone";
                            }
                        }
                    }
                }
            }
        }
        
        void decorate(Chunk* chunk, in ivec3 pos)
        {
            //Secondary terrain decoration phase: block types are decided, decoration blocks are added. Not safe for multithreading
            auto baseWorldPos = pos.chunkWorldPos;
            foreach (x; 0..chunkSize)
            {
                foreach (y; 0..chunkSize)
                {
                    foreach (z; 0..chunkSize)
                    {
                        auto blockPos = ivec3(baseWorldPos.x+x, baseWorldPos.y+y, baseWorldPos.z+z);
                        auto block = &getGenBlock(blockPos);
                        auto aboveBlockPos = blockPos + ivec3(0,1,0);
                        auto aboveBlock = &getGenBlock(aboveBlockPos);
                        
                        if (block.type == blockID!"Stone" && aboveBlock.type != blockID!"Stone")
                        {
                            if (block.temp > 64*3)
                            {
                                //Desert biome
                                block.type = blockID!"Sand";
                                foreach (i; 1..4)
                                {
                                    auto next = &getGenBlock(blockPos - ivec3(0,i,0));
                                    if (next.type == blockID!"Air")
                                    {
                                        break;
                                    }
                                    next.type = blockID!"Sand";
                                }
                            }
                            else
                            {
                                //Plains biome
                                block.type = blockPos.y <= 2 ? blockID!"Sand" : blockID!"Grass";
                                if (block.type == blockID!"Grass")
                                {
                                    if (white(cast(double)_secondarySeed.hashOf(1), cast(double)blockPos.x, cast(double)blockPos.y, cast(double)blockPos.z) > 0.9)
                                    {
                                        //Place tall grass blocks above some grass blocks
                                        aboveBlock.type = blockID!"Tall Grass";
                                    }
                                    else if (white(cast(double)_secondarySeed.hashOf(2), cast(double)blockPos.x, cast(double)blockPos.y, cast(double)blockPos.z) > 0.999)
                                    {
                                        //Place trees above some grass blocks
                                        aboveBlock.type = blockID!"Oak Log";
                                        auto height = cast(int)(5 + white(cast(double)_secondarySeed.hashOf(3), cast(double)blockPos.x, cast(double)blockPos.y, cast(double)blockPos.z)*3);
                                        auto radius = cast(int)(3 + white(cast(double)_secondarySeed.hashOf(4), cast(double)blockPos.x, cast(double)blockPos.y, cast(double)blockPos.z)*2);
                                        foreach (i; 2..height)
                                        {
                                            getGenBlock(blockPos + ivec3(0,i,0)).type = blockID!"Oak Log";                                            
                                        }
                                        
                                        foreach (x2; -radius..radius+1)
                                        {
                                            foreach (y2; -1..radius+1)
                                            {
                                                foreach (z2; -radius..radius+1)
                                                {
                                                    if (dvec3(x2,y2,z2).magSquared <= radius^^2)
                                                    {
                                                        auto leaves = &getGenBlock(blockPos + ivec3(x2,y2+height,z2));
                                                        if (leaves.type == blockID!"Air")
                                                        {
                                                            leaves.type = blockID!"Oak Leaves";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                //Fill the blocks underneath grass blocks with dirt blocks
                                auto nextType = (block.type == blockID!"Sand") ? blockID!"Sand" : blockID!"Dirt";
                                foreach (i; 1..4)
                                {
                                    auto next = &getGenBlock(blockPos - ivec3(0,i,0));
                                    if (next.type == blockID!"Air")
                                    {
                                        break;
                                    }
                                    next.type = nextType;
                                }
                                
                                //Generate animals out of water
                                if (blockPos.y > 0 && white(cast(double)_mobSeed, cast(double)blockPos.x, cast(double)blockPos.y, cast(double)blockPos.z) > 0.9995)
                                {
                                    // THIS NEEDS TO BE MADE THREAD SAFE
                                    //createEntity!Pig(dvec3(aboveBlockPos));
                                }
                            }
                        }
                        else if (block.type == blockID!"Air")
                        {
                            if (blockPos.y <= 0)
                            {
                                block.type = blockID!"Water";
                            }
                        }
                    }
                }
            }
            
            chunk.state = ChunkState.Decorated;
        }
        
        void loadTask()
        {
            //This function is executed by the main generation thread and generates a stream of chunks
            uint id;
            
            bool toYield = true;
            while (!atomicLoad(_genTaskQuit))
            {
                _clientAreaLock.lock;
                if (_clientAreas.length == 0)
                {
                    _clientAreaLock.unlock;
                    _genTask.yield;
                    continue;
                }
                auto index = cast(uint)(id % _clientAreas.length);
                
                //Select a center chunk to generate
                bool found;
                ivec3 toGen;
                
                outer: foreach (immutable radius; _clientAreas[index].genRadius.._clientAreas[index].finalDistance+1)
                {
                    foreach (x; -radius..radius+1)
                    {
                        auto rangeYSq = radius^^2 - x^^2;
                        auto rangeY = cast(int)sqrt(cast(real)rangeYSq);
                        
                        foreach (y; -rangeY..rangeY+1)
                        {
                            auto rangeZ = rangeYSq - y^^2;
                            if (rangeZ >= 0)
                            {
                                rangeZ = cast(int)sqrt(cast(real)rangeZ);
                            
                                foreach (z; -rangeZ..rangeZ+1)
                                {
                                    auto pos = _clientAreas[index].genCenter + ivec3(x,y,z);
                                    auto chunk = _clientAreas[index].lookup.get(pos, null);
                                    if (!chunk || chunk.state < ChunkState.Final)
                                    {
                                        toGen = pos;
                                        found = true;
                                        break outer;
                                    }
                                }
                            }
                        }
                    }
                    _clientAreas[index].genRadius += 1;
                }
                
                if (!found)
                {
                    //Move onto the next area if a chunk to generate could not be found
                    id += 1;
                    _clientAreaLock.unlock;
                    
                    //If a full loop was done and no chunks were found for any load area, yield
                    if (index == _clientAreas.length-1)
                    {
                        if (toYield)
                        {
                            _genTask.yield;
                        }
                        toYield = true;
                    }
                    continue;
                }
                toYield = false;
import std.stdio; writeln(toGen, " is to be loaded");
                //Load a sphere of chunks around a center position with the assigned load distance
                Task!(run, void delegate(Chunk*, in Vec!(3LU, int)), Chunk*, Vec!(3LU, int))[] tasks;
                tasks.reserve(cast(uint)(finalToLoad.radiusChunks));
                
                foreach (x; -finalToLoad..finalToLoad+1)
                {
                    auto rangeYSq = finalToLoad^^2 - x^^2;
                    auto rangeY = cast(int)sqrt(cast(real)rangeYSq);

                    foreach (y; -rangeY..rangeY+1)
                    {
                        auto rangeZ = rangeYSq - y^^2;
                        if (rangeZ >= 0)
                        {
                            rangeZ = cast(int)sqrt(cast(real)rangeZ);
                        
                            foreach (z; -rangeZ..rangeZ+1)
                            {
                                //Check if the chunk isn't already loaded or in generation
                                auto pos = toGen + ivec3(x,y,z);
                                if (!_allLookup.get(pos, null))
                                {
                                    //Attempt to load the chunk from the save file
                                    auto chunk = &(_clientAreas[index].chunks.alloc());
                                    chunk.allocID = index;
                                    _allLookup[pos] = chunk;
                                    
                                    auto state = _save.loadChunk(pos, *chunk);
                                    if (state == ChunkState.NotPresent)
                                    {
                                        //Failing this create a new thread task to begin the sculpting phase of the new chunk to generate
                                        writeln(pos, " was not present in save file, generating");
                                        chunk.toSave = true;
                                        tasks ~= scopedTask(&sculpt, chunk, pos);
                                        taskPool.put(tasks[$-1]);
                                    }
                                    else if (state == ChunkState.Invalid)
                                    {
                                        //If the chunk data in the file was invalid then create an empty chunk in its place
                                        writeln("something fucked up when loading ", pos);
                                        logf(LogLevel.error, "Failed to load %s chunk from save, creating invalid placeholder chunk", pos);
                                        chunk.toSave = true;
                                        chunk.state  = ChunkState.Decorated;
                                        
                                        foreach (ref a; chunk.blocks)
                                        {
                                            foreach (ref b; a)
                                            {
                                                foreach (ref c; b)
                                                {
                                                    c = Block(blockID!"Air");
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                    	writeln(pos, " all fine");
                                    }
                                }
                            }
                        }
                    }
                }
                _clientAreas[index].lookup[toGen] = _allLookup[toGen];
                _clientAreaLock.unlock;

                foreach (ref i; tasks)
                {
                    i.spinForce;
                }
                
                //Decorate the newly generated chunks
                _clientAreaLock.lock;
                foreach (x; -finalToGen..finalToGen+1)
                {
                    auto rangeY = cast(int)sqrt(cast(real)( finalToGen^^2 - x^^2 ));

                    foreach (y; -rangeY..rangeY+1)
                    {
                        auto rangeZ = finalToGen^^2 - (x^^2 + y^^2);
                        if (rangeZ >= 0)
                        {
                            rangeZ = cast(int)sqrt(cast(real)rangeZ);
                            
                            foreach (z; -rangeZ..rangeZ+1)
                            {
                                auto chunk = toGen + ivec3(x,y,z);
                            	assert(chunk in _allLookup, format!"Chunk %s not loaded when it should be"(chunk));
                            
                                auto inGen = _allLookup[chunk];
                                if (inGen.state == ChunkState.Sculpted)
                                {
                                    decorate(inGen, chunk);
                                }
                            }
                        }
                    }
                }
                auto toGive = _allLookup[toGen];
                _clientAreaLock.unlock;
                
                //Save a reference to the newly generated center chunk
                if (toGive.state == ChunkState.Decorated)
                {
                    toGive.state = ChunkState.ToLight;
                }
                                
                _newChunkLock.lock;
                _newChunks.put(NewChunk(toGive, toGen, index));
                _newChunkLock.unlock;

                id += 1;
            }
        }
        
        void acceptIncoming()
        {
            auto socket = _server.accept;
        
            //Accept an authentication request
            _server.blocking = true;
            MServAuth auth;
            socket.receive((&auth)[0..1]);
            auto response = MServAuthResponse(min(auth.activeDistance, finalDistance-2));
            
            if (auth.valid)
            {
                log(LogLevel.info, auth.username, " has joined");
                response.code = MServAuthResponseCode.Ok;
                socket.blocking = false;
                auto username = auth.username;
                
                //Create a new player
                _clientAreaLock.lock;
                auto state = _save.getPlayer(username);
                _clients ~= WorldConnection(socket, _players.alloc(username, state));
                _clientAreas ~= LoadArea(response.actualActiveDistance, auth.viewDistance);
                _clientAreaLock.unlock;
            }
            else
            {
                response.code = MServAuthResponseCode.BadAuth;
            }
            
            //Send back an authentification response and world state data
            _server.blocking = false;
            socket.send((&response)[0..1]);
            
            if (response.code == MServAuthResponseCode.Ok)
            {
                //Send back initial world state data
                auto state = MServWorldState(_save.runtime);
                socket.send((&state)[0..1]);
                socket.send((&_clients[$-1].player)[0..1]);
            }
        }
        
        final void message(const(char)[] msg, string sender)
        {
            logf(LogLevel.info, "<%s> %s", sender, msg);
        }

    public:
        override LocalPlayer player(uint id = 0) { return _clients[id].player; }
        override ulong runtime() { return _save.runtime; }
        
        enum ushort defaultPort = 6578;
        
        final void createPlayer(Socket socket, string username, int activeDistance, int viewDistance)
        {
            enforce(_maxPlayers > _clients.length, "Not enough room for new player");
            _clientAreaLock.lock;
            auto state = _save.getPlayer(username);
            
            if (state.living.entity.origin.magSquared.isNaN)
            {
                _clients ~= WorldConnection(socket, _players.alloc(username, dvec3(0, 100, 0)));
            }
            else
            {
                _clients ~= WorldConnection(socket, _players.alloc(username, state));
            }
            _clientAreas ~= LoadArea(activeDistance, viewDistance);
            _clientAreaLock.unlock;
        }
        
        override void onHandSelect(ubyte slot, uint id = 0) { _clients[id].player.onHandSelect = slot; }
        
        override void message(string msg) { message(msg, "Server"); }

        override void update(ulong nsDT)
        {
            //Accept incoming connections
            _socketSet.reset;
            _socketSet.add(_server);
            
            //If there are no players make the server thread sleep
            if (_clients.length == 0)
            {
                Thread.sleep(dur!"nsecs"(1*(10^^8)));
            }

            //Check for additional connections
            while (_maxPlayers > _clients.length)
            {
                Socket.select(_socketSet, null, null, dur!"nsecs"(0));
                if (_socketSet.isSet(_server))
                {
                    acceptIncoming;
                }
                else
                {
                    break;
                }
            }
            
            //Check for data sent by connected client sockets
            foreach (i, ref e; _clients) with (e)
            {
                if (client)
                {
                    //Keep processing incoming data until there is no more left
                    while (true)
                    {
                        if (receiving == MServDataType.None)
                        {
                            if (client.receive((&receiving)[0..1]) > 0)
                            {
                                received = MServDataType.sizeof;
                            }
                            else
                            {
                                break;
                            }
                        }

                        switch (receiving)
                        {
                            case MServDataType.Disconnect:
                                receiving = MServDataType.None;
                                break;
                                
                            case MServDataType.PlayerMovement:
                                //Receive player movement data
                                if (received < MServPlayerMovement.sizeof)
                                {
                                    received += client.receive((cast(void[])((&playerMovement)[0..1]))[received .. $]);
                                }
                                if (received == MServPlayerMovement.sizeof)
                                {
                                    player.pos = playerMovement.pos;
                                    movePlayer(playerMovement.movement, playerMovement.headOrient, playerMovement.running, false, cast(uint)i);
                                    receiving = MServDataType.None;
                                }
                                break;
                                
                            case MServDataType.ChatMessage:
                                //Recieve a chat message
                                if (received < MServChatMessageHeader.sizeof)
                                {
                                    received += client.receive((cast(void[])((&playerMovement)[0..1]))[received .. $]);
                                }
                                else 
                                {
                                    if (received < MServChatMessageHeader.sizeof + chatMessage.len)
                                    {
                                        received += client.receive(chatMessage.chars[received-MServChatMessageBuf.sizeof..chatMessage.len]);
                                    }
                                    message(chatMessage.chars[0..chatMessage.len], "Player");
                                    receiving = MServDataType.None;
                                }
                                break;
                                
                            default:
                                break;
                        }
                    }
                }
            }
            
            //Update player load areas
            if (_clientAreaLock.tryLock)
            {
                foreach (i; 0.._clientAreas.length) with (_clientAreas[i])
                {
                    //If the center load chunk has changed reset the stored center and radius
                    auto center = player.pos.chunkCoord;
                    if (genCenter != center)
                    {
                        genRadius -= min(genRadius, (genCenter - center).mag+1);
                        genCenter = center;
                    }
                }
                
                unload;
                _clientAreaLock.unlock;
            }
        
            //Load chunks created in the previous load task
            if (_newChunkLock.tryLock)
            {
                while (!_newChunks.empty)
                {
                    auto next = _newChunks.front;
                    _newChunks.popFront;
                    
                    //Save the chunk
                    _chunkLookup[next.pos] = next.ptr;
                    if (next.ptr.state == ChunkState.ToLight)
                    {
                        lightChunk(next.pos);
                        next.ptr.state = ChunkState.Final;
                    }

                    //Send the chunk to the client socket
                    with (_clients[next.player])
                    {
                        if (client)
                        {
                            auto compressed = _save.compress(*next.ptr);
                            auto header = MServChunkHeader(next.pos, cast(uint)compressed.length);
                            client.send((&header)[0..1]);
                            client.send(compressed);
                        }
                    }
                }
                _newChunkLock.unlock;
            }
            
            //A day is equivalent to 24 minutes
            _dt = nsDT;
            _save.runtime += nsDT;
            _time = _save.runtime % (60*24*(10L^^9));
            
            processLightUpdates;

            //Update players and entities
            foreach (ref i; _clients)
            {
                if (i.player)
                {
                    updateEntity(i.player, nsDT);
                    
                    //Create block updates around each player for chunks which are loaded
                    auto center = i.player.pos.chunkCoord;
                    foreach (x; -chunkUpdateRange..chunkUpdateRange)
                    {
                        foreach (y; -chunkUpdateRange..chunkUpdateRange)
                        {
                            foreach (z; -chunkUpdateRange..chunkUpdateRange)
                            {
                                auto pos = ivec3(x,y,z);
                                if (dvec3(pos).magSquared <= chunkUpdateRange^^2 && pos in _chunkLookup)
                                {
                                    //updateChunk(pos);
                                }
                            }
                        }
                    }
                }
            }
            
            auto nextEntity = _entityList;
            while (nextEntity)
            {
                updateEntity(nextEntity, nsDT);
                nextEntity = nextEntity.nextGlobal;
            }
        }
    
        this(string name, uint maxActiveDistance, uint maxPlayers)
            in (maxActiveDistance > 2 && maxActiveDistance < int.max && maxPlayers > 0)
        {
            log(LogLevel.info, "Server is starting");
            _save = Save(name);
            
            //Derive seeds from the primary seed
            _heightSeed = cast(uint)_save.primarySeed.hashOf(1);
            _heightSeed2 = cast(uint)_save.primarySeed.hashOf(2);
            _secondarySeed = cast(uint)_save.primarySeed.hashOf(3);
            _tertiarySeed = cast(uint)_save.primarySeed.hashOf(4);
            _tempSeed = cast(uint)_save.primarySeed.hashOf(5);
            _humiditySeed = cast(uint)_save.primarySeed.hashOf(6);
            _mobSeed = cast(uint)_save.primarySeed.hashOf(7);
            _maxPlayers = maxPlayers;
            
            //Setup sockets
            _server = scoped!TcpSocket;
            _server.blocking = false;
            
            _socketSet = scoped!SocketSet(1);
            _players = typeof(_players)(maxPlayers);
            _clients.reserve(maxPlayers);
            _clientAreas.reserve(maxPlayers);
            _newChunks = Queue!NewChunk(maxPlayers);
            
            //Final distance is 2 chunks larger than the view distance, as models need to be fully surrounded for model generation
            super(maxActiveDistance+2);
            
            _genTask = scoped!Thread(&this.loadTask, 0);
            _genTask.start;
            _genTask.priority = Thread.PRIORITY_MIN;
        }
        
        this(string name, uint maxActiveDistance, uint maxPlayers, ushort port)
        {
            //Make the world server available through some port
            this(name, maxActiveDistance, maxPlayers);
            auto addr = scoped!InternetAddress(port);
            _server.bind(addr);
            log(LogLevel.info, "Server is now bound to port ", port);
            _server.listen(maxPlayers);
        }
        
        ~this()
        {
            log(LogLevel.info, "Server is shutting down");
            atomicStore(_genTaskQuit, true);
            
            //Send a disconnect message to all clients
            auto disconnect = MServDataType.Disconnect;
            foreach (ref i; _clients)
            {
                if (i.client)
                {
                    i.client.send((&disconnect)[0..1]);
                }
            }
            
            //Save all loaded chunks and deallocate their memory
            _genTask.join;
            if (_save.isOpen)
            {
                foreach (i,e; _allLookup)
                {
                    if (e.toSave)
                    {
                        _save.saveChunk(i,*e);
                    }
                    _clientAreas[e.allocID].chunks.free(*e);
                }
            }
            
            //Save all players
            foreach (i, ref e; _clients)
            {
                if (e.player)
                {
                    _save.savePlayer(e.player.username, e.player.saveState);
                }
            }
            
            //Free the dynamic array of connection data structures
            GC.free(_clients.ptr);
            GC.free(_clientAreas.ptr);
        }
}

class InternalWorldServer : WorldServer
{
    this(string name, string username, uint activeDistance, uint viewDistance)
    {
        //Start the server and create the default player
        super(name, activeDistance, 1);
        createPlayer(null, username, activeDistance, viewDistance);
    }
}
