module server.main;

import common.config;
import common.world.server;
import core.stdc.signal;
import std.datetime.stopwatch;
import std.experimental.logger;
import std.exception,
       std.file,
       std.typecons;
     
private bool toTerminate;

extern(C) nothrow @nogc @system void sigint(int sid) { toTerminate = true; }

void createConfig(ref OptionManager options)
{
    options = OptionManager("server.conf");
    
    double val;
    with (options)
    {
        val = createSaved!"maxPlayers"(8);
        enforce(val >= 1, "Maximum player count must allow for at least one player");
        val = createSaved!"maxActiveDistance"(12);
        enforce(val >= 3, "Active chunk distance must be 3 chunks or greater");
    }
}

void main()
{
    debug sharedLog.logLevel = LogLevel.all;

    debug logf(LogLevel.info, "Mantle server version 0.0.3 debug (%s D-%s)", __VENDOR__, __VERSION__);
    else  logf(LogLevel.info, "Mantle server version 0.0.3 release (%s D-%s)", __VENDOR__, __VERSION__);
    
    enforce(signal(SIGINT, &sigint) != SIG_ERR, "Failed to set signal handler for process termination");

    //Load a configuration file for the server
    OptionManager options = void;
    try
    {
        options.createConfig;
    }
    catch (Exception error)
    {
        logf(LogLevel.error, "Failed to load server.conf (%s), reseting config file", error.msg);
        options = OptionManager();
        std.file.remove("server.conf");
        options.createConfig;
    }
    
    //Start the server
    StopWatch delta;
    delta.start;
    auto world = scoped!WorldServer("served", options.num!("maxActiveDistance", uint), options.num!("maxPlayers", uint), WorldServer.defaultPort);
    
    while (!toTerminate)
    {
        world.update(delta.peek.total!"nsecs"/1e9);
        delta.reset;
    }
}
