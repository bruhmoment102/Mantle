import common.data.common,
       common.data.marc;
import std.exception,
       std.file,
       std.format,
       std.getopt,
       std.stdio,
       std.string,
       std.path,
       std.zlib;
       
static assert(MARCHeader.validMagic == "MARCVER0", "MARC format version has changed in the engine, tool must be updated!");

auto defaultType(string extension)
{
    final switch (extension)
    {
        case "bmp" : return strUUID!"texture";
        case "vspv": return strUUID!"shader_vertex";
        case "fspv": return strUUID!"shader_fragment";
        case "wav" : return strUUID!"audio";
        case "mobj": return strUUID!"model";
    }
}

auto defaultLife(UUID type)
{
    switch (type)
    {
        case strUUID!"shader_vertex",
             strUUID!"shader_fragment",
             strUUID!"block_texture"  : return MARCResourceLife.Temporary;
        default                       : return MARCResourceLife.Full;
    }
}

bool isConfig(string file)
{
    switch (file.extension)
    {
        default: return false;   
    }
}

string[string] getMetaConfig(string file)
{
    auto toReturn = ["type":cast(string)null];
    auto metaPath = file.isConfig ? file : format!"%s/.%s.meta"(file.dirName, file.baseName);

    if (metaPath.exists)
    {
        auto metaFile = File(metaPath, "r");
        while (!metaFile.eof)
        {
            string option;
            string value;
            metaFile.readf("%s:%s\n", option, value);
            if (option.length > 0 && value.length > 0)
            {
                toReturn[option] = value.strip;
            }
        }
    }
    return toReturn;
}

void main(string[] args)
{
    //Parse for environment options
    bool extract;
    string output;
    getopt(args, "extract|x", &extract,
                 "output|o" , &output);
    enforce(extract || output !is null, "Output filename must be provided using --output");
                 
    if (extract)
    {
        stderr.writeln("Not implemented yet");
        return;
    }
    else
    {
        //Create a new MARC archive file using the specified resource files
        auto archive = File(output, "w");
        auto header  = MARCHeader(MARCHeader.validMagic, args.length-1);
        rawWrite(archive, header);
        
        auto refsBegin  = archive.tell;
        auto headerRefs = new MARCHeaderRef[args.length-1];
        archive.seek(MARCHeaderRef.sizeof * (args.length-1), SEEK_CUR);
        
        //Create the data for the resources
        foreach (i,e; args[1..$])
        {
            //Save the metadata for the header reference
            auto name = e.baseName;
            enforce(name.length < MARCResourceHeader.maxNameChars, format!"Name for resource %s exceeds maximum allowed length of %s characters"(name.length, MARCResourceHeader.maxNameChars));
            headerRefs[i] = MARCHeaderRef(name.toUUID, archive.tell);

            //Write the data for the resource into the archive file
            auto config = getMetaConfig(e);
            auto data = compress(e.isConfig ? config.get("data", null) : e.read, 9);
            auto meta = MARCResourceHeader(headerRefs[i].uuid, 0, MARCResourceLife.Full, DirEntry(e).size, data.length);
            
            auto type = config["type"];
            meta.typeUUID = type is null ? e.extension[1..$].defaultType : type.toUUID;
            meta.lifetime = meta.typeUUID.defaultLife;

            rawWrite(archive, meta);
            archive.writef("%s\x00", name);
            archive.rawWrite(data);
        }
        
        //Go back to the beginning of the file and update the reference table
        archive.seek(refsBegin);
        foreach (ref i; headerRefs)
        {
            rawWrite(archive, i);
        }
    }
}
