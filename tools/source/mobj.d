import common.data.mobj;
import dvec.vector;
import std.algorithm,
       std.conv,
       std.exception,
       std.file,
       std.format,
       std.getopt,
       std.path,
       std.random,
       std.stdio,
       std.string;
       
static assert(MOBJHeader.validMagic == "MOBJVER0", "ROBJ format version has changed in the engine, tool must be updated!");

void main(string[] args)
{
    foreach (file; args[1..$])
    {
        enforce(file.extension == ".obj", format!"Unrecognised file extension on model file %s"(file));
        writefln("Converting file %s to MOBJ format", file);
        
        //Write the ROBJ header and materials
        auto oldFile = File(file, "r");
        
        //Pass through the usemtl and f command lines of the OBJ file
        uint[3][] toGen;
        vec3[] vertices;
        vec3[] textureCoords;
        
        foreach (line; oldFile.byLine)
        {
            //Ignore empty or comment lines
            if (line.length == 0 || line[0] == '#')
            {
                continue;
            }
            
            //Parse the current line
            auto tokens = line.splitter;
            switch (tokens.front)
            {
                case "f":
                    uint[3][3] indices;
                    foreach (ref i; indices)
                    {
                        tokens.popFront;
                        
                        //For each vertex specified, parse the indices to use using the format base/text/norm
                        auto components = tokens.front.splitter('/');
                        foreach (e; 0..3)
                        {
                            if (components.empty)
                            {
                                break;
                            }
                            
                            i[e] = components.front.length == 0 ? i[0] : (components.front.to!uint - 1);
                            components.popFront;
                        }
                    }
                    toGen ~= indices;
                    break;
                case "v":
                    vertices.length += 1;
                    foreach (ref i; vertices[$-1])
                    {
                        tokens.popFront;
                        i = tokens.front.to!(typeof(i));
                    }
                    break;
                case "vt":
                    textureCoords.length += 1;
                    foreach (i, ref e; textureCoords[$-1])
                    {
		                tokens.popFront;
                        e = tokens.empty ? cast(typeof(e))0.0 : tokens.front.to!(typeof(e));
                        enforce(!tokens.empty || i == 2, "Invalid number of components to texture coordinate");
                    }
                    break;
                default:
                    break;
            }
        }
        
        //Generate an optimised array of vertices with a corresponding index buffer
        uint[] indexBuf;
        uint[] newToGen;
        indexBuf.reserve(toGen.length);
        newToGen.reserve(toGen.length);
        
        outer: foreach (i, ref e; toGen)
        {
            foreach (f, ref g; newToGen)
            {
                //Search all vertices starting from the first, and generate an index to a matching vertex if one is found
                if (e == toGen[g])
                {
                    indexBuf ~= cast(uint)f;
                    continue outer;
                }
            }
            
            newToGen ~= cast(uint)i;
            indexBuf ~= cast(uint)(newToGen.length - 1);
        }
        
        enforce(indexBuf.length <= uint.max && newToGen.length <= uint.max, format!"Number of vertices used in model %s exceeds maximum allowed"(file));

        //Construct the new file from the data gathered
        auto newFile = File(file.setExtension("mobj"), "w");
        rawWrite(newFile, MOBJHeader(MOBJHeader.validMagic));
        rawWrite(newFile, cast(uint)indexBuf.length);
        newFile.rawWrite(indexBuf);

        foreach (i; newToGen)
        {
            auto indices = toGen[i].ptr;
        
            enforce(indices[0] < vertices.length, "Invalid vertex data");
            auto textureCoord = indices[1] >= textureCoords.length ? vec3(0.0) : textureCoords[indices[1]];
            rawWrite(newFile, Vertex(vertices[indices[0]], 0, textureCoord, 0));
        }
    }
}
